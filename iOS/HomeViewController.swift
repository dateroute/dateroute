//
//  HomeViewController.swift
//  DateRoute
//
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import AlamofireImage
import SwiftyJSON
import TGPControls
import CoreLocation

class HomeViewController: UIViewController, GMSMapViewDelegate {
    
    private let cellId = "routeDetailsCell"

    var indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    var alamoFireManager = Alamofire.SessionManager()
    
    var routeLauncher = routeDetailsLauncher()
    var routeDetailsView = routeDetailsLauncher().routeDetailCollectionView
    var routeDetailsVisible = false
    var routeName: String? = nil
    
    var ROUTES = [dateRoute]()
    var colors: [Int:(UIColor,String)] = [1:(UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0), "blue"), 2:(UIColor(red: 134/255, green: 218/255, blue: 134/255, alpha: 1.0),"green"), 3:(UIColor(red: 154/255, green: 127/255, blue: 195/255, alpha: 1.0),"purple"), 4:(UIColor(red: 234/255, green: 107/255, blue: 148/255, alpha: 1.0),"pink"), 5:(UIColor(red: 255/255, green: 147/255, blue: 55/255, alpha: 1.0),"orange")]
    
    var mapView = GMSMapView()
    var infoWindow = markerWindow()

    // Used so that we can remove the info window from the last marker
    var lastTappedMarker = GMSMarker()
    var haveTappedMarker = false
    let infoWindowOffset: CGFloat = 125
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var zoomLevel: Float = 14.0
    var userLocationMarker: GMSMarker?
    var userLocationError: GMSCircle?

    var currentStop : RouteStop?
    var currentRoute: dateRoute?
    
    // Used as the search error popup message
    let searchErrorAlert = UIAlertController(title: "Something went wrong", message: "Try searching again or modifying your search area", preferredStyle: .alert)
    
    enum routeTypes {
        case Featured
        case Saved
        case Custom
        case None
    }
    var routeType = routeTypes.None
    
    let selectedButtonColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
    let unselectedButtonColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.3)
    
    var activityButtons: [UIButton]?
    var activityImages = [UIButton]()
    var barsButtons: [UIButton]?
    var restaurantsButtons: [UIButton]?

    var selectedActivities = [String]()
    var selectedBars = [String]()
    var selectedRestaurants = [String]()
    var numberStops = 3
    
    var currentSearchPage = SearchPage.Activity
    enum SearchPage {
        case Activity
        case Bars
        case Restaurants
        case Search
    }
    
    var paths: [GMSMutablePath]?
    
    let screenHeight = UIApplication.shared.keyWindow?.frame.height
    let screenWidth = UIApplication.shared.keyWindow?.frame.width
    var markerIconScale: Double = 3.0
    var markerIncreaseSize: Bool = true
    
    //let searchFrame = CGRect(x: 0, y: 0, width: (UIApplication.shared.keyWindow?.frame.width)!, height: (UIApplication.shared.keyWindow?.frame.height)!*5/8)
    
    let searchFrame = CGRect(x: 0, y: 0, width: (UIApplication.shared.keyWindow?.frame.width)!, height:415)
    
    let searchLocationFrame = CGRect(x: 0, y: 0, width: (UIApplication.shared.keyWindow?.frame.width)!, height: (UIApplication.shared.keyWindow?.frame.height)!*1/4)

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        if routeType == routeTypes.Featured {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
        } else {
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        for view in (self.navigationController?.navigationBar.subviews)!{
            if view.tag == 1 {
                view.removeFromSuperview()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // add the "ok" action to the search error alert
        searchErrorAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        UIApplication.shared.statusBarStyle = .default
        view.backgroundColor = UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.0)
//        navigationItem.title = "Search"
//        let label = UILabel(frame: CGRect(x: view.center.x, y: view.center.y+66, width: 97, height: 33))
//        label.font = UIFont(name: "Avenir-Heavy", size: 24)
//        label.textAlignment = .center
//        label.textColor = UIColor(red:0, green:0, blue:0, alpha:1.0)
//        navigationItem.titleView = label
//        if routeType == routeTypes.Featured {
//            self.navigationController?.navigationBar.barTintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.0)
//            self.navigationController?.navigationBar.isTranslucent = true
//        } else {
//            self.navigationController?.navigationBar.isHidden = true
//        }
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        do {
            // Set the map style by passing the URL of the local file. Make sure googleMapsStyle.json is present in your project
            if let styleURL = Bundle.main.url(forResource: "googleMapsStyle", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find googleMapsStyle.json")
            }
        } catch {
            print("The style definition could not be loaded: \(error)")
        }
        // TODO: add back num stop slider
        // numStopSlider.addTarget(self, action: #selector(numStopsChanged), for: .valueChanged)
        
        setUpRoutes()

    }
    
    
    func setUpRoutes() {
        // Only showMapView if we aren't already looking at the map
        if self.view != self.mapView {
            self.showMapView()
        }
        if routeType == routeTypes.Featured{ //show map view of featured
            let route = self.ROUTES[0]
            route.showRouteMarkers(onRoute: false, currentLocation: nil)
            
            DispatchQueue.main.async {
                let bounds = GMSCoordinateBounds(path:route.Path)
                self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
                self.mapView.setMinZoom(1, maxZoom: 18)
            }
        } else  {
            if ROUTES.count > 0 {
                let allRoutePath = combineRoutesForFrame(Routes: self.ROUTES, onRoute: false)
                let bounds = GMSCoordinateBounds(path:allRoutePath)
                self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
                self.mapView.setMinZoom(1, maxZoom: 18)
            }
        }

        for route in ROUTES {
            route.getImages()
        }
    }

    func showMapView () {
        self.mapView.delegate = self
        self.view = self.mapView
        self.indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        self.indicator.center = view.center
        self.view.addSubview(indicator)
        if (locationManager.location?.coordinate != nil) {
            self.mapView.camera = GMSCameraPosition.camera(withTarget: (locationManager.location?.coordinate)!, zoom: zoomLevel)
        }
        self.mapView.settings.consumesGesturesInView = false
        if routeType != routeTypes.Featured && Variables.onRoute == false{
            self.addSearchBarConstraints()
        }
        
    }
    
    func combineRoutesForFrame(Routes:[dateRoute], onRoute:Bool) -> GMSMutablePath {
        var allRoutePath = GMSMutablePath()
        for route in Routes {
            if onRoute {
                allRoutePath = route.Path
                allRoutePath.add((locationManager.location?.coordinate)!)
            } else {
                let path =   route.showRouteMarkers(onRoute: false, currentLocation: nil)
                for point in path {
                    allRoutePath.add(point)
                }
            }

        }
        return allRoutePath
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        // Only dismiss route details if they are visible
        if routeDetailsVisible {
            routeLauncher.dismissRouteDetails()
        }
        
        // TODO: add info box here with marker index 0
        let overlayName = overlay.title!
        if routeName == nil || routeName! != overlayName {
            for route in ROUTES {
                if route.Name == overlayName {
                    print("tapped route path")
 
                    let tabBarHeight = tabBarController?.tabBar.frame.height
                    routeLauncher = routeDetailsLauncher()
                    routeLauncher.displayRouteDetails(homeController: self, route: route, routeName: overlayName, screenWidth: screenWidth!, screenHeight: screenHeight!, mapHeight: self.mapView.frame.height, mapView: self.mapView, tabBarHeight: tabBarHeight!, color: route.Color!)
                    routeDetailsVisible = true
                    routeName = Optional.some(overlayName)
                }
            }
        }
    }
    
    @objc func endRoute() {
        Variables.onRoute = false
        Variables.currentRoute = nil
        Variables.checkedinIDs = []
        endRouteButton.removeFromSuperview()
        checkinButton.removeFromSuperview()
        callButton.removeFromSuperview()
        routeLauncher.startRouteButton.isEnabled = true
        nextStopButton.isEnabled = true
        mapView.clear()
        for route in ROUTES {
            if route.OnRoute {
                route.OnRoute = false
            }
            route.showRouteMarkers(onRoute: false, currentLocation: nil)
        }
        let allRoutePath = combineRoutesForFrame(Routes: self.ROUTES, onRoute: false)
        let bounds = GMSCoordinateBounds(path:allRoutePath)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
        if routeType != routeTypes.Featured && Variables.onRoute == false {
            addSearchBarConstraints()
        }
        currentStopview.removeFromSuperview()
    }
    
    
    func addOnRouteButtons(route:dateRoute) {
        route.MapView.addSubview(currentStopview)
        if routeType == routeTypes.Featured {
            currentStopview.topAnchor.constraint(equalTo: route.MapView.safeAreaLayoutGuide.topAnchor, constant: -35).isActive = true
            currentStopview.bottomAnchor.constraint(equalTo: route.MapView.safeAreaLayoutGuide.topAnchor, constant: 60).isActive = true
        } else {
            currentStopview.bottomAnchor.constraint(equalTo: route.MapView.safeAreaLayoutGuide.topAnchor, constant: 95).isActive = true
            currentStopview.topAnchor.constraint(equalTo: route.MapView.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        }
        currentStopview.leftAnchor.constraint(equalTo: route.MapView.rightAnchor, constant: -1*route.MapView.frame.width*3/4).isActive = true
        currentStopview.rightAnchor.constraint(equalTo: route.MapView.rightAnchor, constant: -10).isActive = true
        
        addCurrentStopDetails()
        route.MapView.reloadInputViews()
    }
    
    func addCurrentStopDetails(){
        currentStopview.addSubview(nextStopButton)
        nextStopButton.centerYAnchor.constraint(equalTo: currentStopview.centerYAnchor, constant:0).isActive = true
        nextStopButton.rightAnchor.constraint(equalTo: currentStopview.rightAnchor, constant: -10).isActive = true
        nextStopButton.leftAnchor.constraint(equalTo: currentStopview.rightAnchor, constant: -40).isActive = true
        
        currentStopview.addSubview(currentStopButton)
        currentStopButton.setImage(currentStop?.Image, for: .normal)
        currentStopButton.setTitle(currentStop?.Url, for: .normal)
        currentStopButton.addTarget(self, action: #selector(linkToYelp(sender:)), for: .touchUpInside)
        currentStopButton.topAnchor.constraint(equalTo: currentStopview.topAnchor, constant: 5).isActive = true
        currentStopButton.leftAnchor.constraint(equalTo: currentStopview.leftAnchor, constant: 5).isActive = true
        currentStopButton.rightAnchor.constraint(equalTo: currentStopview.leftAnchor, constant: 85).isActive = true
        currentStopButton.bottomAnchor.constraint(equalTo: currentStopview.bottomAnchor, constant: -5).isActive = true
        
        
        currentStopview.addSubview(currentStopLabel)
        currentStopLabel.text = currentStop?.Name
        currentStopLabel.topAnchor.constraint(equalTo: currentStopview.topAnchor, constant: 3).isActive = true
        currentStopLabel.leftAnchor.constraint(equalTo: currentStopButton.rightAnchor, constant: 10).isActive = true
        currentStopLabel.rightAnchor.constraint(equalTo: nextStopButton.leftAnchor, constant: -15).isActive = true
        currentStopLabel.bottomAnchor.constraint(equalTo: currentStopview.bottomAnchor, constant: -30).isActive = true
        

        currentStopview.addSubview(walkButton)
        walkButton.bottomAnchor.constraint(equalTo: currentStopview.bottomAnchor, constant: -5).isActive = true
        walkButton.topAnchor.constraint(equalTo: currentStopview.bottomAnchor, constant: -29).isActive = true
        walkButton.leftAnchor.constraint(equalTo: currentStopLabel.leftAnchor, constant: 5).isActive = true
        
        currentStopview.addSubview(timeButton)
        timeButton.topAnchor.constraint(equalTo: walkButton.topAnchor, constant: 4).isActive = true
        timeButton.bottomAnchor.constraint(equalTo: walkButton.bottomAnchor, constant: -4).isActive = true
        timeButton.leftAnchor.constraint(equalTo: walkButton.rightAnchor, constant: 5).isActive = true
        //timeButton.rightAnchor.constraint(equalTo: checkinButton.leftAnchor, constant: -10).isActive = true
        
        currentRoute?.MapView.reloadInputViews()
    }
    
    
    func removeCurrentStopDetails() {
        currentStopButton.removeFromSuperview()
        currentStopLabel.removeFromSuperview()
    }
    
    
    @objc func showActivityWindow(_ sender: UIButton) {
        searchImage.image = #imageLiteral(resourceName: "searchIcon")
        exitButton.isHidden = false
        searchTitle.isHidden = true
        displayActivityWindow()
    }

    
    
    func shareRoute(){
        let activityViewController = UIActivityViewController(activityItems: ["I just planned a great date for tonight! Learn more at www.dateroute.io"], applicationActivities: nil)
        present(activityViewController, animated:true, completion:nil)
    }

    
    let activityWindow: UIView = {
        let window = UIView()
        //window.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        window.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        window.layer.shadowColor = UIColor.black.cgColor
        window.layer.shadowOpacity = 0.7
        window.layer.shadowOffset = CGSize.zero
        window.layer.shadowRadius = 10
        return window
    }()
    
    let buildRouteScrollView: UIScrollView = {
        let view = UIScrollView(frame: CGRect(x: 0, y: 320, width: (UIApplication.shared.keyWindow?.frame.width)!, height: 410-320))
        //window.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        view.isScrollEnabled = true
        //window.alwaysBounceHorizontal = true
        view.contentSize = CGSize(width: 600, height: 410-320)

        // TODO: I think this is where we could add a gesture recognizer to change between stops as we scroll through the stops
        // view.addGestureRecognizer(<#T##gestureRecognizer: UIGestureRecognizer##UIGestureRecognizer#>)
        return view
    }()
    
    let barsWindow: UIView = {
        let window = UIView()
        //window.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        window.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        window.layer.shadowColor = UIColor.black.cgColor
        window.layer.shadowOpacity = 0.7
        window.layer.shadowOffset = CGSize.zero
        window.layer.shadowRadius = 10
        return window
    }()
    
    
    let restaurantsWindow: UIView = {
        let window = UIView()
        //window.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        window.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        window.layer.shadowColor = UIColor.black.cgColor
        window.layer.shadowOpacity = 0.7
        window.layer.shadowOffset = CGSize.zero
        window.layer.shadowRadius = 10
        return window
    }()
    
    let searchWindow: UIView = {
        let window = UIView()
        //window.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        window.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        window.layer.shadowColor = UIColor.black.cgColor
        window.layer.shadowOpacity = 0.7
        window.layer.shadowOffset = CGSize.zero
        window.layer.shadowRadius = 10
        return window
    }()

    
    @objc func numStopsChanged(_ sender: TGPDiscreteSlider) {
        numberStops = Int(sender.value) + 2
    }
    
    let numStopLabels: TGPCamelLabels = {
        let labels = TGPCamelLabels()
        labels.names = ["2","3","4","5"]
        labels.translatesAutoresizingMaskIntoConstraints = false
        labels.upFontSize = 25
        labels.upFontColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        labels.downFontSize = 15
        labels.emphasisLayout = 4
        labels.regularLayout = 3
        return labels
    }()
    
    let dayNightLabels: TGPCamelLabels = {
        let labels = TGPCamelLabels()
        labels.names = ["day", "night"]
        labels.translatesAutoresizingMaskIntoConstraints = false
        labels.upFontSize = 15
        labels.ticksDistance = 32
        labels.upFontColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        labels.downFontSize = 11
        labels.emphasisLayout = 4
        labels.value = 1
        //labels.regularLayout = 3
        return labels
    }()
    
    let dayNightSwitch: UISwitch = {
        let switchTime = UISwitch()
        switchTime.translatesAutoresizingMaskIntoConstraints = false
        switchTime.isOn = true
        switchTime.onTintColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        switchTime.addTarget(self, action: #selector(dayNightChanged), for: .valueChanged)
        return switchTime
    }()
    
    @objc func dayNightChanged(_ sender: UISwitch) {
        dayNightLabels.value = (sender.isOn) ? 1 : 0
    }
    
    let numStops:UILabel = {
        let label = UILabel()
        label.text = "How many stops?"
        label.textColor = UIColor.white
        label.font = UIFont(name: "Avenir-Medium", size: 20)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let beginSearchLabel:UILabel = {
        let label = UILabel()
        label.text = "Move the map to your neighborhood of choice"
        //label.textColor = UIColor.white
        label.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
        label.font = UIFont(name: "Avenir-Medium", size: 20)
        label.numberOfLines = 3
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let searchButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "searchRoutesIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(generateRoutes), for: .touchUpInside)
        return button
    }()
    
    
    let searchTitle:UILabel = {
        let label = UILabel() //frame: CGRect(x: (UIApplication.shared.keyWindow?.frame.width)!/2-100, y: 23, width: 200, height: 30)
        label.text = "Create a new route"
        label.textColor = UIColor.gray
        label.font = UIFont(name: "Avenir-Medium", size: 18)
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let searchImage: UIImageView = {
        let image = UIImageView() //frame: CGRect(x: 15, y: 23, width: 30, height: 30))
        image.image = #imageLiteral(resourceName: "searchIcon")
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let exitButton: UIButton = {
        let button = UIButton() //frame: CGRect(x: 10, y: 18, width: screenwidth!-29=0, height: 40)
        button.setImage(#imageLiteral(resourceName: "exitIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        button.addTarget(self, action: #selector(closeActivityWindow), for: .touchUpInside)
        return button
    }()
    
    let searchBar: UIButton = {
        let button = UIButton() //frame: CGRect(x: 1, y: 23, width: 30, height: 30)
        button.backgroundColor = UIColor.white
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(showActivityWindow), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let activityLabel: UILabel = {
        let label = UILabel()
        label.text = "Tap the options below"
        label.font = UIFont(name: "Avenir-Heavy", size: 20)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        //label.textColor = UIColor.white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let barsLabel: UILabel = {
        let label = UILabel()
        label.text = "Any of these bars sound interesting?"
        label.font = UIFont(name: "Avenir-Heavy", size: 20)
        label.numberOfLines = 2
        //label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let restaurantsLabel: UILabel = {
        let label = UILabel()
        label.text = "What kind of restaurant you feelin'?"
        label.font = UIFont(name: "Avenir-Heavy", size: 20)
        label.numberOfLines = 2
        //label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let nextButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "skipIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(nextScreen), for: .touchUpInside)
        return button
    }()
    
    let prevButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "prevIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(prevScreen), for: .touchUpInside)
        return button
    }()
    
    let restaurantsTextField: UITextField = {
        let text = UITextField()
        text.attributedPlaceholder = NSAttributedString(string: "other", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)])
        text.font = UIFont(name: "Avenir-Black", size: 20)
        text.textAlignment = .center
        text.textColor = UIColor.white
        text.autocorrectionType = UITextAutocorrectionType.yes
        text.keyboardType = UIKeyboardType.default
        text.returnKeyType = UIReturnKeyType.done
        text.clearButtonMode = UITextField.ViewMode.whileEditing;
        text.translatesAutoresizingMaskIntoConstraints = false
        text.layer.cornerRadius = 19
        text.layer.borderWidth = 3
        text.layer.borderColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0).cgColor
        return text
    }()
    
    let barsTextField: UITextField = {
        let text = UITextField()
        text.attributedPlaceholder = NSAttributedString(string: "other", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)])
        text.font = UIFont(name: "Avenir-Black", size: 20)
        text.textAlignment = .center
        text.textColor = UIColor.white
        text.autocorrectionType = UITextAutocorrectionType.yes
        text.keyboardType = UIKeyboardType.default
        text.returnKeyType = UIReturnKeyType.done
        text.clearButtonMode = UITextField.ViewMode.whileEditing;
        text.translatesAutoresizingMaskIntoConstraints = false
        text.layer.cornerRadius = 19
        text.layer.borderWidth = 3
        text.layer.borderColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0).cgColor
        return text
    }()
    
    let barsIcon: UIImageView = {
        let icon = UIImageView(image: UIImage(imageLiteralResourceName: "drinkIcon"))
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    let restaurantsIcon: UIImageView = {
        let icon = UIImageView(image: UIImage(imageLiteralResourceName: "restaurantIcon"))
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    
    let endRouteButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "endRouteIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(endRoute), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    let currentStopview: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.shadowColor = UIColor(red:1, green:1, blue:1, alpha:0.4).cgColor
        view.layer.shadowRadius = 10
        view.layer.shadowOffset = CGSize.zero
        view.layer.cornerRadius = 7
        view.alpha = 0.85
        //view.layer.shouldRasterize = true
        return view
    }()
    
    let currentStopLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Black", size: 20)
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let currentStopButton : UIButton = {
        let button = UIButton()
        button.contentMode = UIView.ContentMode.scaleAspectFill// scaleAspectFit
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let nextStopButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "nextStopIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(nextStop), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    let checkinButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "checkInButton"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(checkIn), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    let callButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "callButton"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(callLocation), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    let walkButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "walkIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(directions), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    let timeButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(directions), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        return button
    }()
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoPoints(point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
    
    @objc func nextStop() {
        print("next")
        for (num,stop) in (currentRoute?.RouteOrder)! {
            if stop.Name == currentStop?.Name && num < (currentRoute?.Route.count)! {
                currentStop = currentRoute?.RouteOrder![num + 1]
                currentRoute?.CurrentStop = currentStop
                removeCurrentStopDetails()
                mapView.clear()
                currentRoute?.showRouteMarkers(onRoute: true, currentLocation: locationManager.location?.coordinate)
                
                routeFromLocation()
                addCurrentStopDetails()
                
                // move map to desired location
                let startLoc = locationManager.location!
                let endLoc2D = currentStop?.Location
                let endLoc = CLLocation(latitude: (endLoc2D?.latitude)!, longitude: (endLoc2D?.longitude)!)
                let bearing = getBearingBetweenTwoPoints(point1: startLoc, point2: endLoc)
                mapView.camera = GMSCameraPosition.camera(withTarget: locationManager.location!.coordinate, zoom: 17)
                mapView.animate(toViewingAngle: 45)
                mapView.animate(toBearing: bearing)
                
                if num == (currentRoute?.Route.count)!-1 {
                    nextStopButton.isEnabled = false
                }
                
                break
            }

            
        }
    }
    
    @objc func checkIn() {

        self.tabBarController?.selectedIndex = 3
    }
    
    @objc func callLocation() {
        print("calling")
        let stopYelpInfo = currentStop?.YelpInfo
        let phone = stopYelpInfo!["display_phone"].stringValue
        let callNumber = String((phone.filter { "01234567890.".contains($0) }))
        guard let number = URL(string: "tel://" + callNumber) else { return }
        print(number)
        UIApplication.shared.open(number)
        
    }
    
    func routeFromLocation() {
        //draws from path from locaiton to current stop
        let startLoc = locationManager.location
        let endLoc2D = currentStop?.Location
        let endLoc = CLLocation(latitude: (endLoc2D?.latitude)!, longitude: (endLoc2D?.longitude)!)
        currentRoute?.drawPath(route: currentRoute!, startLocation: startLoc!, endLocation: endLoc, mapView: (currentRoute?.MapView)!, onRoute: true, current:true)
        
//        timeButton.setTitle(currentRoute?.timeToNextStop, for: .normal)
//        currentRoute?.MapView.reloadInputViews()
        let path = combineRoutesForFrame(Routes: [currentRoute!], onRoute: true)
        path.add((startLoc?.coordinate)!)
        let bounds = GMSCoordinateBounds(path:path)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
    }
    
    func timeToNextStop() {
        timeButton.setTitle(currentRoute?.timeToNextStop, for: .normal)
        currentRoute?.MapView.reloadInputViews()
    }
    
    
    @objc func directions(){
        print("directions")
    }
    
}


extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if routeType == routeTypes.None {
            let location = locations.last
            
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude:(location?.coordinate.longitude)!, zoom:14)
            mapView.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            self.locationManager.stopUpdatingLocation()
        }
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}






