//
//  routeClass.swift
//  DateRoute
//
//  Created by Cameron Korb on 4/21/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import Alamofire
import AlamofireImage

class dateRoute {
    
    var Name: String?
    var Route = [RouteStop]()
    var RouteOrder: [Int:RouteStop]?
    var Path = GMSMutablePath()
    var Polyline = [GMSPolyline]()
    var MapView = GMSMapView()
    var Color: (UIColor,String)?
    var CurrentStop: RouteStop?
    var OnRoute = false
    var timeToNextStop: String?
    var Markers = [GMSMarker]()
    let homeViewController: HomeViewController?
    
    var alamoFireManager = Alamofire.SessionManager()
    
    
    init(name:String, order: [Int:RouteStop], path:GMSMutablePath, mapView:GMSMapView, color:(UIColor,String), line:[GMSPolyline], viewController:HomeViewController) {
        
        Name = name
        RouteOrder = order
        for (_, stop) in order {
            self.Route.append(stop)
        }
        Path = path
        MapView = mapView
        Color = color
        Polyline = line
        homeViewController = viewController
    }
    
    func getImages() {
        for stop in Route{
            stop.getImage()
        }
    }
    
    func showRouteMarkers(onRoute:Bool, currentLocation:CLLocationCoordinate2D?) -> [CLLocationCoordinate2D] {
        var allRoutePath = [CLLocationCoordinate2D]()
        for stop in Route {
            let marker = GMSMarker(position: stop.Location!)
            marker.title = stop.Name! + "<>" + self.Name!
            let image_string = (self.Color?.1)! + "_map_marker"
            if onRoute {
                if CurrentStop?.Name != stop.Name {
                    marker.icon = #imageLiteral(resourceName: "onRoute_map_marker_not_selected")
                } else {
                    marker.icon = #imageLiteral(resourceName: "onRoute_map_marker")
                }
            } else {
                marker.icon = UIImage(imageLiteralResourceName: image_string)
            }
            
            marker.map = self.MapView
            self.Path.add(marker.position)
            allRoutePath.append(marker.position)
            Markers.append(marker)
            stop.Marker = marker
        }
        if (onRoute && (currentLocation != nil)){
            allRoutePath.append(currentLocation!)
        }

        if Route.count == 1 {
            // no need to draw the path if it's just one stop
            return allRoutePath
        }

        print(RouteOrder)
        for i in 1...((self.RouteOrder!.count)-1) { // (routeStopOrder[routeName]?.count)! - 2)
                let start = self.RouteOrder![i]
                let startLocation = start?.Location
                let startLong = startLocation?.longitude
                let startLat = startLocation?.latitude
                let startLoc = CLLocation(latitude: startLat!, longitude: startLong!)
            
                let end = self.RouteOrder![i+1]
                let endLocation = end?.Location
                let endLong = endLocation?.longitude
                let endLat = endLocation?.latitude
                let endLoc = CLLocation(latitude: endLat!, longitude: endLong!)
                self.drawPath(route: self, startLocation: startLoc, endLocation: endLoc, mapView: self.MapView, onRoute: onRoute, current: false)
            }
        return allRoutePath
    }
    
    
    
    func drawPath(route:dateRoute, startLocation: CLLocation, endLocation: CLLocation, mapView: GMSMapView, onRoute:Bool, current:Bool) {
        CurrentStop = RouteOrder?[1]
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=walking&key=AIzaSyDnAnzQJTpnCEfAwOnjVGwBubEnSS0mClQ"
        
        Alamofire.request(url).responseJSON { response in
            let json = JSON(response.data!)
            let lines = json["routes"].arrayValue
            
            if onRoute {
                self.timeToNextStop = lines[lines.endIndex-1]["legs"][lines.endIndex-1]["duration"]["text"].stringValue
                self.homeViewController?.timeToNextStop()
            }
            
            if lines.count > 0 {
                let line = lines[0]
                //self.timeToNextStop = line["legs"][0]["duration"]["text"].stringValue
                
                let routeOverviewPolyline = line["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                for i in 0...Int(bitPattern: (path?.count())!) {
                    route.Path.add((path?.coordinate(at: UInt(bitPattern: i)))!)
                }
                let polyline = GMSPolyline.init(path: path)
                polyline.strokeWidth = 7
                if onRoute  {
                    polyline.strokeWidth = 5
                    if current {
                        polyline.strokeColor = UIColor(red: 62/255, green: 130/255, blue: 247/255, alpha: 1.0)
                    } else {
                        polyline.strokeColor = UIColor(red: 62/255, green: 130/255, blue: 247/255, alpha: 0.4)
                        // //This is dashed lines
                        //let styles = [GMSStrokeStyle.solidColor(.clear), GMSStrokeStyle.solidColor(UIColor(red: 62/255, green: 130/255, blue: 247/255, alpha: 1.0))]
                        //let lengths: [NSNumber] = [10, 10]
                        //polyline.spans =  GMSStyleSpans(path!, styles, lengths, GMSLengthKind.rhumb)
                    }
                } else {
                    polyline.strokeColor = (route.Color?.0)!
                }
                polyline.title = route.Name
                polyline.isTappable = true
                polyline.map = self.MapView
                route.Polyline.append(polyline)
            }
        }
        
    }
    
    func centerMapOnRoute(){
        let bounds = GMSCoordinateBounds(path:self.Path)
        //MapView.animate(toZoom: 12)
        //print(bounds)
        self.MapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
    }
    
    func getRouteForDateBase() -> [String:String]{
        var route = [String:String]()
        for (num,stop) in self.RouteOrder! {
            route[String(num)] = stop.Name! + "&id:" + stop.Id!
        }
        return route
    }
    
    
}
