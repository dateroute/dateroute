////
////  favoritesViewController.swift
////  DateRoute
////
////  Created by Vedaant Paul Kukadia  on 1/14/18.
////  Copyright © 2018 DateRoute. All rights reserved.
////
//
import UIKit
import FirebaseFirestore
import Firebase
import GoogleMaps
import Alamofire
import SwiftyJSON


class favoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    var tableView = UITableView()
    var Favorites = [[String:String]]()
    var ref = Database.database().reference()
    var alamoFireManager = Alamofire.SessionManager()

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Favorites.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier") as! favoritesCell

        let route = Favorites[indexPath.row]
        cell.routeName.text = route["title"]
        cell.stops.text = route["stops"]
        //cell.imageView?.image = UIImage(named: headline.image)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            print("deleting")
            print(self.Favorites[indexPath.row]["title"]!)
            
            var uniqueID = self.Favorites[indexPath.row]["title"]!
            
            guard let UserID = Auth.auth().currentUser?.uid else { return }
            
            self.ref.child("users").child(UserID).observeSingleEvent(of: .value, with: { (snapshot) in
                guard let dictionary = snapshot.value as? [String: Any] else { return }
                guard let favorites = dictionary["favorites"] as? [String: Any] else { return }
                for (key, _) in favorites {
                    let title = key.components(separatedBy: "&id:")[0]
                    if title == uniqueID{
                        uniqueID = key
                        print(key)
                        
                        //remove from firebase
                        self.ref.child("users").child(UserID).child("favorites").child(uniqueID).removeValue()
                        print("removed")
                        break
                    }
                }
            }){ (err) in
                print("Failed to identify favorite ID", err)
            }
            self.Favorites.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
        
        let rename = UITableViewRowAction(style: .normal, title: "Rename") { (action, indexPath) in
            
            let alert = UIAlertController(title: "Rename your route", message: "", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.text = self.Favorites[indexPath.row]["title"]!
            }
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                
                var oldTitle = self.Favorites[indexPath.row]["title"]!
                guard let UserID = Auth.auth().currentUser?.uid else { return }
                
                self.ref.child("users").child(UserID).observeSingleEvent(of: .value, with: { (snapshot) in
                guard let dictionary = snapshot.value as? [String: Any] else { return }
                guard let favorites = dictionary["favorites"] as? [String: Any] else { return }
                for (key, value) in favorites {
                    let title = key.components(separatedBy: "&id:")[0]
                    let id = key.components(separatedBy: "&id:")[1]
                    print(title)
                    if (title == oldTitle) && (title != textField?.text){
                        print(key)
                    self.ref.child("users").child(UserID).child("favorites").updateChildValues([textField!.text!+"&id:"+id : value])
                        // remove old
                        self.ref.child("users").child(UserID).child("favorites").child(key).removeValue()
                        print("updated")
                        break
                    }
                }
            }){ (err) in
                print("Failed to identify favorite ID", err)
            }
                self.Favorites[indexPath.row]["title"] = textField?.text
                self.tableView.reloadData()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        rename.backgroundColor = UIColor.orange
        
        return [delete, rename]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let homeController = HomeViewController()
        
        let routeName = Favorites[indexPath.row]["title"]!
        let stops: [String] = Favorites[indexPath.row]["ids"]!.components(separatedBy: " | ")
        var route = [RouteStop]()
        var order = [Int:RouteStop]()
        
        let path = GMSMutablePath()
        let mapView = homeController.mapView
        let color = (UIColor(red:119/255, green:162/255, blue:187/255, alpha:1.0),"blue")
        for i in 0...(stops.count-1) {
            let accessToken = "3Zdg7kl2psI3NWUEfmHuFl6Vqy-qz3aOHxpR54lHnnq9M0ELW2vad_Ht1wA5-IwB1VpnnEm3lU51JwdjVxLdgMaTFKekCNEOVsT2AT7gsw2RdSltPU8GxNIuQPn4WnYx"
            let headers: HTTPHeaders = ["Authorization":"Bearer \(accessToken)"]
            let id = stops[i]
            let url = "https://api.yelp.com/v3/businesses/\(id)"
            
            alamoFireManager.request(url, headers : headers).responseJSON { response in
                let json = JSON(response.data!)
                
                let name = json["name"].stringValue
                let id = json["alias"].stringValue
                let address = json["location"]["display_address"].stringValue
                let lat = json["coordinates"]["latitude"].floatValue
                let long = json["coordinates"]["longitude"].floatValue
                let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                var categories = [String]()
                for (_, category) in json["categories"] {
                    let type = category["alias"].stringValue
                    categories.append(type)
                }
                let rating = json["rating"].stringValue
                let phone = json["phone"].stringValue
                let imageUrl = json["image_url"].stringValue
                let url = json["url"].stringValue
                let routeStop = RouteStop(Name: name, Id: id, YelpInfo: json, Address: address, location:location, Categories: categories, Rating: rating, Phone: phone, ImageURL: imageUrl, Url: url, image: nil, marker:nil)
                route.append(routeStop)
                order[i+1] = routeStop
                
                if route.count == stops.count {
                    let selectedRoute = dateRoute(name: routeName, order: order, path: path, mapView: mapView, color: color, line: [GMSPolyline](), viewController: homeController)
                    
                    homeController.ROUTES = [selectedRoute]
                    homeController.routeType = HomeViewController.routeTypes.Featured
                    self.navigationController?.pushViewController(homeController, animated: true)
                    //self.transitioning = false
                    
                }
                else {
                    print("DIDNT FINISH FETCHING, missing:", id)
                    if i == stops.count-1 {
                        print("last item")
                    }
                }
            }
        }
        
    }

    

    @objc func handleEditButton(){
        if(self.tableView.isEditing == true)
        {
            self.tableView.isEditing = false
            self.navigationItem.rightBarButtonItem?.title = "Edit"
        }
        else
        {
            self.tableView.isEditing = true
            self.navigationItem.rightBarButtonItem?.title = "Done"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

       UIApplication.shared.statusBarStyle = .default
        
        tableView = UITableView(frame: self.view.bounds, style: UITableView.Style.plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = UIColor.white
        
        tableView.register(favoritesCell.self, forCellReuseIdentifier: "cellReuseIdentifier")
        tableView.rowHeight = 80
        view.addSubview(tableView)
        fetchFavorites()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)
        self.navigationController?.navigationBar.isTranslucent = false
        navigationItem.title = "Favorites"
        let label = UILabel(frame: CGRect(x: view.center.x, y: view.center.y+66, width: 97, height: 33))
        label.font = UIFont(name: "Avenir-Heavy", size: 24)
        label.text = "Favorites"
        label.textAlignment = .center
        label.textColor = UIColor(red:1, green:1, blue:1, alpha:1.0)
        navigationItem.titleView = label
        
        let button = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(handleEditButton))
        button.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 18)!, NSAttributedString.Key.foregroundColor: UIColor(red:1, green:1, blue:1, alpha:1)], for: .normal)
        navigationItem.rightBarButtonItem = button
        
        print("getting data from firebase...")
        
        //Set up table view
        fetchFavorites()

    }

    
    fileprivate func fetchFavorites(){
        
        print("fetching USER")
        guard let UserID = Auth.auth().currentUser?.uid else { return }
        
        self.ref.child("users").child(UserID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            guard let favorites = dictionary["favorites"] as? [String: Any] else { return }
        
            self.Favorites = []
            var titlesList = [String]()
            var titleToRouteInfo = Dictionary<String, Dictionary<String, String>>()
            for (key, value) in favorites {
                let title = key.components(separatedBy: "&id:")[0]
                let stops = (value as! NSMutableArray)
                stops.removeObject(identicalTo: NSNull())
                var stopNames = ""
                var ids = ""
                for stop in stops {
                    stopNames = stopNames + (stop as! String).components(separatedBy: "&id:")[0] + " | "
                    ids = ids + (stop as! String).components(separatedBy: "&id:")[1] + " | "
                }
                let dict = ["title":title, "stops":String(stopNames.dropLast(3)), "ids":String(ids.dropLast(3))]
                titlesList.append(title)
                titleToRouteInfo[title] = dict
            }
            // Sort the routes alphabetically by title and add to Favorites in that order
            titlesList.sort()
            for title in titlesList {
                self.Favorites.append(titleToRouteInfo[title]!)
            }

            self.tableView.reloadData()
        }){ (err) in
            print("Failed to fetch favorites", err)
        }
    }
}


