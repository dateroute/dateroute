//
//  stopClass.swift
//  DateRoute
//
//  Created by Cameron Korb on 5/24/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import GoogleMaps

class RouteStop {
    
    var Name: String?
    var Id: String?
    var YelpInfo: JSON?
    var Address: String?
    var Location: CLLocationCoordinate2D?
    var Categories: [String]?
    var Rating: String?
    var Phone: String?
    var ImageURL: String?
    var Url: String?
    var Image: UIImage?
    var Marker: GMSMarker?
    
    var alamoFireManager = Alamofire.SessionManager()
    
    init(Name:String, Id:String, YelpInfo: JSON, Address:String, location: CLLocationCoordinate2D?, Categories:[String], Rating:String, Phone:String, ImageURL:String?, Url:String, image:UIImage?, marker:GMSMarker?) {
        self.Name = Name
        self.Id = Id
        self.YelpInfo = YelpInfo
        self.Address = Address
        self.Location = location
        self.Categories = Categories
        self.Rating = Rating
        self.Phone = Phone
        self.ImageURL = ImageURL
        self.Url = Url
        self.Image = image
        self.Marker = marker
    }
    
    func getImage() {
        if self.ImageURL != nil {
            self.alamoFireManager.request(self.ImageURL!).responseImage(completionHandler: { (response) in
                if let stopImage = response.result.value {
                    //print("image downloaded: \(stopImage)")
                    self.Image = stopImage
                    
                }
            })
        }
    }
    
}
