//
//  LogInViewController.swift
//  DateRoute
//
//  Created by Cameron Korb on 10/16/17.
//  Copyright © 2017 DateRoute. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import FirebaseDatabase

class ViewController: UIViewController {
    
    var imageURL = ""
    var DOB = ""
    var Home = ""
    var login = false

    
    let appName: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Black", size: 50)
        label.text = "dateroute"
        label.textColor = UIColor(red:1, green:1, blue:1, alpha:1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    
    let agreeLabel: UILabel = {
        let agreeLabel = UILabel()
        agreeLabel.font = UIFont(name: "Avenir-Medium", size: 12)
        agreeLabel.text = "We don't post anything to Facebook. By signing in, you agree to our Terms of Service and Privacy Policy"
        //agreeLabel.lineBreakMode = .byWordWrapping // notice the 'b' instead of 'B'
        agreeLabel.numberOfLines = 3
        agreeLabel.textAlignment = .center
        agreeLabel.adjustsFontSizeToFitWidth = true
        agreeLabel.textColor = UIColor(red:112/255, green:112/255, blue:112/255, alpha:1.0)
        agreeLabel.translatesAutoresizingMaskIntoConstraints = false
        agreeLabel.textAlignment = .center
        return agreeLabel
    }()
    
    
    let logo: UIImageView = {
        let logo = UIImageView()
        logo.image = #imageLiteral(resourceName: "homeLogo")
        logo.translatesAutoresizingMaskIntoConstraints = false
        return logo
    }()
    
    
    let loginButton: UIButton = {
        let loginButton = UIButton()
        loginButton.setImage(#imageLiteral(resourceName: "facebook login.png").withRenderingMode(.alwaysOriginal), for: .normal)
        loginButton.setImage(#imageLiteral(resourceName: "facebook login.png").withRenderingMode(.alwaysOriginal), for: .highlighted)
        loginButton.addTarget(self, action: #selector(handleLoginPress), for: .touchUpInside)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        return loginButton
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (FBSDKAccessToken.current() != nil) {
            let loginViewController = MainTabBarController()
            self.present(loginViewController, animated: false, completion: nil)
            print("Login Succesful")
        }
        
        
        navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = UIColor(red:119/255, green:162/255, blue:187/255, alpha:1.0)
        
        view.addSubview(logo)
        logo.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -40).isActive = true
        logo.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        
        view.addSubview(appName)
        appName.bottomAnchor.constraint(equalTo: logo.topAnchor, constant: -15).isActive = true
        appName.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        appName.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        
        view.addSubview(loginButton)
        loginButton.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 35).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        
        view.addSubview(agreeLabel)
        agreeLabel.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 10).isActive = true
        agreeLabel.leftAnchor.constraint(equalTo: loginButton.centerXAnchor, constant: -1*(loginButton.imageView?.frame.width)!/2).isActive = true
        agreeLabel.rightAnchor.constraint(equalTo: loginButton.centerXAnchor, constant: (loginButton.imageView?.frame.width)!/2).isActive = true
    }
    
    
    @objc func handleLoginPress(){
        
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, err) in
            if err != nil {
                print("Custom FB Login failed:", err as Any)
                return
            } else {
                                
                
                if (FBSDKAccessToken.current() != nil){
                    self.showEmailAddress()
                    let loginViewController = MainTabBarController()
                    //
                    self.present(loginViewController, animated: false, completion: nil)
                    print("Login Succesful")
                    
                }
                else {
                    print("User did not login")
                }
            }
        }
        
        
        
        
       
        
    }
    
    //Grabs the users email address from Facebook
    func showEmailAddress() {
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else {return}
        
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        
        Auth.auth().signInAndRetrieveData(with: credentials, completion: { (user, error) in
            if error != nil {
                print("FB User wasn't logged in to Firebase: ", error as Any)
                
            } else {
                
                FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start { (connection, result, err) in
                    if err != nil {
                        print("Failed to start graph request:", err as Any)
                        return
                    }
                    
                    print(result as Any)
                    
                    guard let UserID = Auth.auth().currentUser?.uid else { return }
                    
                    let data = result as! [String : AnyObject]
                    
                    let FBid = data["id"] as! String
                    
                    //initialize the email, profile picture and name
                    var userEmail: String
                    if data["email"] != nil {
                        userEmail = data["email"] as! String
                    } else {
                        userEmail = "click to add"
                    }
                    var username = data["name"] as Any
                    
                    // first time log ons need to create a new folder
                    Database.database().reference().child("users").updateChildValues([UserID+"/temp/": "temp"])
                    
                    // now populate with other data
                    Database.database().reference().child("users").child(UserID).observeSingleEvent(of: .value, with: { (snapshot) in
                        print(snapshot.value ?? "")
                        
                        guard let dictionary = snapshot.value as? [String: Any] else { return }
                        print("DICTIONARY", dictionary)
                        
                        if dictionary["username"] != nil{
                            username = dictionary["username"] as Any
                        } else{
                            username = data["name"] as Any
                        }
                        
                        if dictionary["profileImageUrl"] != nil && dictionary["profileImageUrl"] as! String != ""{
                            print("2.1")
                            self.imageURL = dictionary["profileImageUrl"] as! String
                        } else{
                            print("2.2")
                            let url1 = "https://graph.facebook.com/\(FBid)/picture?type=large&return_ssl_resources=1"
                            Database.database().reference().child("users").child(UserID).updateChildValues(["profileImageUrl" : url1])
                            self.imageURL = url1
                        }
                        
                        if dictionary["Date of Birth"] != nil && dictionary["Date of Birth"] as! String != "MM/DD/YYYY"{
                            print("2.3")
                            self.DOB = dictionary["Date of Birth"] as! String
                        } else{
                            Database.database().reference().child("users").child(UserID).updateChildValues(["Date of Birth" : "MM/DD/YYYY"])
                            self.DOB = "MM/DD/YYYY"
                        }
                        
                        if dictionary["Hometown"] != nil && dictionary["Hometown"] as! String != "click to add"{
                            print("2.3")
                            self.Home = dictionary["Hometown"] as! String
                        } else{
                            Database.database().reference().child("users").child(UserID).updateChildValues(["Hometown" : "click to add"])
                            self.Home = "click to add"
                        }
                        
                        
                        
                        if dictionary["email"] != nil{
                            userEmail = dictionary["email"] as! String
                        } else {
                            if data["email"] != nil {
                                userEmail = data["email"] as! String
                            } else {
                                userEmail = "click to add"
                            }
                        }
                        
                        
                        print("2.6")
                        self.login = true
                        
                        ///////////////////
                        //var dictionaryValues: [String: Any] = [:]
                        var dictionaryValues = ["username" : username, "email" : userEmail, "Date of Birth" : self.DOB, "Hometown" : self.Home, "profileImageUrl" : self.imageURL, "FBid" : FBid] as [String : Any]
                        
                        if dictionary["favorites"] != nil {
                            let favorites = dictionary["favorites"] as Any
                            dictionaryValues["favorites"] = favorites
                        }
                        
                        if dictionary["checkIns"] != nil {
                            let checkIns = dictionary["checkIns"] as Any
                            dictionaryValues["checkIns"] = checkIns
                        }
                        
                        print("2.9")
                        let values = [UserID: dictionaryValues]
                        print("2.10")
                        Database.database().reference().child("users").updateChildValues(values, withCompletionBlock: {(err, ref) in
                            
                            if let err = err {
                                print("failed to save user info into db:", err)
                                return
                                
                            }
                            
                            print("successfully saved user info into db")
                            
                        })
                        
                        ///////////////////
                        print("2.6.1")
                        
                        
                        
                    }){ (err) in
                        print("Failed to fetch user", err)
                    }
                    
                    print("2.6.2")
                    print("username", username)
                    print("2.6.3")
                    print("email", userEmail)
                    print("2.6.4")
                    print("photo", self.imageURL)
                    
                    print("2.7")
                    
                    
                }
                
                print("FB User successfully logged in to Firebase: ", user as Any)
                
            }
        })
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
