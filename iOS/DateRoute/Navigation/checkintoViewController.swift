//
//  checkintoViewController.swift
//  DateRoute
//
//  Created by Vedaant Paul Kukadia  on 6/8/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase


class checkintoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var stopName: String?
    var imageAdded = false
    var messageAdded = false
    
    //photo button
    let photoButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "add a photo"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleAddPhoto), for: .touchUpInside)
        return button
    }()
    
    //Checkin button
    let checkinButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "big check in button"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleCheckedin), for: .touchUpInside)
        return button
    }()
    
    // prompt
    let header: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Heavy", size: 20)
        label.numberOfLines = 2
        label.textColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //comment bubble
    let commentBubble: UIImageView = {
        let image = UIImageView() //frame: CGRect(x: 15, y: 23, width: 30, height: 30))
        image.image = #imageLiteral(resourceName: "commentBubble")
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    // caption text field
    let addCaption: UITextField = {
        let tf = UITextField()
        tf.placeholder = " Add a comment..."
        tf.font = UIFont(name: "Avenir-Medium", size: 14)
//        tf.backgroundColor = .white
        tf.addTarget(self, action: #selector(handleCaption), for: .editingDidEnd)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.textAlignment = .left
        tf.contentVerticalAlignment = .top
        return tf
    }()
    
    // star rating slider
    let starSlider: starRating = {
        let slider = starRating()
        return slider
    }()
    
    let profImageView: UIImageView = {
        let profImageView = UIImageView()
        profImageView.image = #imageLiteral(resourceName: "temp prof")
        profImageView.layer.masksToBounds = false
        profImageView.layer.cornerRadius = 30
        //profImageView.layer.cornerRadius = profImageView.frame.width/2
        profImageView.translatesAutoresizingMaskIntoConstraints = false
        profImageView.clipsToBounds = true
        return profImageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        guard let UserID = Auth.auth().currentUser?.uid else { return }
        var ref = Database.database().reference()
    
        ref.child("users").child(UserID).observeSingleEvent(of: .value, with: { (snapshot) in
            print(snapshot.value ?? "")
            
            print("getting in databse")
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            
            guard let imageURL = dictionary["profileImageUrl"] else { return }
            
            let url = NSURL(string: (imageURL as? String)!)
            
            self.profImageView.image = UIImage(data: NSData(contentsOf: url! as URL)! as Data)
            
        }){ (err) in
            print("Failed to fetch user", err)
        }
        
        
        
        view.backgroundColor = UIColor(red:1, green:1, blue:1, alpha:1.0)
        navigationController?.navigationBar.barTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Heavy", size: 24)
        label.text = "Current Stop"
        label.textColor = UIColor(red:1, green:1, blue:1, alpha:1.0)
        navigationItem.titleView = label
        
        
        // add and position UI elements
        header.text = "What did you think of " + stopName! + "?"
        view.addSubview(header)
        header.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 25, paddingBottom: 0, paddingRight: 25, width: 0, height: 80)
        
        view.addSubview(starSlider)
        starSlider.anchor(top: header.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        starSlider.centerXAnchor.constraint(equalTo: view.centerXAnchor ).isActive = true

        view.addSubview(commentBubble)
        commentBubble.anchor(top: starSlider.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 20, width: 0, height: 150)
        
        view.addSubview(photoButton)
        photoButton.anchor(top: commentBubble.topAnchor, left: nil, bottom: nil, right: commentBubble.rightAnchor, paddingTop: 12, paddingLeft: 0, paddingBottom: 0, paddingRight: 15, width: 90, height: 90)
        
        view.addSubview(addCaption)
        addCaption.anchor(top: commentBubble.topAnchor, left: commentBubble.leftAnchor, bottom: commentBubble.bottomAnchor, right: photoButton.leftAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 50, paddingRight: 10, width: 0, height: 0)
        
        view.addSubview(profImageView)
        profImageView.anchor(top: commentBubble.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: -5, paddingLeft: 15, paddingBottom: 0, paddingRight: 0, width: 60, height: 60)
        
        view.addSubview(checkinButton)
        checkinButton.topAnchor.constraint(equalTo: profImageView.bottomAnchor, constant: 10).isActive = true
        checkinButton.centerXAnchor.constraint(equalTo: view.centerXAnchor ).isActive = true
        
        //Add facebook switch
        let fbswitch: UISwitch = {
            let fbswitch = UISwitch()
            fbswitch.isOn = false
            fbswitch.onTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)
            fbswitch.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha:1.0)
            fbswitch.layer.cornerRadius = 16
            fbswitch.translatesAutoresizingMaskIntoConstraints = false
            return fbswitch
        }()
        
        //Add twitter switch
        let twswitch: UISwitch = {
            let twswitch = UISwitch()
            twswitch.isOn = false
            twswitch.onTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)
            twswitch.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha:1.0)
            twswitch.layer.cornerRadius = 16
            twswitch.translatesAutoresizingMaskIntoConstraints = false
            return twswitch
            
        }()
        
        
        //Add insta switch
        let instaswitch: UISwitch = {
            let instaswitch = UISwitch()
            instaswitch.isOn = false
            instaswitch.onTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)
            instaswitch.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha:1.0)
            instaswitch.layer.cornerRadius = 16
            instaswitch.translatesAutoresizingMaskIntoConstraints = false
            return instaswitch
        }()
        
        let switchWidth = CGFloat(61)
        view.addSubview(twswitch)
        twswitch.topAnchor.constraint(equalTo: addCaption.bottomAnchor, constant: 25).isActive = true
        twswitch.centerXAnchor.constraint(equalTo: view.centerXAnchor ).isActive = true
        
        view.addSubview(fbswitch)
        fbswitch.topAnchor.constraint(equalTo: addCaption.bottomAnchor, constant: 25).isActive = true
        fbswitch.leftAnchor.constraint(equalTo: view.leftAnchor, constant: (view.bounds.width - switchWidth)/8).isActive = true
        
        view.addSubview(instaswitch)
        instaswitch.topAnchor.constraint(equalTo: addCaption.bottomAnchor, constant: 25).isActive = true
        instaswitch.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 7*((view.bounds.width - switchWidth)/8)).isActive = true

        //Add the facebook icon
        let fb: UIImageView = {
            let iv = UIImageView(image: #imageLiteral(resourceName: "fb"))
            iv.contentMode = .scaleAspectFill
            iv.clipsToBounds = true
            iv.translatesAutoresizingMaskIntoConstraints = false
            iv.backgroundColor = UIColor.clear
            iv.isOpaque = false
            return iv
        }()
        
        view.addSubview(fb)
        fb.topAnchor.constraint(equalTo: addCaption.bottomAnchor, constant: 22).isActive = true
        fb.rightAnchor.constraint(equalTo: fbswitch.leftAnchor).isActive = true
        
        //add the twitter icon
        let tw: UIImageView = {
            let iv = UIImageView(image: #imageLiteral(resourceName: "twitter"))
            iv.contentMode = .scaleAspectFill
            iv.clipsToBounds = true
            iv.translatesAutoresizingMaskIntoConstraints = false
            return iv
        }()
        
        view.addSubview(tw)
        tw.topAnchor.constraint(equalTo: addCaption.bottomAnchor, constant: 27).isActive = true
        tw.rightAnchor.constraint(equalTo: twswitch.leftAnchor, constant: -6).isActive = true
        
        //add the insta icon
        let insta: UIImageView = {
            let iv = UIImageView(image: #imageLiteral(resourceName: "insta"))
            iv.contentMode = .scaleAspectFill
            iv.clipsToBounds = true
            iv.translatesAutoresizingMaskIntoConstraints = false
            return iv
        }()
        
        view.addSubview(insta)
        insta.topAnchor.constraint(equalTo: addCaption.bottomAnchor, constant: 18).isActive = true
        insta.rightAnchor.constraint(equalTo: instaswitch.leftAnchor).isActive = true
        
        // temporarily setting these as hidden for the beta version
        fbswitch.isHidden = true
        twswitch.isHidden = true
        instaswitch.isHidden = true
        fb.isHidden = true
        tw.isHidden = true
        insta.isHidden = true
        
        
        
        
    }
    
    @objc func handleCheckedin(){
        print("Checked in")
        
        DispatchQueue.main.async {
            
            let savedLabel = UILabel()
            savedLabel.text = "Checked in!"
            savedLabel.font = UIFont.boldSystemFont(ofSize: 18)
            savedLabel.textColor = .white
            savedLabel.numberOfLines = 0
            savedLabel.backgroundColor = UIColor(white: 0, alpha: 0.3)
            savedLabel.textAlignment = .center
            
            savedLabel.frame = CGRect(x: 0, y: 0, width: 150, height: 80)
            savedLabel.center = self.view.center
            
            self.view.addSubview(savedLabel)
            
            savedLabel.layer.transform = CATransform3DMakeScale(0, 0, 0)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                
                savedLabel.layer.transform = CATransform3DMakeScale(1, 1, 1)
                
            }, completion: { (completed) in
                //completed
                
                UIView.animate(withDuration: 0.5, delay: 0.75, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    
                    savedLabel.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.1)
                    savedLabel.alpha = 0
                    
                }, completion: { (_) in
                    
                    savedLabel.removeFromSuperview()
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
                
            })
            
        }
        
        
        //handle the part for saving to firebase
        guard let image = self.photoButton.image(for: .normal) else {
            return
        }
        guard let uploadData = image.jpegData(compressionQuality: 0.3) else{
            return
        }
        
        //let yelp_id = "test-yelp-id"
        let name = stopName
        var message = "None"
        var checkedinImageUrl = "None"
        var rating = starSlider.rating
        
        // get identifier information
        guard let UserID = Auth.auth().currentUser?.uid else { return }
        let ref: DatabaseReference = Database.database().reference()
        let key = ref.child("check ins").childByAutoId().key
        
        if messageAdded==true {
            message = addCaption.text!
        }
        
        //TODO: add address and yelp id if it exists

        if imageAdded==true {
            let filename = UserID + "&" + key
            checkedinImageUrl = "gs://dateroute-fc903.appspot.com/checkedin_images/" + filename

            Storage.storage().reference().child("checkedin_images").child(filename).putData(uploadData, metadata: nil) { (metadata, err) in

                if let err = err {
                    print("failed to upload profile image: ", err)
                    return
                }
                }
        }
            

            //Save the new profile picture to Firebase
        let post = ["name" : name, "message" : message, "image" : checkedinImageUrl, "rating" : rating] as [String : Any]
            let childUpdates = ["/users/\(UserID)/checkIns/\(key)/": post]
            ref.updateChildValues(childUpdates)

            print("Successfully uploaded checkin data!")


        
        //Reset checkin image after successfull checkin
        Variables.checkinImage = #imageLiteral(resourceName: "add a photo")
        
    }
    
    @objc func handleAddPhoto(){
        print("adding photo")
        imageAdded = true
        
        let cameraController = CameraController()
        present(cameraController, animated: true, completion: nil)
    }
    


    
    @objc func handleCaption(){
        print("adding caption")
        messageAdded = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        print("Yes, reloading checkinto page")
        photoButton.setImage(Variables.checkinImage, for: .normal)
        
    }
    
    
    
    
}
