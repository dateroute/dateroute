//
//  PreviewPhotoContainerView.swift
//  DateRoute
//
//  Created by Vedaant Paul Kukadia  on 6/9/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import Photos

class StoryContainerView: UIView {
    
    let viewWidth = checkinViewController().view.bounds.width
    let storyPhotosLength = Variables.checkinStory.count
    var currentIndex = 0

    let previewImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "cancel button"), for: .normal)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return button
        
    }()
    
    let rightButton: UIButton = {
        let button = UIButton(type: .system)
//        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(handleShiftRight), for: .touchUpInside)
        return button
    }()
    
    let leftButton: UIButton = {
        let button = UIButton(type: .system)
//        button.backgroundColor = .green
        button.addTarget(self, action: #selector(handleShiftLeft), for: .touchUpInside)
        return button
    }()
    
    @objc func handleShiftRight(){
        if currentIndex < (storyPhotosLength-1){
            currentIndex += 1
            previewImageView.image = Variables.checkinStory[currentIndex]
        } else {
            print("at end of list")
            self.removeFromSuperview()
        }
        
        print("shifting image to right")
    }
    
    @objc func handleShiftLeft(){
        
        if currentIndex == 0 {
            print("at beginning of list")
        } else {
            currentIndex -= 1
            previewImageView.image = Variables.checkinStory[currentIndex]
        }
        
        print("shifting image to left")
    }
    
    
    
    
    @objc func handleCancel(){
        print("handling cancel button")
        self.removeFromSuperview()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .yellow
        
        addSubview(previewImageView)
        previewImageView.anchor(top: topAnchor , left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addSubview(rightButton)
        rightButton.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: viewWidth/2, paddingBottom: 0, paddingRight: 0, width:0, height: 0)
        
        addSubview(leftButton)
        leftButton.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: viewWidth/2, width:0, height: 0)
        
        addSubview(cancelButton)
        cancelButton.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 12, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

