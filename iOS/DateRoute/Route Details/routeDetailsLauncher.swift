//
//  routeDetailsLauncher.swift
//  DateRoute
//
//  Created by Cameron Korb on 1/30/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseDatabase

class routeDetailsLauncher: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var homeController: HomeViewController?
    var screenWidth : CGFloat!
    var screenHeight : CGFloat!
    var mapHeight : CGFloat!
    var mapView : GMSMapView!
    var tabBarHeight : CGFloat!
    var route: dateRoute?
    var routeName : String!
    var Color: (UIColor,String)?
    
    let mapCoverView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let height : CGFloat = 170
    let spaceFromEdge: CGFloat = 22.5
    let cellWidth : CGFloat = (UIApplication.shared.keyWindow?.frame.width)! * 4/7
    
    let flowLayout : UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        return layout
    }()
    
    let routeHeaderView: UIView = {
        let view = UIView()
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: -2)
        view.layer.shadowRadius = 1
        //view.isUserInteractionEnable
        return view
    }()
    
    let cellId = "routeDetailsID"
    let headerId = "routeDetailsHeaderID"
    let headerHeight : CGFloat = 50
    
    var SelectedMarker : GMSMarker?
    
    let routeDetailCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: (UIApplication.shared.keyWindow?.frame.width)! * 4/7, height: 100)
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsets.init(top: 50, left: 0, bottom: 20, right: 0)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    let routeTitleView: UIView = {
        let view = UIView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 9
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let routeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont(name: "Avenir-Heavy", size: 32)
        label.textAlignment = .center
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let startRouteButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "startRouteIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        //for some reasons target needs to be added in cell creator
        //button.addTarget(self, action: #selector(startRoute), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    let saveButton: UIButton = {
        let button = UIButton()
        //button.setImage(#imageLiteral(resourceName: "saveIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        //for some reasons target needs to be added in cell creator
        //button.addTarget(self, action: #selector(saveRoute), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    
    let shareButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "shareIcon"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        //for some reasons target needs to be added in cell creator
        //button.addTarget(nil, action: #selector(shareRoute), for: .touchUpInside)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        return button
    }()
    
    
    
    func displayRouteDetails(homeController: HomeViewController, route : dateRoute, routeName : String, screenWidth: CGFloat, screenHeight: CGFloat, mapHeight: CGFloat, mapView : GMSMapView, tabBarHeight : CGFloat, color: (UIColor,String)) {
        self.homeController = homeController
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.mapHeight = mapHeight
        self.mapView = mapView
        self.route = route
        self.tabBarHeight = tabBarHeight
        self.routeName = routeName
        self.routeDetailCollectionView.backgroundView?.backgroundColor = color.0
        self.routeDetailCollectionView.backgroundColor = color.0
        self.routeTitleView.backgroundColor = color.0
        self.Color = color
        self.mapView.selectedMarker = nil
        let saveButtonImageString = (route.Color?.1)! + "_saveIcon"
        saveButton.setImage(UIImage(imageLiteralResourceName: saveButtonImageString), for: .normal)
        
        showRouteDetails()
        addRouteHeading()
    }
    
    func addRouteHeading() {
        self.routeHeaderView.addSubview(startRouteButton)
        startRouteButton.leftAnchor.constraint(equalTo: routeHeaderView.leftAnchor, constant: 10.0).isActive = true
        startRouteButton.bottomAnchor.constraint(equalTo: routeHeaderView.bottomAnchor, constant: 0).isActive = true
        
        if (route?.OnRoute)! {
            startRouteButton.isEnabled = false
        }
        
        self.routeHeaderView.addSubview(shareButton)
        shareButton.topAnchor.constraint(equalTo: routeHeaderView.topAnchor, constant: 10).isActive = true
        shareButton.rightAnchor.constraint(equalTo: routeHeaderView.rightAnchor, constant: -10).isActive = true
        shareButton.bottomAnchor.constraint(equalTo: routeHeaderView.bottomAnchor, constant: 0).isActive = true
        
        self.routeHeaderView.addSubview(saveButton)
        saveButton.topAnchor.constraint(equalTo: routeHeaderView.topAnchor, constant: 10).isActive = true
        saveButton.rightAnchor.constraint(equalTo: shareButton.leftAnchor, constant: -40).isActive = true
        saveButton.bottomAnchor.constraint(equalTo: routeHeaderView.bottomAnchor, constant: 0).isActive = true
        
    }
    
    func showRouteDetails() {
        if let window = UIApplication.shared.keyWindow {
            homeController?.routeDetailsVisible = true
            customBackButtonBehavior()
            
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
            
            let y = self.mapHeight - height
            // This is what shows the scroll view of the route names at the bottom
            window.addSubview(routeDetailCollectionView)
            routeDetailCollectionView.frame = CGRect(x: 0, y:self.mapHeight, width: screenWidth!, height: height)
            window.addSubview(routeHeaderView)
            routeHeaderView.backgroundColor = self.Color?.0
            routeHeaderView.frame = CGRect(x: 0, y: self.mapHeight, width: screenWidth!, height: headerHeight)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.routeDetailCollectionView.frame = CGRect(x: 0, y: y, width: self.routeDetailCollectionView.frame.width, height: self.routeDetailCollectionView.frame.height)
                
                self.mapView.frame = CGRect(x:0, y:0, width: self.screenWidth!, height: self.screenHeight! - self.height)
                self.routeHeaderView.frame = CGRect(x: 0, y: y, width: self.screenWidth!, height: self.headerHeight)
                
                let bounds = GMSCoordinateBounds(path:(self.route?.Path)!)
                self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
                
                if (self.route?.OnRoute)! {
                    self.homeController?.endRouteButton.removeFromSuperview()
                    self.route?.MapView.addSubview((self.homeController?.endRouteButton)!)
                    self.homeController?.endRouteButton.bottomAnchor.constraint(equalTo: (self.route?.MapView.safeAreaLayoutGuide.bottomAnchor)!, constant: -7).isActive = true
                    self.homeController?.endRouteButton.leftAnchor.constraint(equalTo: (self.route?.MapView.leftAnchor)!, constant: 10).isActive = true
                    
                    self.homeController?.callButton.removeFromSuperview()
                    self.route?.MapView.addSubview((self.homeController?.callButton)!)
                    self.homeController?.callButton.bottomAnchor.constraint(equalTo: (self.route?.MapView.safeAreaLayoutGuide.bottomAnchor)!, constant: -80).isActive = true
                    self.homeController?.callButton.leftAnchor.constraint(equalTo: (self.route?.MapView.leftAnchor)!, constant: 10).isActive = true
                    
                    self.homeController?.checkinButton.removeFromSuperview()
                    self.route?.MapView.addSubview((self.homeController?.checkinButton)!)
                    self.homeController?.checkinButton.bottomAnchor.constraint(equalTo: (self.route?.MapView.safeAreaLayoutGuide.bottomAnchor)!, constant: -160).isActive = true
                    self.homeController?.checkinButton.leftAnchor.constraint(equalTo: (self.route?.MapView.leftAnchor)!, constant: 10).isActive = true
                    
                }
                
            }, completion: { _ in
                window.addSubview(self.mapCoverView)
                self.mapCoverView.topAnchor.constraint(equalTo: self.mapView.centerYAnchor, constant: 0).isActive = true
                self.mapCoverView.bottomAnchor.constraint(equalTo: self.routeHeaderView.topAnchor, constant: -5).isActive = true
                self.mapCoverView.leftAnchor.constraint(equalTo: self.mapView.leftAnchor, constant: 0).isActive = true
                self.mapCoverView.rightAnchor.constraint(equalTo: self.mapView.rightAnchor, constant: 0).isActive = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleDismissRouteDetails))
                self.mapCoverView.addGestureRecognizer(tap)
                let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.handleDismissRouteDetails))
                swipeDown.direction = .down
                self.mapCoverView.addGestureRecognizer(swipeDown)
                self.routeDetailCollectionView.addGestureRecognizer(swipeDown)
            })
            
        }
    }
    
    @objc func startRoute(){
        print("beginning route")
        route?.OnRoute = true
        Variables.onRoute = true
        Variables.currentRoute = route
        self.route?.MapView.clear()
        self.route?.showRouteMarkers(onRoute: true, currentLocation: self.homeController?.locationManager.location?.coordinate)
        homeController?.timeButton.setTitle(route?.timeToNextStop, for: .normal)
        print(route?.timeToNextStop)
        dismissRouteDetails()
        homeController?.removeSearchBar()
        homeController?.routeDetailsView = self.routeDetailCollectionView
        homeController?.currentRoute = route
        homeController?.currentStop = route?.RouteOrder![1]
        homeController?.addOnRouteButtons(route: self.route!)
        routeFromLocation()
    }
    
    func routeFromLocation() {
        //draws from path from locaiton to current stop
        let startLoc = (homeController?.locationManager.location)!
        let endLoc2D = route?.CurrentStop?.Location
        let endLoc = CLLocation(latitude: (endLoc2D?.latitude)!, longitude: (endLoc2D?.longitude)!)
        route?.drawPath(route: route!, startLocation: startLoc, endLocation: endLoc, mapView: (route?.MapView)!, onRoute: true, current:true)
        //let path = homeController?.combineRoutesForFrame(Routes: [self.route!], onRoute: true)
        //path?.add(startLoc.coordinate)
        
        self.mapView.camera = GMSCameraPosition.camera(withTarget: startLoc.coordinate, zoom: 17)
        self.mapView.animate(toViewingAngle: 45)
        self.mapView.animate(toBearing: self.getBearingBetweenTwoPoints(point1: startLoc, point2: endLoc))
    }
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoPoints(point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
    
    
    @objc func saveRoute(){
        let alert = UIAlertController(title: "Name your route", message: "", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = "My Favorite Route"
        }
        
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            self.routeName = textField!.text
            print("save route: " + self.routeName)
            
            guard let UserID = Auth.auth().currentUser?.uid else { print("error"); return }
            
            
            let ref: DatabaseReference = Database.database().reference()
            let key = self.routeName + "&id:" + ref.child("saved routes").childByAutoId().key
            let post = self.route?.getRouteForDateBase()
            let childUpdates = ["/saved routes/\(key)": post,
                                "/users/\(UserID)/favorites/\(key)/": post]
            print(post)
            ref.updateChildValues(childUpdates)
            
            DispatchQueue.main.async {
                let savedLabel = UILabel()
                savedLabel.text = "Route Saved!"
                savedLabel.font = UIFont(name: "Avenir-Heavy", size: 20)
                savedLabel.textColor = .white
                savedLabel.numberOfLines = 0
                savedLabel.backgroundColor = UIColor(white: 0, alpha: 0.3)
                savedLabel.textAlignment = .center
                
                savedLabel.frame = CGRect(x: 0, y: 0, width: 150, height: 80)
                savedLabel.center = (self.route?.MapView.center)!
                
                self.route?.MapView.addSubview(savedLabel)
                
                savedLabel.layer.transform = CATransform3DMakeScale(0, 0, 0)
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    
                    savedLabel.layer.transform = CATransform3DMakeScale(1, 1, 1)
                    
                }, completion: { (completed) in
                    //completed
                    
                    UIView.animate(withDuration: 0.5, delay: 0.75, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                        
                        savedLabel.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.1)
                        savedLabel.alpha = 0
                        
                    }, completion: { (_) in
                        
                        savedLabel.removeFromSuperview()
                        
                    })

                })
                
            }

            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        homeController?.present(alert, animated: true, completion: nil)
    }
    
    @objc func shareRoute(){
        print("share route")
        var stopString = ""
        for stop in (route?.Route)! {
            stopString = stopString + "- " + stop.Name! + "\r"
        }
        let rootViewController = topMostController()
        
        let activityViewController = UIActivityViewController(activityItems: ["I just created an awesome date with dateroute!\r\r" + stopString + "\rLearn more at www.dateroute.io"], applicationActivities: nil)
        rootViewController.present(activityViewController, animated:true, completion:nil)
    }
    
    func topMostController() -> UIViewController {
        var rootViewController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (rootViewController.presentedViewController != nil) {
            rootViewController = rootViewController.presentedViewController!
        }
        return rootViewController
    }
    
    // Dismiss Route details on swipe down motion
    @objc func handleDismissRouteDetails(gesture: UIGestureRecognizer) {
        
        if let tapGesture = gesture as? UITapGestureRecognizer {
            dismissRouteDetails()
            // Remove info window from previous marker
            homeController?.infoWindow.removeFromSuperview()
            if homeController?.haveTappedMarker == true {
                homeController?.lastTappedMarker.iconView = nil
            }
        }
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            if swipeGesture.direction == UISwipeGestureRecognizer.Direction.down {
                dismissRouteDetails()
            }
        }
        
    }
    
    @objc func back(sender: UIBarButtonItem) {
        
        // Perform your custom actions}
        _ = homeController?.navigationController?.popViewController(animated: true)
        self.dismissRouteDetails()
    }
    
    func customBackButtonBehavior(){
        if (homeController?.routeDetailsVisible)! {
            let imageView = UIImageView()
            imageView.backgroundColor = UIColor.clear
            imageView.frame = CGRect(x:0,y:0,width:2*(homeController?.navigationController?.navigationBar.bounds.height)!,height:(homeController?.navigationController?.navigationBar.bounds.height)!)
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(back))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
            imageView.tag = 1
            homeController?.navigationController?.navigationBar.addSubview(imageView)
        }
    }
    
    func dismissRouteDetails() {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.routeDetailCollectionView.frame = CGRect(x: 0, y: self.screenHeight!, width: self.routeDetailCollectionView.frame.width, height: self.routeDetailCollectionView.frame.height)
            
            self.mapView.frame = CGRect(x: 0, y: 0, width: self.screenWidth!, height: self.mapHeight)
            self.mapCoverView.removeFromSuperview()
            self.routeHeaderView.removeFromSuperview()
            if (self.route?.OnRoute)! {
                self.homeController?.endRouteButton.removeFromSuperview()
                self.route?.MapView.addSubview((self.homeController?.endRouteButton)!)
                self.homeController?.endRouteButton.bottomAnchor.constraint(equalTo: (self.route?.MapView.safeAreaLayoutGuide.bottomAnchor)!, constant: -10).isActive = true
                self.homeController?.endRouteButton.leftAnchor.constraint(equalTo: (self.route?.MapView.leftAnchor)!, constant: 10).isActive = true
                
                self.homeController?.callButton.removeFromSuperview()
                self.route?.MapView.addSubview((self.homeController?.callButton)!)
                self.homeController?.callButton.bottomAnchor.constraint(equalTo: (self.route?.MapView.safeAreaLayoutGuide.bottomAnchor)!, constant: -80).isActive = true
                self.homeController?.callButton.leftAnchor.constraint(equalTo: (self.route?.MapView.leftAnchor)!, constant: 10).isActive = true
                
                self.homeController?.checkinButton.removeFromSuperview()
                self.route?.MapView.addSubview((self.homeController?.checkinButton)!)
                self.homeController?.checkinButton.bottomAnchor.constraint(equalTo: (self.route?.MapView.safeAreaLayoutGuide.bottomAnchor)!, constant: -160).isActive = true
                self.homeController?.checkinButton.leftAnchor.constraint(equalTo: (self.route?.MapView.leftAnchor)!, constant: 10).isActive = true
            }
            
        }, completion: nil)
        self.mapView.selectedMarker = nil
        homeController?.infoWindow.removeFromSuperview()
        homeController?.routeDetailsVisible = false
        if (route?.OnRoute)! {
            let bounds = GMSCoordinateBounds(path:(self.route?.Path)!)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
        } else {
            let allRoutePath = homeController?.combineRoutesForFrame(Routes: (homeController?.ROUTES)!, onRoute: false)
            let bounds = GMSCoordinateBounds(path:allRoutePath!)
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 75.0))
        }
    }
    
    
    //MARK - Collection View Data Source / Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func scrollViewDidEndDecelerating(scrollView: UICollectionView) {
        for cell in routeDetailCollectionView.visibleCells  as [UICollectionViewCell]    {
            let indexPath = routeDetailCollectionView.indexPath(for: cell as UICollectionViewCell)
            print(indexPath)
            //NSLog("%@", indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (route?.RouteOrder?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! routeDetailsCollectionViewCell
        //let colors = [UIColor.red, UIColor.green, UIColor.blue]
        cell.backgroundColor = self.Color?.0
        cell.stopNameLabel.text = route?.RouteOrder![indexPath.item + 1]?.Name
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap(_:))))
        // TODO: this is where we set the tap recognizer, now let's add a drag recognizer to show the pop info windows
        addCellConstraints(indexPath: indexPath, cell: cell)
        shareButton.addTarget(self, action: #selector(shareRoute), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(saveRoute), for: .touchUpInside)
        startRouteButton.addTarget(self, action: #selector(startRoute), for: .touchUpInside)
//        confirmSaveButton.addTarget(self, action: #selector(saveRoute), for: .touchUpInside)
        return cell
    }
    
    
    @objc func tap(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: routeDetailCollectionView)
        let indexPath = routeDetailCollectionView.indexPathForItem(at: location)
        
        if let index = indexPath {
            print("Tapped location on details window")
            let selectedStop = route?.RouteOrder![index.item + 1]
            let selectedMarker = selectedStop?.Marker
            mapView.animate(with: GMSCameraUpdate.setTarget((selectedMarker?.position)!))
            mapView.selectedMarker = selectedMarker
            
            // Remove info window from previous marker
            homeController?.infoWindow.removeFromSuperview()
            if homeController?.haveTappedMarker == true {
                homeController?.lastTappedMarker.iconView = nil
            }

            homeController?.infoWindow = markerWindow(frame: CGRect(x: 0, y: 0, width: mapView.frame.width - 150, height: mapView.safeAreaLayoutGuide.layoutFrame.height/3))
            let location = selectedMarker?.position
            
            homeController?.infoWindow.center = mapView.projection.point(for: location!)
            homeController?.infoWindow.center.y = (homeController?.infoWindow.center.y)! - (homeController?.infoWindowOffset)!
            selectedMarker!.tracksViewChanges = false
            homeController?.newInfoWindow(marker: selectedMarker!)
            
            let dragOffset = cellWidth // - spaceFromEdge
            let offset = dragOffset * CGFloat(index.item)
            if routeDetailCollectionView.contentOffset.x != offset {
                routeDetailCollectionView.setContentOffset(CGPoint(x: offset, y: 0), animated: true)
            }

            homeController?.haveTappedMarker = true
            homeController?.lastTappedMarker = selectedMarker!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        let selectedStop = route?.RouteOrder![indexPath.item + 1]
        //        let selectedMarker = selectedStop?.Marker
        //        mapView.animate(with: GMSCameraUpdate.setTarget((selectedMarker?.position)!))
        //        mapView.selectedMarker = selectedMarker
        //        SelectedMarker = selectedMarker
        //        let dragOffset = cellWidth // - spaceFromEdge
        //        let offset = dragOffset * CGFloat(indexPath.item)
        //        if collectionView.contentOffset.x != offset {
        //            collectionView.setContentOffset(CGPoint(x: offset, y: 0), animated: true)
        //        }
        
    }
    
    func addCellConstraints(indexPath: IndexPath, cell: routeDetailsCollectionViewCell){
        cell.stopImage.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: -10).isActive = true
        cell.stopImage.topAnchor.constraint(equalTo: cell.topAnchor, constant: 55.0).isActive = true
        
        cell.stopNameLabel.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: -40.0).isActive = true
        cell.stopNameLabel.topAnchor.constraint(equalTo: cell.topAnchor, constant: 5.0).isActive = true
        
        let color = self.Color?.1
        let labelWidth = 3*cell.frame.width/4
        
        if indexPath.item == 0 {
            let imageString = color! + "_stopBegin"
            cell.stopImage.image = UIImage(imageLiteralResourceName: imageString)
            cell.stopImage.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: spaceFromEdge).isActive = true
            cell.stopImage.rightAnchor.constraint(equalTo: cell.rightAnchor, constant: spaceFromEdge)
            
            cell.stopNameLabel.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 10).isActive = true
            cell.stopNameLabel.rightAnchor.constraint(equalTo: cell.leftAnchor, constant: 10+labelWidth).isActive = true
        }else if indexPath.item == (route?.RouteOrder!.count)!-1{
            let imageString = color! + "_stopEnd"
            cell.stopImage.image = UIImage(imageLiteralResourceName: imageString)
            cell.stopImage.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: -1*spaceFromEdge).isActive = true
            cell.stopImage.rightAnchor.constraint(equalTo: cell.leftAnchor, constant: 100).isActive = true
            
            cell.stopNameLabel.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 20.0).isActive = true
            cell.stopNameLabel.rightAnchor.constraint(equalTo: cell.leftAnchor, constant: 20+labelWidth).isActive = true
        }else {
            let imageString = color! + "_stopMiddle"
            cell.stopImage.image = UIImage(imageLiteralResourceName: imageString)
            cell.stopImage.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 0.0).isActive = true
            cell.stopImage.rightAnchor.constraint(equalTo: cell.rightAnchor, constant: 0.0).isActive = true
            
            cell.stopNameLabel.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 20).isActive = true
            cell.stopNameLabel.rightAnchor.constraint(equalTo: cell.leftAnchor, constant: 20+labelWidth).isActive = true
        }
    }
    
    
    override init() {
        super.init()
        routeDetailCollectionView.dataSource = self
        routeDetailCollectionView.delegate = self
        routeDetailCollectionView.allowsSelection = true
        routeDetailCollectionView.register(routeDetailsCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        
    }
}








