//
//  searchWindow.swift
//  DateRoute
//
//  Created by Cameron Korb on 4/21/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import TGPControls
import GoogleMaps

extension HomeViewController {

    func displayActivityWindow() {
        currentSearchPage = SearchPage.Activity
        activityWindow.frame = searchFrame
        self.view.insertSubview(activityWindow, aboveSubview: searchBar)
        searchBar.isHidden = true
        searchImage.isHidden = true
        
        
        addActivityButtons()
        
        if selectedActivities.count > 0{
            nextButton.setImage(#imageLiteral(resourceName: "NextIcon"), for: .normal)
        }
    }
    
    
    func addActivityButtons() {
        
        activityWindow.addSubview(activityLabel)
        activityLabel.topAnchor.constraint(equalTo: exitButton.bottomAnchor, constant: 0).isActive = true
        activityLabel.leftAnchor.constraint(equalTo: activityWindow.leftAnchor, constant: 10).isActive = true
        activityLabel.rightAnchor.constraint(equalTo: activityWindow.rightAnchor, constant: -10).isActive = true
        
        activityWindow.addSubview(nextButton)
        nextButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        nextButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true

        
        // create "Drinks" button
        let drinksButton = UIButton(frame: CGRect(x:(screenWidth! - 85)/2 - 105, y:105, width:85, height:85))
        drinksButton.setTitle("Bar", for: .normal)
        drinksButton.setImage(#imageLiteral(resourceName: "drinksButton"), for: .normal)
        //drinksButton.setImage(#imageLiteral(resourceName: "drinksButtonPressed"), for: .selected)
        drinksButton.addTarget(self, action: #selector(activitiesButtonTapped), for: .touchUpInside)
        activityWindow.addSubview(drinksButton)
        if selectedActivities.contains("Bar") == true{
            drinksButton.isSelected = true
        }
        
        // create "Dinner" button
        let dinnerButton = UIButton(frame: CGRect(x:(screenWidth! - 85)/2, y:105, width:85, height:85))
        dinnerButton.setTitle("Restaurant", for: .normal)
        dinnerButton.setImage(#imageLiteral(resourceName: "dinnerButton"), for: .normal)
        //dinnerButton.setImage(#imageLiteral(resourceName: "dinnerButtonPressed"), for: .selected)
        dinnerButton.addTarget(self, action: #selector(activitiesButtonTapped), for: .touchUpInside)
        activityWindow.addSubview(dinnerButton)
        if selectedActivities.contains("Restaurant") == true{
            dinnerButton.isSelected = true
        }
        
        // create "Coffee" buttons
        let coffeeButton = UIButton(frame: CGRect(x:(screenWidth! - 85)/2 + 105, y:105, width:85, height:85))
        coffeeButton.setTitle("Coffee", for: .normal)
        coffeeButton.setImage(#imageLiteral(resourceName: "coffeeButton"), for: .normal)
        //coffeeButton.setImage(#imageLiteral(resourceName: "coffeeButtonPressed"), for: .selected)
        coffeeButton.addTarget(self, action: #selector(activitiesButtonTapped), for: .touchUpInside)
        activityWindow.addSubview(coffeeButton)
        if selectedActivities.contains("Coffee") == true{
            coffeeButton.isSelected = true
        }
        
        let dessertButton = UIButton(frame: CGRect(x:(screenWidth! - 85)/2 - 105, y:200, width:85, height:85))
        dessertButton.setTitle("Dessert", for: .normal)
        dessertButton.setImage(#imageLiteral(resourceName: "dessertButton"), for: .normal)
        //dessertButton.setImage(#imageLiteral(resourceName: "dessertButtonPressed"), for: .selected)
        dessertButton.addTarget(self, action: #selector(activitiesButtonTapped), for: .touchUpInside)
        activityWindow.addSubview(dessertButton)
        if selectedActivities.contains("Dessert") == true{
            dessertButton.isSelected = true
        }
        
        let parkButton = UIButton(frame: CGRect(x:(screenWidth! - 85)/2, y:200, width:85, height:85))
        parkButton.setTitle("Park", for: .normal)
        parkButton.setImage(#imageLiteral(resourceName: "parkButton"), for: .normal)
        //parkButton.setImage(#imageLiteral(resourceName: "parkButtonPressed"), for: .selected)
        parkButton.addTarget(self, action: #selector(activitiesButtonTapped), for: .touchUpInside)
        activityWindow.addSubview(parkButton)
        if selectedActivities.contains("Park") == true{
            parkButton.isSelected = true
        }
        
        let nightlifeButton = UIButton(frame: CGRect(x:(screenWidth! - 85)/2 + 105, y:200, width:85, height:85))
        nightlifeButton.setTitle("Nightlife", for: .normal)
        nightlifeButton.setImage(#imageLiteral(resourceName: "nightlifeButton"), for: .normal)
        //nightlifeButton.setImage(#imageLiteral(resourceName: "nightlifeButtonPressed"), for: .selected)
        nightlifeButton.addTarget(self, action: #selector(activitiesButtonTapped), for: .touchUpInside)
        activityWindow.addSubview(nightlifeButton)
        if selectedActivities.contains("Nightlife") == true{
            nightlifeButton.isSelected = true
        }
        

        
        activityButtons = [drinksButton, dinnerButton, coffeeButton, dessertButton, parkButton, nightlifeButton]
        
        // space used for building the current route
        activityWindow.addSubview(buildRouteScrollView)
    
        
        // add placeholders
        let getXSpacing = [(screenWidth! - 85)/2 - 105, (screenWidth! - 85)/2, (screenWidth! - 85)/2 + 105, (screenWidth! - 85)/2 + 210, (screenWidth! - 85)/2 + 315]
        for (i,X) in getXSpacing.enumerated() {
            let placeholder = UIImageView(frame: CGRect(x:X, y:0, width:85, height:85))
            placeholder.image = UIImage(imageLiteralResourceName: "placeholder")
            buildRouteScrollView.addSubview(placeholder)
            
            let label = UILabel(frame: CGRect(x:X+25, y:0, width:85, height:85))
//            let label = UILabel()
            label.text = "Stop " + String(i+1)
            label.font = UIFont(name: "Avenir", size: 12)
            label.textColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0)
            buildRouteScrollView.addSubview(label)
//            label.centerYAnchor.constraint(equalTo: placeholder.centerYAnchor, constant: 0).isActive = true
//            label.centerXAnchor.constraint(equalTo: placeholder.centerXAnchor, constant: 0).isActive = true
        }
        // refresh images and selectedActivities array
        selectedActivities.removeAll()
        if activityImages.count > 0 {
            for i in 0...activityImages.count-1 {
                selectedActivities.append(activityImages[i].title(for: .normal)!)
                activityImages[i].frame = CGRect(x:getXSpacing[i], y:0, width:90, height:85)
                buildRouteScrollView.bringSubviewToFront(activityImages[i])
            }
        }
    }
    
    
    
    // manage selection on activities buttons
    @objc func activitiesButtonTapped(sender: UIButton){
        let name = sender.title(for: .normal)
        let getXSpacing = [(screenWidth! - 85)/2 - 105, (screenWidth! - 85)/2, (screenWidth! - 85)/2 + 105, (screenWidth! - 85)/2 + 210, (screenWidth! - 85)/2 + 315]
        let resourceName = name! + "Icon"
        if selectedActivities.count < 5 {
            
            // scroll to right if needed
            if selectedActivities.count > 1 && selectedActivities.count<4 {
                let goto = 105*(selectedActivities.count-1)
                buildRouteScrollView.setContentOffset(CGPoint(x: goto, y: 0), animated: true)
            }
            if selectedActivities.count <= 1 {
                buildRouteScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            }
            
            selectedActivities.append(name!)
            let activityAdded = UIButton(frame: CGRect(x:getXSpacing[selectedActivities.count-1], y:0, width:90, height:85))
            activityAdded.setTitle(name, for: .normal)
            activityAdded.setImage(#imageLiteral(resourceName: resourceName), for: .normal)
            activityAdded.addTarget(self, action: #selector(deleteActivity), for: .touchUpInside)
            buildRouteScrollView.addSubview(activityAdded)
            activityImages.append(activityAdded)
            
        }
        
        
        if (selectedActivities.count) > 0 {
            nextButton.setImage(#imageLiteral(resourceName: "NextIcon"), for: .normal)
        } else {
            nextButton.setImage(#imageLiteral(resourceName: "skipIcon"), for: .normal)
        }
    }
    
    // deleteActivity
    @objc func deleteActivity(sender: UIButton){
        // remove reference to UIButton
        sender.removeFromSuperview()
        activityImages = activityImages.filter {$0 != sender}
        
        // refresh image position and update selectedActivities array
        let getXSpacing = [(screenWidth! - 85)/2 - 105, (screenWidth! - 85)/2, (screenWidth! - 85)/2 + 105, (screenWidth! - 85)/2 + 210, (screenWidth! - 85)/2 + 315]
        selectedActivities.removeAll()
        if activityImages.count > 0 {
            for i in 0...activityImages.count-1 {
                selectedActivities.append(activityImages[i].title(for: .normal)!)
                activityImages[i].frame = CGRect(x:getXSpacing[i], y:0, width:90, height:85)
            }
        }
        // scroll to right if needed
        var goto = 0
        if selectedActivities.count > 2 {
            goto = 105*(selectedActivities.count-2)
        }
        buildRouteScrollView.setContentOffset(CGPoint(x: goto, y: 0), animated: true)
    }
    
    @objc func barButtonTapped(sender: UIButton){
        let name = sender.title(for: .normal)
        if sender.isSelected == true {
            sender.isSelected = false
            selectedBars.remove(at: selectedBars.firstIndex(of: name!)!)
            sender.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
            sender.layer.borderWidth = 0.5
            
        }else {
            sender.isSelected = true
            selectedBars.append(name!)
            sender.layer.borderColor = UIColor(red:114/255, green:161/255, blue:187/255, alpha:1.0).cgColor
            sender.layer.borderWidth = 4
        }
        if (selectedBars.count) > 0 {
            nextButton.setImage(#imageLiteral(resourceName: "NextIcon"), for: .normal)
        } else {
            nextButton.setImage(#imageLiteral(resourceName: "skipIcon"), for: .normal)
        }
    }
    
    // manage selection of restaurant buttons
    @objc func restaurantButtonTapped(sender: ovalButton){
        let name = sender.title(for: .normal)
        if sender.isSelected == true {
            sender.isSelected = false
            selectedRestaurants.remove(at: selectedRestaurants.firstIndex(of: name!)!)
            sender.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
            sender.layer.borderWidth = 0.5
        }else {
            sender.isSelected = true
            selectedRestaurants.append(name!)
            sender.layer.borderColor = UIColor(red:114/255, green:161/255, blue:187/255, alpha:1.0).cgColor
            sender.layer.borderWidth = 4
        }
        if (selectedRestaurants.count) > 0 {
            nextButton.setImage(#imageLiteral(resourceName: "NextIcon"), for: .normal)
        } else {
            nextButton.setImage(#imageLiteral(resourceName: "skipIcon"), for: .normal)
        }
    }
    

    // decides what the next screen should be and move to it
    @objc func nextScreen() {
        if currentSearchPage == SearchPage.Activity {
            if selectedActivities.contains("Bar") && currentSearchPage != SearchPage.Bars{
                self.view.insertSubview(barsWindow, aboveSubview: activityWindow)
                slideToNextView(current: activityWindow, next: barsWindow, searchPage: SearchPage.Bars, forward: true)
                barsWindow.isHidden = false
                activityLabel.removeFromSuperview()
            } else if selectedActivities.contains("Restaurant") {
                self.view.insertSubview(restaurantsWindow, aboveSubview: activityWindow)
                slideToNextView(current: activityWindow, next: restaurantsWindow, searchPage: SearchPage.Restaurants, forward: true)
            } else {
                self.view.insertSubview(searchWindow, aboveSubview: activityWindow)
                slideToNextView(current: activityWindow, next: searchWindow, searchPage: SearchPage.Search, forward: true)
            }
        }else if currentSearchPage == SearchPage.Bars {
            
            if selectedActivities.contains("Restaurant") {
                self.view.insertSubview(restaurantsWindow, aboveSubview: barsWindow)
                slideToNextView(current: barsWindow, next: restaurantsWindow, searchPage: SearchPage.Restaurants, forward: true)
                
            } else {
                self.view.insertSubview(searchWindow, aboveSubview: barsWindow)
                slideToNextView(current: barsWindow, next: searchWindow, searchPage: SearchPage.Search, forward: true)
            }
        } else if currentSearchPage == SearchPage.Restaurants {
            self.view.insertSubview(searchWindow, aboveSubview: restaurantsWindow)
            slideToNextView(current: restaurantsWindow, next: searchWindow, searchPage: SearchPage.Search, forward: true)
        }
    }
    
    // Move to previous screen
    @objc func prevScreen() {
        if currentSearchPage == SearchPage.Bars {
            self.view.insertSubview(activityWindow, aboveSubview: barsWindow)
            slideToNextView(current: barsWindow, next: activityWindow, searchPage: SearchPage.Activity, forward: false)
            
        } else if currentSearchPage == SearchPage.Restaurants {
            if selectedActivities.contains("Bar") {
                self.view.insertSubview(barsWindow, aboveSubview: restaurantsWindow)
                slideToNextView(current: restaurantsWindow, next: barsWindow, searchPage: SearchPage.Bars, forward: false)
            } else {
                self.view.insertSubview(activityWindow, aboveSubview: restaurantsWindow)
                slideToNextView(current: restaurantsWindow, next: activityWindow, searchPage: SearchPage.Activity, forward: false)
            }
        }
        else if currentSearchPage == SearchPage.Search {
            if selectedActivities.contains("Restaurant") {
                self.view.insertSubview(restaurantsWindow, aboveSubview: searchWindow)
                slideToNextView(current: searchWindow, next: restaurantsWindow, searchPage: SearchPage.Restaurants, forward: false)
            } else if selectedActivities.contains("Bar") {
                self.view.insertSubview(barsWindow, aboveSubview: searchWindow)
                slideToNextView(current: searchWindow, next: barsWindow, searchPage: SearchPage.Bars, forward: false)
            } else {
                self.view.insertSubview(activityWindow, aboveSubview: searchWindow)
                slideToNextView(current: searchWindow, next: activityWindow, searchPage: SearchPage.Activity, forward: false)
            }
        }
    }
    
    // Remove current elements and go to next screen - this can probably be cleaned up a lot
    func slideToNextView(current:UIView, next:UIView, searchPage:SearchPage, forward:Bool) {
        nextButton.setImage(#imageLiteral(resourceName: "skipIcon"), for: .normal)
        if next.isHidden {
            next.isHidden = false
        }
        
        if forward {
            next.frame = CGRect(x: searchFrame.maxX, y: searchFrame.minX, width: searchFrame.width, height: searchFrame.height)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                current.frame = CGRect(x: self.searchFrame.minX - self.screenWidth!, y: self.searchFrame.minY, width: self.searchFrame.width, height: self.searchFrame.height)
                if searchPage == SearchPage.Search {
                    next.frame = self.searchLocationFrame
                } else {
                    next.frame = self.searchFrame
                }
                
                
            }, completion: { (done) in
                if done {
                    self.currentSearchPage = searchPage
                    if searchPage == SearchPage.Bars {
                        self.addBarsOptions()
                        self.removeButtons(buttons: self.activityButtons)
                        self.buildRouteScrollView.removeFromSuperview()
                        self.restaurantsLabel.removeFromSuperview()
                        self.activityLabel.removeFromSuperview()
                    }else if searchPage == SearchPage.Restaurants {
                        self.addRestaurantOptions()
                        self.removeButtons(buttons: self.activityButtons)
                        self.removeButtons(buttons: self.barsButtons)
                        self.buildRouteScrollView.removeFromSuperview()
                        self.barsTextField.removeFromSuperview()
                        self.barsIcon.removeFromSuperview()
                        self.barsLabel.removeFromSuperview()
                        self.activityLabel.removeFromSuperview()
                    } else if searchPage == SearchPage.Search {
                        self.addSearchOptions()
                        self.removeButtons(buttons: self.activityButtons)
                        self.removeButtons(buttons: self.barsButtons)
                        self.removeButtons(buttons: self.restaurantsButtons)
                        self.buildRouteScrollView.removeFromSuperview()
                        self.restaurantsTextField.removeFromSuperview()
                        self.barsTextField.removeFromSuperview()
                        self.barsIcon.removeFromSuperview()
                        self.barsLabel.removeFromSuperview()
                        self.restaurantsLabel.removeFromSuperview()
                        self.activityLabel.removeFromSuperview()
                    }
                    current.removeFromSuperview()
                }
            })
        } else {
            next.frame = CGRect(x: searchFrame.minX - searchFrame.width, y: searchFrame.minX, width: searchFrame.width, height: searchFrame.height)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                current.frame = CGRect(x: self.searchFrame.minX + self.screenWidth!, y: self.searchFrame.minY, width: self.searchFrame.width, height: self.searchFrame.height)
                if searchPage == SearchPage.Search {
                    next.frame = self.searchLocationFrame
                } else {
                    next.frame = self.searchFrame
                }
                
            }, completion: { (done) in
                if done {
                    self.currentSearchPage = searchPage
                    if searchPage == SearchPage.Activity {
                        self.addActivityButtons()
                        self.removeButtons(buttons: self.barsButtons)
                        self.removeButtons(buttons: self.restaurantsButtons)
                        self.beginSearchLabel.removeFromSuperview()
                        self.restaurantsTextField.removeFromSuperview()
                        self.barsTextField.removeFromSuperview()
                        self.barsIcon.removeFromSuperview()
                        self.barsLabel.removeFromSuperview()
                        self.restaurantsLabel.removeFromSuperview()
                    }else if searchPage == SearchPage.Bars {
                        self.addBarsOptions()
                        self.removeButtons(buttons: self.restaurantsButtons)
                        self.beginSearchLabel.removeFromSuperview()
                        self.restaurantsTextField.removeFromSuperview()
                        self.restaurantsLabel.removeFromSuperview()
                        self.activityLabel.removeFromSuperview()
                    }else if searchPage == SearchPage.Restaurants {
                        self.addRestaurantOptions()
                        self.beginSearchLabel.removeFromSuperview()
                        self.barsLabel.removeFromSuperview()
                        self.activityLabel.removeFromSuperview()
                    }
                    current.removeFromSuperview()
                }
            })
            // TODO: add back numStopSlider
            // numStopSlider.value = 1
            // numStopLabels.value = UInt(numStopSlider.value)
        }
        
    }
    
    
    // -------------------------------- Define bar buttons --------------------------------------
    
    func addBarsOptions() {
        selectedBars = []
        barsWindow.addSubview(nextButton)
        nextButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        nextButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true

//        barsWindow.addSubview(prevButton)
//        prevButton.bottomAnchor.constraint(equalTo: barsWindow.bottomAnchor, constant: -10).isActive = true
//        prevButton.leftAnchor.constraint(equalTo: barsWindow.leftAnchor, constant: 5).isActive = true
        
        barsWindow.addSubview(barsIcon)
        barsIcon.topAnchor.constraint(equalTo: exitButton.bottomAnchor, constant: 15).isActive = true
        barsIcon.centerXAnchor.constraint(equalTo: barsWindow.centerXAnchor, constant: 0).isActive = true
        
        barsWindow.addSubview(barsLabel)
        barsLabel.topAnchor.constraint(equalTo: barsIcon.bottomAnchor, constant: 5).isActive = true
        barsLabel.leftAnchor.constraint(equalTo: barsWindow.leftAnchor, constant: 10).isActive = true
        barsLabel.rightAnchor.constraint(equalTo: barsWindow.rightAnchor, constant: -10).isActive = true
        
        //Maybe add the scroll stuff here:
        let scrollView: UIScrollView = {
            let view = UIScrollView(frame: CGRect(x: 0, y: 180, width: (UIApplication.shared.keyWindow?.frame.width)!, height: 410-180))
            //window.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
            view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
            view.isScrollEnabled = true
            //window.alwaysBounceHorizontal = true
            view.contentSize = CGSize(width: screenWidth!, height: 400)
            return view
        }()
        barsWindow.addSubview(scrollView)
        
        let beerButton = UIButton(frame: CGRect(x:30, y:0, width:screenWidth!-60, height:40))
        beerButton.setTitle("Beer", for: .normal)
        beerButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        beerButton.layer.cornerRadius = 19
        beerButton.layer.borderWidth = 0.5
        beerButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        beerButton.setTitleColor(UIColor.black, for: .normal)
        beerButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(beerButton)
        if selectedActivities.contains("Beer") == true{
            beerButton.isSelected = true
        }
        
        let wineButton = UIButton(frame: CGRect(x:30, y:50, width:screenWidth!-60, height:40))
        wineButton.setTitle("Wine", for: .normal)
        wineButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        wineButton.layer.cornerRadius = 19
        wineButton.layer.borderWidth = 0.5
        wineButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        wineButton.setTitleColor(UIColor.black, for: .normal)
        wineButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(wineButton)
        if selectedActivities.contains("Wine") == true{
            wineButton.isSelected = true
        }
        
        let cocktailButton = UIButton(frame: CGRect(x:30, y:100, width:screenWidth!-60, height:40))
        cocktailButton.setTitle("Cocktails", for: .normal)
        cocktailButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        cocktailButton.layer.cornerRadius = 19
        cocktailButton.layer.borderWidth = 0.5
        cocktailButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        cocktailButton.setTitleColor(UIColor.black, for: .normal)
        cocktailButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(cocktailButton)
        if selectedActivities.contains("Cocktails") == true{
            cocktailButton.isSelected = true
        }
        
        let diveBarButton = UIButton(frame: CGRect(x:30, y:150, width:screenWidth!-60, height:40))
        diveBarButton.setTitle("Dive Bar", for: .normal)
        diveBarButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        diveBarButton.layer.cornerRadius = 19
        diveBarButton.layer.borderWidth = 0.5
        diveBarButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        diveBarButton.setTitleColor(UIColor.black, for: .normal)
        diveBarButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(diveBarButton)
        if selectedActivities.contains("Dive Bar") == true{
            diveBarButton.isSelected = true
        }
        
        let liveMusicButton = UIButton(frame: CGRect(x:30, y:200, width:screenWidth!-60, height:40))
        liveMusicButton.setTitle("Live Music", for: .normal)
        liveMusicButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        liveMusicButton.layer.cornerRadius = 19
        liveMusicButton.layer.borderWidth = 0.5
        liveMusicButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        liveMusicButton.setTitleColor(UIColor.black, for: .normal)
        liveMusicButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(liveMusicButton)
        if selectedActivities.contains("Live Music") == true{
            liveMusicButton.isSelected = true
        }
        
        let gayButton = UIButton(frame: CGRect(x:30, y:250, width:screenWidth!-60, height:40))
        gayButton.setTitle("LGBT", for: .normal)
        gayButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        gayButton.layer.cornerRadius = 19
        gayButton.layer.borderWidth = 0.5
        gayButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        gayButton.setTitleColor(UIColor.black, for: .normal)
        gayButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(gayButton)
        if selectedActivities.contains("LGBT") == true{
            gayButton.isSelected = true
        }
        
        let sportsButton = UIButton(frame: CGRect(x:30, y:300, width:screenWidth!-60, height:40))
        sportsButton.setTitle("Sports Bar", for: .normal)
        sportsButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        sportsButton.layer.cornerRadius = 19
        sportsButton.layer.borderWidth = 0.5
        sportsButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        sportsButton.setTitleColor(UIColor.black, for: .normal)
        sportsButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(sportsButton)
        if selectedActivities.contains("Sports Bar") == true{
            sportsButton.isSelected = true
        }
        
        let speakeasyButton = UIButton(frame: CGRect(x:30, y:350, width:screenWidth!-60, height:40))
        speakeasyButton.setTitle("Speakeasy", for: .normal)
        speakeasyButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        speakeasyButton.layer.cornerRadius = 19
        speakeasyButton.layer.borderWidth = 0.5
        speakeasyButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        speakeasyButton.setTitleColor(UIColor.black, for: .normal)
        speakeasyButton.addTarget(self, action: #selector(barButtonTapped), for: .touchUpInside)
        scrollView.addSubview(speakeasyButton)
        if selectedActivities.contains("Speakeasy") == true{
            speakeasyButton.isSelected = true
        }
        
        
        barsButtons = [beerButton, wineButton, cocktailButton, diveBarButton, liveMusicButton, gayButton, sportsButton, speakeasyButton]

    }
    
    // -------------------------------- Define restuarant buttons --------------------------------------
    
    func addRestaurantOptions() {
        selectedRestaurants = []
        restaurantsWindow.addSubview(nextButton)
        nextButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        nextButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true
        
//        restaurantsWindow.addSubview(prevButton)
//        prevButton.bottomAnchor.constraint(equalTo: restaurantsWindow.bottomAnchor, constant: -10).isActive = true
//        prevButton.leftAnchor.constraint(equalTo: restaurantsWindow.leftAnchor, constant: 5).isActive = true
        
        restaurantsWindow.addSubview(restaurantsIcon)
        restaurantsIcon.topAnchor.constraint(equalTo: exitButton.bottomAnchor, constant: 15).isActive = true
        restaurantsIcon.centerXAnchor.constraint(equalTo: restaurantsWindow.centerXAnchor, constant: 0).isActive = true
        
        restaurantsWindow.addSubview(restaurantsLabel)
        restaurantsLabel.topAnchor.constraint(equalTo: restaurantsIcon.bottomAnchor, constant: 5).isActive = true
        restaurantsLabel.leftAnchor.constraint(equalTo: restaurantsWindow.leftAnchor, constant: 10).isActive = true
        restaurantsLabel.rightAnchor.constraint(equalTo: restaurantsWindow.rightAnchor, constant: -10).isActive = true
        
        let scrollView: UIScrollView = {
            let view = UIScrollView(frame: CGRect(x: 0, y: 180, width: (UIApplication.shared.keyWindow?.frame.width)!, height: 410-180))
            //window.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
            view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
            view.isScrollEnabled = true
            //window.alwaysBounceHorizontal = true
            view.contentSize = CGSize(width: screenWidth!, height: 550)
            //view.contentOffset = CGPoint(x: 0,y: 180)
            return view
        }()
        restaurantsWindow.addSubview(scrollView)
        
        let americanButton = UIButton(frame: CGRect(x:30, y:0, width:screenWidth!-60, height:40))
        americanButton.setTitle("American", for: .normal)
        americanButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        americanButton.layer.cornerRadius = 19
        americanButton.layer.borderWidth = 0.5
        americanButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        americanButton.setTitleColor(UIColor.black, for: .normal)
        americanButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(americanButton)
        if selectedActivities.contains("American") == true{
            americanButton.isSelected = true
        }
        
        let chineseButton = UIButton(frame: CGRect(x:30, y:50, width:screenWidth!-60, height:40))
        chineseButton.setTitle("Chinese", for: .normal)
        chineseButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        chineseButton.layer.cornerRadius = 19
        chineseButton.layer.borderWidth = 0.5
        chineseButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        chineseButton.setTitleColor(UIColor.black, for: .normal)
        chineseButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(chineseButton)
        if selectedActivities.contains("Chinese") == true{
            chineseButton.isSelected = true
        }
        
        let casualButton = UIButton(frame: CGRect(x:30, y:100, width:screenWidth!-60, height:40))
        casualButton.setTitle("Fast Casual", for: .normal)
        casualButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        casualButton.layer.cornerRadius = 19
        casualButton.layer.borderWidth = 0.5
        casualButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        casualButton.setTitleColor(UIColor.black, for: .normal)
        casualButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(casualButton)
        if selectedActivities.contains("Fast Casual") == true{
            casualButton.isSelected = true
        }
        
        let fineButton = UIButton(frame: CGRect(x:30, y:150, width:screenWidth!-60, height:40))
        fineButton.setTitle("Fine Dining", for: .normal)
        fineButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        fineButton.layer.cornerRadius = 19
        fineButton.layer.borderWidth = 0.5
        fineButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        fineButton.setTitleColor(UIColor.black, for: .normal)
        fineButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(fineButton)
        if selectedActivities.contains("Fine Dining") == true{
            fineButton.isSelected = true
        }
        
        let indianButton = UIButton(frame: CGRect(x:30, y:200, width:screenWidth!-60, height:40))
        indianButton.setTitle("Indian", for: .normal)
        indianButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        indianButton.layer.cornerRadius = 19
        indianButton.layer.borderWidth = 0.5
        indianButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        indianButton.setTitleColor(UIColor.black, for: .normal)
        indianButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(indianButton)
        if selectedActivities.contains("Indian") == true{
            indianButton.isSelected = true
        }
        
        let italianButton = UIButton(frame: CGRect(x:30, y:250, width:screenWidth!-60, height:40))
        italianButton.setTitle("Italian", for: .normal)
        italianButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        italianButton.layer.cornerRadius = 19
        italianButton.layer.borderWidth = 0.5
        italianButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        italianButton.setTitleColor(UIColor.black, for: .normal)
        italianButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(italianButton)
        if selectedActivities.contains("Italian") == true{
            italianButton.isSelected = true
        }
        
        let mexicanButton = UIButton(frame: CGRect(x:30, y:300, width:screenWidth!-60, height:40))
        mexicanButton.setTitle("Mexican", for: .normal)
        mexicanButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        mexicanButton.layer.cornerRadius = 19
        mexicanButton.layer.borderWidth = 0.5
        mexicanButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        mexicanButton.setTitleColor(UIColor.black, for: .normal)
        mexicanButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(mexicanButton)
        if selectedActivities.contains("Mexican") == true{
            mexicanButton.isSelected = true
        }
        
        let mediterraneanButton = UIButton(frame: CGRect(x:30, y:350, width:screenWidth!-60, height:40))
        mediterraneanButton.setTitle("Mediterranean", for: .normal)
        mediterraneanButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        mediterraneanButton.layer.cornerRadius = 19
        mediterraneanButton.layer.borderWidth = 0.5
        mediterraneanButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        mediterraneanButton.setTitleColor(UIColor.black, for: .normal)
        mediterraneanButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(mediterraneanButton)
        if selectedActivities.contains("Mediterranean") == true{
            mediterraneanButton.isSelected = true
        }

        let sushiButton = UIButton(frame: CGRect(x:30, y:400, width:screenWidth!-60, height:40))
        sushiButton.setTitle("Sushi", for: .normal)
        sushiButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        sushiButton.layer.cornerRadius = 19
        sushiButton.layer.borderWidth = 0.5
        sushiButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        sushiButton.setTitleColor(UIColor.black, for: .normal)
        sushiButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(sushiButton)
        if selectedActivities.contains("Sushi") == true{
            sushiButton.isSelected = true
        }
        
        let thaiButton = UIButton(frame: CGRect(x:30, y:450, width:screenWidth!-60, height:40))
        thaiButton.setTitle("Thai", for: .normal)
        thaiButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        thaiButton.layer.cornerRadius = 19
        thaiButton.layer.borderWidth = 0.5
        thaiButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        thaiButton.setTitleColor(UIColor.black, for: .normal)
        thaiButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(thaiButton)
        if selectedActivities.contains("Thai") == true{
            thaiButton.isSelected = true
        }
        
        let vegetarianButton = UIButton(frame: CGRect(x:30, y:500, width:screenWidth!-60, height:40))
        vegetarianButton.setTitle("Vegetarian", for: .normal)
        vegetarianButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 16)
        vegetarianButton.layer.cornerRadius = 19
        vegetarianButton.layer.borderWidth = 0.5
        vegetarianButton.layer.borderColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0).cgColor
        vegetarianButton.setTitleColor(UIColor.black, for: .normal)
        vegetarianButton.addTarget(self, action: #selector(restaurantButtonTapped), for: .touchUpInside)
        scrollView.addSubview(vegetarianButton)
        if selectedActivities.contains("Vegetarian") == true{
            vegetarianButton.isSelected = true
        }
        
        restaurantsButtons = [americanButton, chineseButton, casualButton, fineButton, indianButton, italianButton, mexicanButton, mediterraneanButton, sushiButton, thaiButton, vegetarianButton]
        
    }
    
    func addSearchOptions() {
//        searchWindow.addSubview(prevButton)
//        prevButton.bottomAnchor.constraint(equalTo: searchWindow.bottomAnchor, constant: -10).isActive = true
//        prevButton.leftAnchor.constraint(equalTo: searchWindow.leftAnchor, constant: 5).isActive = true
        
        searchWindow.addSubview(beginSearchLabel)
        beginSearchLabel.topAnchor.constraint(equalTo: exitButton.bottomAnchor, constant: 5).isActive = true
        beginSearchLabel.leftAnchor.constraint(equalTo: searchWindow.leftAnchor, constant: 10).isActive = true
        beginSearchLabel.rightAnchor.constraint(equalTo: searchWindow.rightAnchor, constant: -10).isActive = true
        beginSearchLabel.bottomAnchor.constraint(equalTo: searchWindow.topAnchor, constant: searchWindow.frame.height*2/3).isActive = true
        
        searchWindow.addSubview(searchButton)
        searchButton.topAnchor.constraint(equalTo: beginSearchLabel.bottomAnchor, constant: 5).isActive = true
        searchButton.centerXAnchor.constraint(equalTo: searchWindow.centerXAnchor, constant: 0).isActive = true
    }
    
    
    
    @objc func generateRoutes() {
        print("generating routes")
        searchWindow.addSubview(indicator)
        self.indicator.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
        self.indicator.center = view.center
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 5, y: 5)
        self.indicator.transform = transform
        self.indicator.startAnimating()
        let activities: [String] = selectedActivities
        var bar_categories: [String] = selectedBars
        if (barsTextField.text?.count)! > 0 {
            bar_categories.append(barsTextField.text!)
        }
        var restaurant_categories: [String] = selectedRestaurants
        if (restaurantsTextField.text?.count)! > 0 {
            restaurant_categories.append(restaurantsTextField.text!)
        }
        
        let searchCenter = self.mapView.camera.target
        let searchLatitude = searchCenter.latitude
        let searchLongitude = searchCenter.longitude
        let searchRadius = self.mapView.getRadius()
        let day = !dayNightSwitch.isOn
        
        let parameters: Parameters = ["Activities": activities,
                                      "Bar_categories": bar_categories,
                                      "Restaurant_categories": restaurant_categories,
                                      "Day_range": "5",
                                      "Search_radius": String(Int(searchRadius)),
                                      "Day": day,
                                      "Latitude": String(searchLatitude),
                                      "Longitude": String(searchLongitude),
                                      "Num_stops": String(numberStops) // Note: this is unused at the moment
        ]
        
        let key = "AIzaSyAvN5Jkm1Ui8XY2Y6fjrmX2GgdbW8n8uY8"
        
        let url = URL(string: "http://dateroute-fc903.appspot.com/dateRoute?key=\(key)")
        
        //        alamoFireManager.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        
        var searchErrorOccurred = false
        alamoFireManager.request(url!, method: .post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            // TODO: [MARTY] this is sloppy, but idgaf
            switch response.result {
            case .failure(let error):
                self.indicator.stopAnimating()
                self.present(self.searchErrorAlert, animated: true)
                return
            case .success(let value):
                break
            }
            var routes = [dateRoute]()
            let json = JSON(response.data!)
            for (route_num, route) in json {
                
                let route_name = "Custom Route: " + route_num
                var order = [Int:RouteStop]()
                let path = GMSMutablePath()
                let color = self.colors[Int(route_num)!+1]!
                
                if route[0].count != self.selectedActivities.count {
                    // If a route does not have the requested number of stops, that means something went wrong with the search
                    // and we should show the popup error
                    searchErrorOccurred = true
                }

                for (stop_num,stop) in route[0] {
                    let stop_name = stop["name"].stringValue
                    let id = stop["alias"].stringValue
                    let address = stop["location"]["display_address"].stringValue
                    let lat = stop["coordinates"]["latitude"].floatValue
                    let long = stop["coordinates"]["longitude"].floatValue
                    let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                    var categories = [String]()
                    for (_, category) in stop["categories"] {
                        let type = category["alias"].stringValue
                        categories.append(type)
                    }
                    let rating = stop["rating"].stringValue
                    let phone = stop["phone"].stringValue
                    let imageUrl = stop["image_url"].stringValue
                    let url = stop["url"].stringValue
                    let routeStop = RouteStop(Name: stop_name, Id: id, YelpInfo: stop, Address: address, location:location, Categories: categories, Rating: rating, Phone: phone, ImageURL: imageUrl, Url: url, image: nil, marker:nil)
                    order[Int(stop_num)!+1] = routeStop
                }
                self.mapView.clear()
                let customRoute = dateRoute(name: route_name, order: order, path: path, mapView: self.mapView, color: color, line: [GMSPolyline](), viewController:self)
                
                routes.append(customRoute)
                
            }
            self.indicator.stopAnimating()
            self.ROUTES = routes
            self.routeType = HomeViewController.routeTypes.Custom
            self.closeActivityWindow()
            self.setUpRoutes()
            
            if searchErrorOccurred {
                // Show an error message to the user if the length of stops returned is not what they asked for
                // But we still show the routes returned and prompt them to search again
                self.present(self.searchErrorAlert, animated: true)
            }
        }
        
        //Alternative way to go about post
        //                let jsonParameters = try? JSONSerialization.data(withJSONObject: parameters)
        //                var request = URLRequest(url: url!)
        //                request.httpMethod = "POST"
        //                request.httpBody = jsonParameters
        //        let task = URLSession.shared.dataTask(with: request) { data, response, error in
        //            guard let data = data, error == nil else {
        //                print(error?.localizedDescription ?? "No data")
        //                return
        //            }
        //            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
        //            if let responseJSON = responseJSON as? [String: Any] {
        //                print(responseJSON)
        //            }
        //        }
        //
        //        task.resume()
        
        
    }
    
    @objc func closeActivityWindow() {
        
        activityWindow.removeFromSuperview()
        barsWindow.removeFromSuperview()
        restaurantsWindow.removeFromSuperview()
        searchWindow.removeFromSuperview()
        searchButton.removeFromSuperview()
        searchBar.isHidden = false
        exitButton.isHidden = true
        searchTitle.isHidden = false
        searchImage.isHidden = false
        activityLabel.removeFromSuperview()
        barsLabel.removeFromSuperview()
        restaurantsLabel.removeFromSuperview()
        nextButton.removeFromSuperview()
        //prevButton.removeFromSuperview()
        beginSearchLabel.removeFromSuperview()
        restaurantsTextField.removeFromSuperview()
        barsTextField.removeFromSuperview()
        barsIcon.removeFromSuperview()
        buildRouteScrollView.removeFromSuperview()
        
        removeButtons(buttons: activityButtons)
        removeButtons(buttons: barsButtons)
        removeButtons(buttons: restaurantsButtons)
    }
    
    func removeButtons(buttons:[UIButton]?) {
        if buttons != nil {
            for button in buttons! {
                button.removeFromSuperview()
            }
        }
    }
    
    func addSearchBarConstraints() {
        //search bar stuff
        self.view.addSubview(searchBar)
        self.view.addSubview(searchTitle)
        self.view.addSubview(exitButton)
        self.view.addSubview(searchImage)
        
        searchBar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        searchBar.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10).isActive = true
        searchBar.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true
        searchBar.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 60).isActive = true
        
        exitButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        exitButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10).isActive = true
        
        searchImage.centerYAnchor.constraint(equalTo: searchBar.centerYAnchor, constant: 0).isActive = true
        searchImage.leftAnchor.constraint(equalTo: searchBar.leftAnchor, constant: 10).isActive = true
        
        searchTitle.centerYAnchor.constraint(equalTo: searchBar.centerYAnchor, constant: 0).isActive = true
        searchTitle.leftAnchor.constraint(equalTo: searchImage.rightAnchor, constant: 10).isActive = true
        
    }
    
    func removeSearchBar(){
        searchBar.removeFromSuperview()
        searchTitle.removeFromSuperview()
        exitButton.removeFromSuperview()
        searchImage.removeFromSuperview()
    }
}
















// MARK:- ---> UITextFieldDelegate

extension HomeViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        // return NO to disallow editing.
        //print("TextField should begin editing method called")
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // became first responder
        //print("TextField did begin editing method called")
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
        //print("TextField should snd editing method called")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
        //print("TextField did end editing method called")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        // if implemented, called in place of textFieldDidEndEditing:
        //print("TextField did end editing with reason method called")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // return NO to not change text
        //print("While entering the characters this method gets called")
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        // called when clear button pressed. return NO to ignore (no notifications)
        //print("TextField should clear method called")
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // called when 'return' key pressed. return NO to ignore.
        if (textField.text?.count)! > 0 {
            textField.backgroundColor = selectedButtonColor
        } else {
            textField.backgroundColor = unselectedButtonColor
        }
        //print("TextField should return method called")
        self.view.endEditing(true)
        // may be useful: textField.resignFirstResponder()
        return false
    }
    
}

// MARK: UITextFieldDelegate <---



