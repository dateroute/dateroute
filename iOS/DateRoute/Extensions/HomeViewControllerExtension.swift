//
//  homeViewControllerExtension.swift
//  DateRoute
//
//  Created by Cameron Korb on 2/9/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//
//Displays the information about each stop as the overlay on the mapview

import Foundation
import UIKit
import SwiftyJSON
import GoogleMaps
import Alamofire
import AlamofireImage

extension HomeViewController {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        //TODO: implement the same funcionality of didTap overlay

        print("tapped stop")
        // remove last tapped marker's info window
        if haveTappedMarker == true {
            lastTappedMarker.iconView = nil
        }
        
        mapView.animate(with: GMSCameraUpdate.setTarget(marker.position))
        mapView.selectedMarker = marker
        
        // Remove info window from previous marker
        infoWindow.removeFromSuperview()

        infoWindow = markerWindow(frame: CGRect(x: 0, y: 0, width: mapView.frame.width - 150, height: mapView.safeAreaLayoutGuide.layoutFrame.height/3))
        let location = marker.position
        
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = (infoWindow.center.y) - (infoWindowOffset)
        marker.tracksViewChanges = false
        newInfoWindow(marker: marker)
        
        let dragOffset : CGFloat = (UIApplication.shared.keyWindow?.frame.width)! * 4/7 // - spaceFromEdge
        let offset = dragOffset * CGFloat(0)
//        if routeDetailsCollectionView.contentOffset.x != offset {
//            routeDetailsCollectionViewCell.setContentOffset(CGPoint(x: offset, y: 0), animated: true)
//        }
        
        // set new marker
        lastTappedMarker = marker
        haveTappedMarker = true
    

        for route in ROUTES {
            if route.Name! == String((marker.title?.split(separator: Character(">"))[1])!) {
                let tabBarHeight = tabBarController?.tabBar.frame.height
                // Only dismiss route details if they are visible
                if routeDetailsVisible {
                    routeLauncher.dismissRouteDetails()
                }
                routeLauncher = routeDetailsLauncher()
                routeLauncher.displayRouteDetails(homeController: self, route: route, routeName: route.Name!, screenWidth: screenWidth!, screenHeight: screenHeight!, mapHeight: self.mapView.frame.height, mapView: self.mapView, tabBarHeight: tabBarHeight!, color: route.Color!)
                routeDetailsVisible = true
                }
            }
        
        // Remember to return false
        // so marker event is still handled by delegate
        return false
    }
    
    func newInfoWindow(marker:GMSMarker) {
        infoWindow.removeFromSuperview()
        if self.mapView.frame.height/3 < 180 {
            infoWindow = markerWindow(frame: CGRect(x: 0, y: 0, width: self.mapView.frame.width - 150, height: 180))
        } else {
            infoWindow = markerWindow(frame: CGRect(x: 0, y: 0, width: mapView.frame.width - 150, height: mapView.safeAreaLayoutGuide.layoutFrame.height/3))
        }

        let location = marker.position
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - infoWindowOffset
        self.view.addSubview(infoWindow)
        setUpInfoWindow(marker: marker)
    }
    
    func addMarkerInfoConstraints(markerView: markerWindow) {
        
        markerView.stopLabel.topAnchor.constraint(equalTo: markerView.topAnchor, constant: 10).isActive = true
        markerView.stopLabel.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 10).isActive = true
        markerView.stopLabel.rightAnchor.constraint(equalTo: markerView.rightAnchor, constant: -10).isActive = true
        //markerView.stopLabel.bottomAnchor.constraint(equalTo: markerView.bottomAnchor, constant: 10).isActive = true
        
        markerView.yelpRating.topAnchor.constraint(equalTo: markerView.stopLabel.bottomAnchor, constant: 5).isActive = true
        markerView.yelpRating.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 10).isActive = true
        markerView.yelpRating.rightAnchor.constraint(equalTo: markerView.leftAnchor, constant: 110).isActive = true
        
        markerView.priceCategoryLabel.topAnchor.constraint(equalTo: markerView.yelpRating.bottomAnchor, constant: 5).isActive = true
        markerView.priceCategoryLabel.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 10).isActive = true
        markerView.priceCategoryLabel.rightAnchor.constraint(equalTo: markerView.rightAnchor, constant: -10).isActive = true
        
        markerView.addressLabel.topAnchor.constraint(equalTo: markerView.priceCategoryLabel.bottomAnchor, constant: 10).isActive = true
        markerView.addressLabel.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 10).isActive = true
        markerView.addressLabel.rightAnchor.constraint(equalTo: markerView.centerXAnchor, constant: -5).isActive = true
        
        markerView.stopImage.topAnchor.constraint(equalTo: markerView.priceCategoryLabel.bottomAnchor, constant: 5).isActive = true
        markerView.stopImage.leftAnchor.constraint(equalTo: markerView.centerXAnchor, constant: 5).isActive = true
        markerView.stopImage.rightAnchor.constraint(equalTo: markerView.rightAnchor, constant: -15).isActive = true
        markerView.stopImage.bottomAnchor.constraint(equalTo: markerView.bottomAnchor, constant: -25).isActive = true
        
        
        markerView.phoneNumberButton.topAnchor.constraint(equalTo: markerView.addressLabel.bottomAnchor, constant: 0).isActive = true
        markerView.phoneNumberButton.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 10).isActive = true
        markerView.phoneNumberButton.rightAnchor.constraint(equalTo: markerView.stopImage.leftAnchor, constant: -10).isActive = true
        markerView.phoneNumberButton.bottomAnchor.constraint(equalTo: markerView.bottomAnchor, constant: -5).isActive = true
    }
    
    func setUpInfoWindow(marker:GMSMarker) {
        if marker.title!.contains("<>") {
            let stop_route = marker.title!.components(separatedBy: "<>")

            let stop = stop_route[0]
            let routeName = stop_route[1]
            var route: dateRoute?
            for r in ROUTES {
                if r.Name == routeName {
                    route = r
                }
            }
            var selectedStop : RouteStop?
            for s in (route?.Route)! {
                if s.Name! == stop {
                    selectedStop = s
                }
            }

            infoWindow.stopImage.setTitle(selectedStop?.Url, for: .normal)
            infoWindow.stopImage.titleLabel?.isHidden = true
            infoWindow.stopImage.addTarget(self, action: #selector(linkToYelp), for: .touchUpInside)

            let stopYelpInfo = selectedStop?.YelpInfo

            infoWindow.setupViews()
            infoWindow.layer.borderWidth = 0
            infoWindow.backgroundColor = UIColor.white
            infoWindow.layer.cornerRadius = 10
            infoWindow.layer.shadowColor = UIColor.black.cgColor
            infoWindow.layer.shadowOpacity = 0.7
            infoWindow.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
            infoWindow.layer.shadowRadius = 3.0

            infoWindow.stopLabel.text = stop

            let rating = stopYelpInfo!["rating"].rawString()
            infoWindow.yelpRating.image = UIImage(named: "regular_" + rating!)

            var categoriesText = ""
            let categories = stopYelpInfo!["categories"]
            for (count, category) in categories {
                let type = category["alias"]
                if Int(count)! == (categories.count)-1 {
                    categoriesText += type.stringValue
                }else if Int(count)! < 3 {
                    categoriesText += type.stringValue + ", "
                }
            }
            let price = stopYelpInfo!["price"]
            if price != JSON.null {
                infoWindow.priceCategoryLabel.text =  (price.stringValue) + "  •  " + categoriesText
            }else {
                infoWindow.priceCategoryLabel.text = "No price info  •  " + categoriesText
            }

            let phoneNumber = stopYelpInfo!["display_phone"].stringValue
            infoWindow.phoneNumberButton.setTitle(phoneNumber, for: .normal)
            

            let location = stopYelpInfo!["location"]["display_address"]
            var address = ""
            for (count, line) in location {
                if Int(count) == location.count-1 {
                    address += line.stringValue
                } else {
                    address += line.stringValue + "\n"
                }
            }
            infoWindow.addressLabel.text = address
            infoWindow.stopImage.setImage(selectedStop?.Image, for: .normal)

            addMarkerInfoConstraints(markerView: infoWindow)
            marker.map = self.mapView
        }
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let location = lastTappedMarker.position
        infoWindow.center = self.mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - infoWindowOffset
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {

        // Only dismiss route details if they are visible
        if routeDetailsVisible {
            routeLauncher.dismissRouteDetails()
            routeDetailsVisible = false
        }

        // Need to remove the infoWindow from the last tapped marker
        if haveTappedMarker == true {
            lastTappedMarker.iconView = nil
        }
        infoWindow.removeFromSuperview()
    }
    
    @objc func linkToYelp(sender:UIButton) {
        print("to yelp")
        let url = sender.title(for: .normal)
        
        UIApplication.shared.open((NSURL(string: url!)! as URL), options: [:]) { (done) in
            print("success")
        }
    }
}

class markerWindow: UIView {
    
    var color: UIColor!
    
    let stopLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Black", size: 18)
        //label.textColor = self.colors[1]
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.6
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let yelpRating: UIImageView = {
        let image = UIImageView()
        image.contentMode = UIView.ContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    
    let priceCategoryLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Black", size: 13)
        label.textColor = UIColor.gray
        label.numberOfLines = 0
        //label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let addressLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Black", size: 11)
        label.textColor = UIColor.gray
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let phoneNumberButton : UIButton = {
        let button = UIButton()
        button.titleLabel?.textAlignment = .left
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        //button.titleLabel?.font =  UIFont(name: "Avenir-Medium", size: 12)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector(handlePhoneNumberPressed), for: .touchUpInside)
        return button
    }()
    
    let stopImage : UIButton = {
        let button = UIButton()
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFill// scaleAspectFit
        button.imageView?.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
//        self.layer.borderWidth = 0
        //self.layer.borderColor = color.cgColor
        addSubview(stopLabel)
        addSubview(yelpRating)
        addSubview(priceCategoryLabel)
        addSubview(addressLabel)
        addSubview(stopImage)
        addSubview(phoneNumberButton)
    }
    
    @objc func handlePhoneNumberPressed(sender:UIButton) {
        let phone = sender.title(for: .normal)
        let callNumber = String((phone?.filter { "01234567890.".contains($0) })!)
        guard let number = URL(string: "tel://" + callNumber) else { return }
        UIApplication.shared.open(number)
    }

    
}


