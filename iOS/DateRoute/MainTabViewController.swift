//
//  MainTabViewController.swift
//  DateRoute
//
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupViewControllers()
        
        
    }
    
    func setupViewControllers(){
        //settings
        let flowLayout = FeaturedLayout()
        let featController = featuredViewController(collectionViewLayout: flowLayout)
        let featuredNavController = templateNavController(viewControllerTab: featController, selectedImage: #imageLiteral(resourceName: "featuredButtonSelected"), unselectedImage: #imageLiteral(resourceName: "featuredButton"))
        
        
        //Favorites
        let favoritesNavController = templateNavController(viewControllerTab: favoritesViewController(), selectedImage: #imageLiteral(resourceName: "favoritesButtonselected"), unselectedImage: #imageLiteral(resourceName: "favoritesButton"))
        
        //home
        tabBar.barTintColor = .white
        let homeNavController = templateNavController(viewControllerTab: HomeViewController(), selectedImage: #imageLiteral(resourceName: "searchButtonSelected.png"), unselectedImage: #imageLiteral(resourceName: "searchButton.png"))
        
        
        //checkin
        tabBar.barTintColor = .white
        let checkinNavController = templateNavController(viewControllerTab: checkinViewController(), selectedImage: #imageLiteral(resourceName: "checkinButtonSelected"), unselectedImage: #imageLiteral(resourceName: "checkinButton"))
        
        //profile
        tabBar.barTintColor = .white
        let profileNavController = templateNavController(viewControllerTab: ProfileViewController(), selectedImage: #imageLiteral(resourceName: "profileButtonSelected"), unselectedImage: #imageLiteral(resourceName: "profileButton"))
        
        
        tabBar.tintColor = UIColor(red:0.22, green:0.20, blue:0.21, alpha:1.0)
        
        viewControllers = [favoritesNavController, featuredNavController,  homeNavController, checkinNavController, profileNavController]
        selectedViewController = homeNavController
        
        guard let items = tabBar.items else { return }
        
        items[0].imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -8, right: 0)
        items[1].imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        items[2].imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        items[3].imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        items[4].imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        
    }
    
    fileprivate func templateNavController(viewControllerTab: UIViewController, selectedImage: UIImage, unselectedImage: UIImage) -> UINavigationController {
        let viewController = viewControllerTab
        let navController = UINavigationController(rootViewController: viewController)
        
        navController.tabBarItem.image = unselectedImage.withRenderingMode(.alwaysOriginal)
        navController.tabBarItem.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
        
        var delegate = navController.delegate
        
        

        //figure out why this doesnt work
//        navController.navigationBar.setBackgroundImage(UIImage.imageWithColor(color: .white), for: .default)
        
        return navController
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        if parent == nil{
            print("Back Button pressed.")
        }
    }
    
    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let newTabBarHeight = defaultTabBarHeight //+ 12.0
        
        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight
        
        tabBar.frame = newFrame
    }
}

