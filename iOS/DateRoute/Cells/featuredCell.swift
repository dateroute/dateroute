//
//  CollectionViewCell.swift
//  DateRoute
//
//  Created by Cameron Korb on 1/26/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import Foundation

class featuredCell: UICollectionViewCell, UIGestureRecognizerDelegate  {
    
    var pan: UIPanGestureRecognizer!
    var featured: Bool = false
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let imageCoverView: UIImageView = {
        let imageCoverView = UIImageView()
        imageCoverView.backgroundColor = UIColor.black
        imageCoverView.translatesAutoresizingMaskIntoConstraints = false
        imageCoverView.contentMode = UIView.ContentMode.scaleAspectFill
        return imageCoverView
    }()
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "Avenir-Black", size: 30)
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 2
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    let stopsLabel: UILabel = {
        let stopsLabel = UILabel()
        stopsLabel.textColor = UIColor.white
        stopsLabel.font = UIFont(name: "Avenir-Black", size: 18)
        stopsLabel.textAlignment = .center
        stopsLabel.numberOfLines = 0
        stopsLabel.translatesAutoresizingMaskIntoConstraints = false
        return stopsLabel
    }()
    
//    let swipeLabel: UILabel = {
//        let swipeLabel = UILabel()
//        swipeLabel.textColor = UIColor.white
//        swipeLabel.font = UIFont(name: "Avenir-Black", size: 14)
//        swipeLabel.text = "Swipe left to see route on map"
//        swipeLabel.textAlignment = .center
//        swipeLabel.numberOfLines = 0
//        swipeLabel.translatesAutoresizingMaskIntoConstraints = false
//        return swipeLabel
//    }()
    
    var route: FeaturedRoute? {
        didSet {
            if let route = route {
                imageView.image = route.backgroundImage
                titleLabel.text = route.title
                stopsLabel.text = route.stopsTogether
            }
        }
    }
    
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        
        
        addSubview(imageView)
        addSubview(imageCoverView)
        addSubview(titleLabel)
        addSubview(stopsLabel)
        //addSubview(swipeLabel)
        
        let standardHeight = FeaturedLayoutConstants.Cell.standardHeight
        let featuredHeight = FeaturedLayoutConstants.Cell.featuredHeight
        
        if self.frame.height == featuredHeight {
            self.featured = true
        } else {
            self.featured = false
        }
        
        let delta = 1 - ((featuredHeight - frame.height) / (featuredHeight - standardHeight))
        let minAlpha: CGFloat = 0.5
        let maxAlpha: CGFloat = 0.80
        imageCoverView.alpha = maxAlpha - (delta * (maxAlpha - minAlpha))
        
        let scale = max(delta, 0.55)
        titleLabel.transform = CGAffineTransform(scaleX: scale, y:scale)
        stopsLabel.alpha = delta
        //swipeLabel.alpha = delta
        
        let imageCoverViewConstraintLeading =  NSLayoutConstraint(item: imageCoverView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0)
        let imageCoverViewConstraintBottom = NSLayoutConstraint(item: imageCoverView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let imageCoverViewConstraintTop = NSLayoutConstraint(item: imageCoverView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let imageCoverViewConstraintTrailing = NSLayoutConstraint(item: imageCoverView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        
        
        let imageViewConstraintLeading = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0)
        let imageViewConstraintTop = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
        let imageViewConstraintBottom = NSLayoutConstraint(item: imageView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let imageViewConstraintTrailing = NSLayoutConstraint(item: imageView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        
        
        let titleLabelCenterY = NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: imageCoverView, attribute: .centerY, multiplier: 1.0, constant: -10.0)
        let titleLabelTrailing = NSLayoutConstraint(item: titleLabel, attribute: .trailing, relatedBy: .equal, toItem: imageView, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        let titleLabelLeading = NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: imageCoverView, attribute: .leading, multiplier: 1.0, constant: 0.0)

        
        let stopsLabelTrailing = NSLayoutConstraint(item: stopsLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        let stopsLabelTop = NSLayoutConstraint(item: stopsLabel, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1.0, constant: 10.0)
        let stopsLabelCenterX = NSLayoutConstraint(item: stopsLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let stopsLabelLeading = NSLayoutConstraint(item: stopsLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0)
        
//        let swipeLabelTrailing = NSLayoutConstraint(item: swipeLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0)
//        let swipeLabelTop = NSLayoutConstraint(item: swipeLabel, attribute: .bottom, relatedBy: .equal, toItem: imageCoverView, attribute: .bottom, multiplier: 1.0, constant: -20.0)
//        let swipeLabelCenterX = NSLayoutConstraint(item: swipeLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0)
//        let swipeLabelLeading = NSLayoutConstraint(item: swipeLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0)
        
        
        //translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([imageCoverViewConstraintLeading, imageCoverViewConstraintBottom, imageCoverViewConstraintTop, imageCoverViewConstraintTrailing, imageViewConstraintLeading, imageViewConstraintTop, imageViewConstraintBottom, imageViewConstraintTrailing, titleLabelCenterY, titleLabelTrailing, titleLabelLeading, stopsLabelTrailing, stopsLabelTop, stopsLabelCenterX, stopsLabelLeading]) //, swipeLabelTrailing, swipeLabelTop, swipeLabelCenterX, swipeLabelLeading])
        
        
        
//        pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
//        pan.delegate = self
//        self.addGestureRecognizer(pan)
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        if (pan.state == UIGestureRecognizer.State.changed) {
//            let p: CGPoint = pan.translation(in: self)
//            let width = self.contentView.frame.width
//            let height = self.contentView.frame.height
//            self.contentView.frame = CGRect(x: p.x,y: 0, width: width, height: height);
//            //self.deleteLabel1.frame = CGRect(x: p.x - deleteLabel1.frame.size.width-10, y: 0, width: 100, height: height)
//            //self.deleteLabel2.frame = CGRect(x: p.x + width + deleteLabel2.frame.size.width, y: 0, width: 100, height: height)
//        }
//
//    }
    
    
    
//    @objc func handlePan(_ pan: UIPanGestureRecognizer) {
//                if pan.state == UIGestureRecognizer.State.began {
//
//                } else if pan.state == UIGestureRecognizer.State.changed {
//                    self.setNeedsLayout()
//                } else {
//                    if pan.velocity(in: self).x < -400 && abs((pan.velocity(in: pan.view)).x) > abs((pan.velocity(in: pan.view)).y){
//                        let collectionView: UICollectionView = self.superview as! UICollectionView
//                        let indexPath: IndexPath = collectionView.indexPathForItem(at: self.center)!
//                        collectionView.delegate?.collectionView!(collectionView, performAction: #selector(handlePan(_:)), forItemAt: indexPath, withSender: nil)
//                    } else {
//                        UIView.animate(withDuration: 0.2, animations: {
//                            self.setNeedsLayout()
//                            self.layoutIfNeeded()
//                        })
//
//                    }
//                }
//        if pan.state == UIGestureRecognizer.State.ended {
//            //self.swipeLabel.fadeOut(duration: 2.0, delay: 1.0)
//        }
//    }
    
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
//
//    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        return abs((pan.velocity(in: pan.view)).x) > 0 || abs((pan.velocity(in: pan.view)).y) > 0//abs((pan.velocity(in: pan.view)).x) > abs((pan.velocity(in: pan.view)).y)
//    }
    

}

