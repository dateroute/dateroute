//
//  favoritesCell.swift
//  DateRoute
//
//  Created by David Sherwood on 7/7/19.
//  Copyright © 2019 DateRoute. All rights reserved.
//

import UIKit
class favoritesCell: UITableViewCell {
    
    //var routeName = UILabel()
    let routeName: UILabel = {
        let routeName = UILabel()
        routeName.textColor = UIColor(red:0.5, green:0.5, blue:0.5, alpha:1.0)
        routeName.font = UIFont(name: "Avenir-Black", size: 20)
        routeName.numberOfLines = 0
        routeName.translatesAutoresizingMaskIntoConstraints = true
        return routeName
    }()
    let stops: UILabel = {
        let stops = UILabel()
        stops.textColor = UIColor.black
        stops.font = UIFont(name: "Avenir", size: 14)
        stops.numberOfLines = 2
        stops.translatesAutoresizingMaskIntoConstraints = true
        return stops
    }()
    
    let routeIcon: UIImageView = {
        let icon = UIImageView(image: UIImage(imageLiteralResourceName: "temp story"))
        icon.translatesAutoresizingMaskIntoConstraints = true
        return icon
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.contentView.addSubview(routeIcon)
        self.contentView.addSubview(routeName)
        self.contentView.addSubview(stops)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        routeName.frame = CGRect(x: 80, y: 8, width: self.bounds.size.width-80, height: 30)
        stops.frame = CGRect(x: 80, y: 36, width: self.bounds.size.width-80, height: 40)
        routeIcon.frame = CGRect(x: 10, y: 10, width: 63, height: 63)
        stops.sizeToFit()
    }
}
