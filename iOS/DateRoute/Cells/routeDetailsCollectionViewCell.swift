//
//  routeDetailsCollectionViewCell.swift
//  DateRoute
//
//  Created by Cameron Korb on 1/29/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit

class routeDetailsCollectionViewCell: UICollectionViewCell {
    
    let stopNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Black", size: 20)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.isUserInteractionEnabled = false
        return label
    }()
    
    let stopImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = UIView.ContentMode.scaleAspectFit
        //image.clipsToBounds = true
        image.isUserInteractionEnabled = false
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.backgroundColor = UIColor(red:0.47, green:0.64, blue:0.73, alpha:1.0) //UIColor.white//
        //addSubview(stopLabel)
        addSubview(stopNameLabel)
        addSubview(stopImage)
        addViewConstraints()
    }
    
    func addViewConstraints() {
        //self.frame = CGRect(x: 0, y: 100, width: routeDetailsLauncher().cellWidth, height: 100)

    }
}
