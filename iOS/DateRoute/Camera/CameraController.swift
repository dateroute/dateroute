//
//  CameraController.swift
//  DateRoute
//
//  Created by Vedaant Paul Kukadia  on 6/9/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import AVFoundation

class CameraController: UIViewController, AVCapturePhotoCaptureDelegate, UIViewControllerTransitioningDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    //dismiss button
    let dismissButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "left-arrow"), for: .normal)
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    
    @objc func handleDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //capture photo button
    let captureButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "capture photo"), for: .normal)
        button.addTarget(self, action: #selector(handleCapturePhoto), for: .touchUpInside)
        return button
    }()
    
    //choose from camera roll button
    let selectFromPhotosButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "choose from camera roll"), for: .normal)
        button.addTarget(self, action: #selector(handleSelectPhoto), for: .touchUpInside)
        return button
    }()
    
    //switch camera button
    let switchCameraButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "switch camera "), for: .normal)
        button.addTarget(self, action: #selector(handleSwitchCamera), for: .touchUpInside)
        return button
    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        transitioningDelegate = self
        
        setUpCaptureSession()
        setupHUD()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    let customAnimationPresentor = CustomAnimationPresentor()
    let customAnimationDismisser = CustomAnimationDismisser()
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customAnimationPresentor
        
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customAnimationDismisser
    }
    
    fileprivate func setupHUD() {
        //add capture photo button to view
        view.addSubview(captureButton)
        captureButton.anchor(top: nil, left: nil, bottom: view.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 90, paddingRight: 0, width: 80, height: 80)
        captureButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        //add dismiss button to view
        view.addSubview(dismissButton)
        dismissButton.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 25, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        //add choose from camera roll button
        view.addSubview(selectFromPhotosButton)
        selectFromPhotosButton.anchor(top: nil, left: nil, bottom: nil, right: captureButton.leftAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 50, paddingRight: 30, width: 42, height: 44)
        selectFromPhotosButton.centerYAnchor.constraint(equalTo: captureButton.centerYAnchor).isActive = true
        
        //switch camera
        view.addSubview(switchCameraButton)
        switchCameraButton.anchor(top: nil, left: captureButton.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 30, paddingBottom: 50, paddingRight: 0, width: 39, height: 34)
        switchCameraButton.centerYAnchor.constraint(equalTo: captureButton.centerYAnchor).isActive = true
        
        
        
    }
    
    @objc func handleCapturePhoto() {
        print("Capturing photo...")
        
        let settings = AVCapturePhotoSettings()
        
        guard let previewFormatType = settings.availablePreviewPhotoPixelFormatTypes.first else { return }
        settings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewFormatType]
        
        output.capturePhoto(with: settings, delegate: self)
    }
    
    @objc func handleSelectPhoto() {
        print("Choose from camera roll...")
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        dismiss(animated: false, completion: nil)
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            Variables.checkinImage = editedImage.withRenderingMode(.alwaysOriginal)
            Variables.checkinStory.append(editedImage.withRenderingMode(.alwaysOriginal))
            
        } else if let originalImage =
            info["UIImagePickerControllerOriginalImage"] as? UIImage{
            Variables.checkinImage = originalImage.withRenderingMode(.alwaysOriginal)
            Variables.checkinStory.append(originalImage.withRenderingMode(.alwaysOriginal))
        }

//        guard let image = self.profImageView.image else {
//            return
//        }
        
//        guard let uploadData = UIImageJPEGRepresentation(image, 0.3) else{
//            return
//        }
        
//        let filename = NSUUID().uuidString
        
        //        let storageRef = Storage.storage().reference().child()
        //        let profileImageRef = StorageReference.child("profile_images")
        //
//        Storage.storage().reference().child("profile_images").child(filename).putData(uploadData, metadata: nil) { (metadata, err) in
//
//            if let err = err {
//                print("failed to upload profile image: ", err)
//                return
//            }
//
//            Storage.storage().reference().child("profile_images").child(filename).downloadURL { (url, error) in
//                guard let downloadURL = url else {
//                    // Uh-oh, an error occurred!
//                    return
//                }
//
//                //Save the new profile picture to Firebase
//                guard let UserID = Auth.auth().currentUser?.uid else { return }
//                Database.database().reference().child("users").child(UserID).updateChildValues(["profileImageUrl" : downloadURL.absoluteString])
//
//                print("Successfully uploaded prof image: ", downloadURL)
//
//            }
//
//        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc func handleSwitchCamera() {
        print("Switching camera...")
//        setUpFrontCaptureSession()
//        setupHUD()
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?){
        
        let imageData = photo.fileDataRepresentation()
        
        let previewImage = UIImage(data: imageData!)
        
        let containerView = PreviewPhotoContainerView()
        containerView.previewImageView.image = previewImage
        view.addSubview(containerView)
        containerView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        
//        let previewImageView = UIImageView(image: previewImage)
//        view.addSubview(previewImageView)
//        previewImageView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
//        
//        print("Finish processing photo sample...")
    }
    
    
    
    let output = AVCapturePhotoOutput()
    
    fileprivate func setUpCaptureSession() {
        let captureSession = AVCaptureSession()
        
        //1. Setup inputs
        
        guard let captureDevice  = AVCaptureDevice.default(for: .video) else { return }
        
        
        do {
            
            let input = try AVCaptureDeviceInput(device: captureDevice)
            if captureSession.canAddInput(input){
                captureSession.addInput(input)
            }
 
        } catch let err {
            print("Could not setup camera input:", err)
        }
                
        //2. Setup outputs
        
        
        if captureSession.canAddOutput(output) {
            captureSession.addOutput(output)
        }
        //3. setup output preview
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.frame
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
//    fileprivate func setUpFrontCaptureSession() {
//        let captureSession = AVCaptureSession()
//
//        //1. Setup inputs
//
////        guard let captureDevice  = AVCaptureDevice.default(for: .video) else { return }
//        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else { return }
//
//
//        do {
//
//            let input = try AVCaptureDeviceInput(device: captureDevice)
//            if captureSession.canAddInput(input){
//                captureSession.addInput(input)
//            }
//
//        } catch let err {
//            print("Could not setup camera input:", err)
//        }
//
//        //2. Setup outputs
//
//
//        if captureSession.canAddOutput(output) {
//            captureSession.addOutput(output)
//        }
//        //3. setup output preview
//        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//        previewLayer.frame = view.frame
//        view.layer.addSublayer(previewLayer)
//
//        captureSession.startRunning()
//    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        print("Yes, reloading camera controller")
        

        
    }
    
    
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
