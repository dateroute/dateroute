//
//  PreviewPhotoContainerView.swift
//  DateRoute
//
//  Created by Vedaant Paul Kukadia  on 6/9/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import Photos


class PreviewPhotoContainerView: UIView {
    
    
    let previewImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "cancel button"), for: .normal)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return button
        
    }()
    
    let saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "save button"), for: .normal)
        button.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        return button
        
    }()
    
    let shareButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage( #imageLiteral(resourceName: "share arrow"), for: .normal)
        button.addTarget(self, action: #selector(handleShare), for: .touchUpInside)
        return button
        
    }()
    
    @objc func handleShare(){
        print("Handling share...")
        
        guard let previewImage = previewImageView.image else { return }
        Variables.checkinImage = previewImage
        Variables.checkinStory.append(previewImage)
        self.removeFromSuperview()
        //TODO: return to main check in page
        //TODO: save to firebase for 24 hours
        
        
    }
    
    @objc func handleSave(){
        print("handling save...")
        
        guard let previewImage = previewImageView.image else { return }
        
        let library = PHPhotoLibrary.shared()
        
        library.performChanges({
            
            PHAssetChangeRequest.creationRequestForAsset(from: previewImage)
            
        }) { (success, err) in
            if let err = err{
                print("Failed to save image to photo library:", err)
                return
            }
            
            print("Successfully saved image to library")
            
            DispatchQueue.main.async {
                
                let savedLabel = UILabel()
                savedLabel.text = "Saved!"
                savedLabel.font = UIFont.boldSystemFont(ofSize: 18)
                savedLabel.textColor = .white
                savedLabel.numberOfLines = 0
                savedLabel.backgroundColor = UIColor(white: 0, alpha: 0.3)
                savedLabel.textAlignment = .center
                
                savedLabel.frame = CGRect(x: 0, y: 0, width: 150, height: 80)
                savedLabel.center = self.center
                
                self.addSubview(savedLabel)
                
                savedLabel.layer.transform = CATransform3DMakeScale(0, 0, 0)
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    
                    savedLabel.layer.transform = CATransform3DMakeScale(1, 1, 1)
                    
                }, completion: { (completed) in
                    //completed
                    
                    UIView.animate(withDuration: 0.5, delay: 0.75, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                        
                        savedLabel.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.1)
                        savedLabel.alpha = 0
                        
                    }, completion: { (_) in
                        
                        savedLabel.removeFromSuperview()
                        
                        
                    })
                    
                    
                })
                
            }
            
            
            
        }
        
    }
    
    @objc func handleCancel(){
        self.removeFromSuperview()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
        
        addSubview(previewImageView)
        previewImageView.anchor(top: topAnchor , left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 74, paddingLeft: 0, paddingBottom: 72, paddingRight: 0, width: 0, height: 0)
        
        addSubview(cancelButton)
        cancelButton.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 35, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 30, height: 30)
        
        addSubview(saveButton)
        saveButton.anchor(top: nil, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 24, paddingBottom: 25, paddingRight: 0, width: 30, height: 30)
        
        addSubview(shareButton)
        shareButton.anchor(top: nil, left: nil, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 25, paddingRight: 24, width: 30, height: 30)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
