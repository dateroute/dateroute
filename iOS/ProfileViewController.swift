//
//  ProfileController.swift
//  DateRoute
//
//  Created by Vedaant Paul Kukadia  on 1/14/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase
import FirebaseDatabase

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    var profImageView : UIImageView!
    var label1: UILabel!
    var ref = Database.database().reference()
    var indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    //button to log user out
    let logoutButton: UIButton = {
        let logoutButton = UIButton()
        logoutButton.setImage(#imageLiteral(resourceName: "facebookLogout").withRenderingMode(.alwaysOriginal), for: .normal)
        logoutButton.setImage(#imageLiteral(resourceName: "facebookLogout").withRenderingMode(.alwaysOriginal), for: .highlighted)
        logoutButton.addTarget(self, action: #selector(handleLogoutPress), for: .touchUpInside)
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        return logoutButton
    }()
    
    //email Label
    let emailLabel: UILabel = {
        let emailLabel = UILabel()
        emailLabel.text = "Email"
        emailLabel.font = UIFont(name: "Avenir-Heavy", size: 14)
        emailLabel.textColor = UIColor(red:0, green:0, blue:0, alpha:1.0)
        emailLabel.translatesAutoresizingMaskIntoConstraints = false
        return emailLabel
    }()
    
    //to add email
    let addEmail: UILabel = {
        let addEmail = UILabel()
        addEmail.font = UIFont(name: "Avenir-Book", size: 18)
        addEmail.text = "Email Address "
        addEmail.textColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0)
        addEmail.translatesAutoresizingMaskIntoConstraints = false
        return addEmail
    }()
    
    
    //Date of Birth Label
    let doblLabel: UILabel = {
        let doblLabel = UILabel()
        doblLabel.text = "Date of Birth"
        doblLabel.font = UIFont(name: "Avenir-Heavy", size: 14)
        doblLabel.textColor = UIColor(red:0, green:0, blue:0, alpha:1.0)
        doblLabel.translatesAutoresizingMaskIntoConstraints = false
        return doblLabel
    }()
    
    
//    button to add email
    let addDOB: UITextField = {
        let addDOB = UITextField()
        addDOB.placeholder = "MM/DD/YYYY"
        addDOB.font = UIFont(name: "Avenir-Book", size: 18)
        addDOB.textColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0)
        addDOB.keyboardType = .numberPad
        addDOB.addTarget(self, action: #selector(handleSaveUserDOB), for: .editingDidEnd)
        addDOB.translatesAutoresizingMaskIntoConstraints = false
        return addDOB
    }()

    
    //Home City Label
    let homeLabel: UILabel = {
        let homeLabel = UILabel()
        homeLabel.text = "Home City"
        homeLabel.font = UIFont(name: "Avenir-Heavy", size: 14)
        homeLabel.textColor = UIColor(red:0, green:0, blue:0, alpha:1.0)
        homeLabel.translatesAutoresizingMaskIntoConstraints = false
        return homeLabel
    }()
    
    //button to add home
    let addhome: UITextField = {
        let addhome = UITextField()
        addhome.placeholder = "click to add"
        addhome.font = UIFont(name: "Avenir-Book", size: 18)
        addhome.textColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0)
        addhome.addTarget(self, action: #selector(handleSaveUserHome), for: .editingDidEnd)
        addhome.translatesAutoresizingMaskIntoConstraints = false
        return addhome
    }()

    
    //button that allows the user to edit their profile picture
    let editProfButton: UIButton = {
        let button = UIButton(type: .system)
        let attributedTitle = NSMutableAttributedString(string: "change photo", attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir-Heavy", size: 14) as Any, NSAttributedString.Key.foregroundColor: UIColor(red:74/255, green:144/255, blue:226/255, alpha:1.0)])
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.addTarget(self, action: #selector(handleEditProf), for: .touchUpInside)
        return button
    }()
    
    //funciton for editing profile picture 
    @objc func handleEditProf(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

  
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
//                        let url = info[UIImagePickerControllerReferenceURL] as? NSURL
//                        self.profImageView.image = UIImage(data: NSData(contentsOf: url! as URL)! as Data)
            
            self.profImageView.image = editedImage.withRenderingMode(.alwaysOriginal)
            
        } else if let originalImage =
            info["UIImagePickerControllerOriginalImage"] as? UIImage{
            self.profImageView.image = originalImage.withRenderingMode(.alwaysOriginal)
        }
        
        profImageView.layer.cornerRadius = profImageView.frame.height/2
        profImageView.layer.masksToBounds = true
        profImageView.layer.borderColor = UIColor.black.cgColor
        profImageView.layer.borderWidth = 0
        
        guard let image = self.profImageView.image else {
            return
        }
        
        guard let uploadData = image.jpegData(compressionQuality: 0.3) else{
            return
        }
        
        let filename = NSUUID().uuidString
        
//        let storageRef = Storage.storage().reference().child()
//        let profileImageRef = StorageReference.child("profile_images")
//
        Storage.storage().reference().child("profile_images").child(filename).putData(uploadData, metadata: nil) { (metadata, err) in
            
            if let err = err {
                print("failed to upload profile image: ", err)
                return
            }
            
            Storage.storage().reference().child("profile_images").child(filename).downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    return
                }

                //Save the new profile picture to Firebase
                guard let UserID = Auth.auth().currentUser?.uid else { return }
                self.ref.child("users").child(UserID).updateChildValues(["profileImageUrl" : downloadURL.absoluteString])
                
                print("Successfully uploaded prof image: ", downloadURL)
                
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    //label for the users profile name
    let profileNameLbl: UILabel = {
        let profileNameLbl = UILabel()
        profileNameLbl.font = UIFont(name: "Avenir-Roman", size: 20)
        profileNameLbl.textColor = UIColor(red:0, green:0, blue:0, alpha:1.0)
        profileNameLbl.numberOfLines = 2
        profileNameLbl.translatesAutoresizingMaskIntoConstraints = false
        return profileNameLbl
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Change to the correct color and font
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)
        UIApplication.shared.statusBarStyle = .default
    
        let label = UILabel(frame: CGRect(x: view.center.x, y: view.center.y+66, width: 97, height: 33))
        label.font = UIFont(name: "Avenir-Heavy", size: 24)
        label.text = "My Profile"
        label.textColor = UIColor(red:1, green:1, blue:1, alpha:1.0)
        navigationItem.titleView = label
        self.hideKeyboardWhenTappedAround()
        view.backgroundColor = UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.0)
        
        
        profImageView = UIImageView() 
        profImageView.image = #imageLiteral(resourceName: "temp prof")
        profImageView.layer.masksToBounds = false
        profImageView.layer.cornerRadius = 50
        //profImageView.layer.cornerRadius = profImageView.frame.width/2
        profImageView.translatesAutoresizingMaskIntoConstraints = false
        profImageView.clipsToBounds = true
        view.addSubview(profImageView)
        profImageView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 20, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 100, height: 100)
        
        fetchUser()

        //add subviews to the view
        
        // Add loading indicator (for when we load the data from firebase)
        view.addSubview(self.indicator)
        self.indicator.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
        self.indicator.center = view.center
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 5, y: 5)
        self.indicator.transform = transform

        view.addSubview(profileNameLbl)
        profileNameLbl.topAnchor.constraint(equalTo: profImageView.topAnchor, constant: 5).isActive = true
        profileNameLbl.leftAnchor.constraint(equalTo: profImageView.rightAnchor, constant: 10).isActive = true
        profileNameLbl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -50).isActive = true
        profileNameLbl.centerYAnchor.constraint(equalTo: profImageView.centerYAnchor, constant: -15 ).isActive = true
        
        view.addSubview(editProfButton)
        editProfButton.anchor(top: profImageView.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 3, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 85, height: 18)
        editProfButton.centerXAnchor.constraint(equalTo: profImageView.centerXAnchor ).isActive = true


        view.addSubview(emailLabel)
        emailLabel.anchor(top: editProfButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 25, paddingLeft: 31, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        view.addSubview(addEmail)
        addEmail.anchor(top: emailLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 31, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        view.addSubview(doblLabel)
        doblLabel.anchor(top: addEmail.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 31, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        view.addSubview(addDOB)
        addDOB.anchor(top: doblLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: -5, paddingLeft: 31, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addDOB.delegate = self
        
        view.addSubview(homeLabel)
        homeLabel.anchor(top: addDOB.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 31, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        view.addSubview(addhome)
        addhome.anchor(top: homeLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, paddingTop: -5, paddingLeft: 31, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        
        //Change to the correct color and font
        navigationItem.titleView = label
        
        
        //Facebook logout button
        view.addSubview(logoutButton)
        logoutButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        logoutButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
    }
    
    fileprivate func fetchUser(){
        // Animate loading image while loading from firebase
        self.indicator.startAnimating()
        print("fetching USER")
        guard let UserID = Auth.auth().currentUser?.uid else { return }
    
        print("getting here also")
        self.ref.child("users").child(UserID).observeSingleEvent(of: .value, with: { (snapshot) in
            print(snapshot.value ?? "")

            print("getting in databse")
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            print("1.1")
            guard let username = dictionary["username"] else { return }
            print("1.2")
            guard let userEmail = dictionary["email"] else { return }
            print("1.3")
            guard let FBid = dictionary["FBid"] else { return }
            print("1.4")
            
            guard let imageURL = dictionary["profileImageUrl"] else { return }
            print("1.5")
            
            
            guard let DOB = dictionary["Date of Birth"] else { return }
            
            guard let Home = dictionary["Hometown"] else { return }

            //set user profile name
            self.profileNameLbl.text = username as? String

            self.addDOB.placeholder = DOB as? String
            
            self.addhome.placeholder = Home as? String

            self.addEmail.text = userEmail as? String

            let url = NSURL(string: (imageURL as? String)!)

            self.profImageView.image = UIImage(data: NSData(contentsOf: url! as URL)! as Data)
            // stop animating loading image now that we've loaded the routes
            self.indicator.stopAnimating()

        }){ (err) in
            print("Failed to fetch user", err)
        }
        

    }
    
    @objc func handleSaveUserDOB(){
        
        guard let UserID = Auth.auth().currentUser?.uid else { return }
        //Save the new DOB to Firebase
        self.ref.child("users").child(UserID).updateChildValues(["Date of Birth" : addDOB.text ?? "MM/DD/YYYY"])
    }
    
    @objc func handleSaveUserHome(){
        
        guard let UserID = Auth.auth().currentUser?.uid else { return }
        //Save the new Hometown to Firebase
        self.ref.child("users").child(UserID).updateChildValues(["Hometown" : addhome.text ?? "click to add"])
        
    }
    
    @objc func handleLogoutPress() {
        
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to Log Out?", preferredStyle: .actionSheet)
        
        present(alertController, animated: true, completion: nil)
        
        alertController.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { (_) in
            
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            if (FBSDKAccessToken.current() == nil){
                let logoutViewController = ViewController()
                //
                self.present(logoutViewController, animated: false, completion: nil)
                print("Logout Succesful")
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
 
    }
    
    
    //Function to make input for DOB as MM/DD/YYYY
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Format Date of Birth dd-MM-yyyy
        
        //initially identify your textfield
        
        if textField == addDOB {
            
            // check the chars length dd -->2 at the same time calculate the dd-MM --> 5
            if (addDOB.text?.count == 2) || (addDOB.text?.count == 5) {
                //Handle backspace being pressed
                if !(string == "") {
                    // append the text
                    addDOB.text = (addDOB.text)! + "/"
                }
            }
            // check the condition not exceed 9 chars
            return !(textField.text!.count > 9 && (string.count ) > range.length)
        }
        else {
            return true
        }
    }
    
    
    
}







// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
