//
//  activityButton.swift
//  DateRoute
//
//  Created by Cameron Korb on 4/26/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit

class ovalButton: UIButton {

    //let color = UIColor.white
    let fillAlpha: CGFloat = 0.3
    let selectedColor = UIColor(red:114/255, green:161/255, blue:187/255, alpha:1.0)
    let bordercolor = UIColor.black
    let selectedTextColor = UIColor.white
    var height:CGFloat = 52
    var width:CGFloat = 0
    let fillColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0)
    let titleColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:1.0)
    var fontSize:CGFloat = 18
    var name = "name"
    var widthConstraint: NSLayoutConstraint?
    var heightConstraint: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        //self.frame.size = CGSize(width: (self.titleLabel?.frame.width)! , height: height)
        self.titleLabel?.font = UIFont(name: "Avenir-Black", size: fontSize)
        self.layer.cornerRadius = 19
        self.layer.borderWidth = 1
        self.layer.backgroundColor = fillColor.cgColor
        self.layer.borderColor = bordercolor.cgColor
        self.setTitleColor(UIColor.black, for: .normal)
        //self.setTitleColor(UIColor.white, for: .selected)
        self.isOpaque = true
    }
    

    func setWidth(){
        let width = (self.title(for: .normal)! as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: self.fontSize)])
        var widthPadding: CGFloat = 40
//        if self.height > 35 {
//            widthPadding = 3/2*self.height
//        }
        removeConstraints(self.constraints)
        widthConstraint = self.widthAnchor.constraint(equalToConstant: width.width + widthPadding)
        widthConstraint?.isActive = true
        widthConstraint?.identifier = "widthConstraint"
        self.width = width.width + widthPadding
        //self.width = 100 + widthPadding
    }
    
    
    
    func setHeight(height:CGFloat){
        let setHeight = height
        heightConstraint = heightAnchor.constraint(equalToConstant: setHeight)
        heightConstraint?.isActive = true
        self.layer.cornerRadius = setHeight/2
        self.fontSize = setHeight/3
        self.titleLabel?.font = UIFont(name: "Avenir-Black", size: fontSize)
        self.height = setHeight
    }
    
    func setConstraints(top:NSLayoutYAxisAnchor?, left:NSLayoutXAxisAnchor?, right:NSLayoutXAxisAnchor?, center:NSLayoutXAxisAnchor?, padding:CGFloat?) {
    
        if top != nil {
            self.topAnchor.constraint(equalTo: top!, constant: 12).isActive = true
        }
        if left != nil {
            self.leftAnchor.constraint(equalTo: left!, constant: padding!).isActive = true
        }
        if right != nil {
            self.rightAnchor.constraint(equalTo: right!, constant: -1*padding!).isActive = true
        }
        if center != nil {
            self.centerXAnchor.constraint(equalTo: center!, constant: 0).isActive = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
