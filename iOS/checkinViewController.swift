//
//  checkinViewController.swift
//  DateRoute
//
//  Created by Vedaant Paul Kukadia  on 1/14/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces

///For future change this to a UICollectionView

class checkinViewController: UIViewController {
    
    var homeController: HomeViewController?
    var currentRoute: dateRoute?
    var onRoute: Bool = false
    var listStops: [UILabel] = []
    var listCheckinButtons: [UIButton] = []
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var storyImage : UIImageView!
    
    //Story button
    let storyButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "story oval"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleStory), for: .touchUpInside)
        return button
    }()
    
    //add to story button
    let addToStoryButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "add to story"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleAddToStory), for: .touchUpInside)
        return button
    }()
    
    
    
    
    
    //Search bar
    let tableView: UITableView = {
        let tableView = UITableView()
//        tableView.separatorStyle = .none
        return tableView
        
    }()
    
    let currentRouteLabel = UILabel()
    
    let createRouteLabel = UILabel()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = (self as GMSAutocompleteResultsViewControllerDelegate)
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.dimsBackgroundDuringPresentation = true
        searchController?.searchBar.searchBarStyle = UISearchBar.Style.prominent
//        searchController?.edgesForExtendedLayout = view.safeAre
        searchController?.searchBar.placeholder = "look for a different place"
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        navigationItem.searchController = searchController
        searchController?.isActive = true
        

        //Story
        storyImage = UIImageView()
        storyImage.image = #imageLiteral(resourceName: "temp story")
        storyImage.layer.masksToBounds = false
        storyImage.layer.cornerRadius = 50
        storyImage.translatesAutoresizingMaskIntoConstraints = false
        storyImage.clipsToBounds = true
        
        //"current route" label
        currentRouteLabel.textAlignment = .center
        currentRouteLabel.text = "my current route"
        currentRouteLabel.font = UIFont(name: "Avenir-Heavy", size: 18)
        currentRouteLabel.textColor = UIColor(red:120/255, green:162/255, blue:187/255, alpha:1.0)
        currentRouteLabel.isHidden = false
        currentRouteLabel.translatesAutoresizingMaskIntoConstraints = false
        
        //"current route" label
        createRouteLabel.textAlignment = .center
        createRouteLabel.text = "Start a route to check in, or search for locations above"
        createRouteLabel.font = UIFont(name: "Avenir-Heavy", size: 18)
        createRouteLabel.numberOfLines = 3
        createRouteLabel.textColor = UIColor.gray
        createRouteLabel.isHidden = false
        createRouteLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.hideKeyboardWhenTappedAround()
    
        
        print("ONROUTE", onRoute)
        
       //background color of the page
        view.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
        
        //handle navigation bar
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Heavy", size: 24)
        label.text = "Check In"
        label.textColor = UIColor(red:1, green:1, blue:1, alpha:1.0)
        navigationItem.titleView = label
        navigationController?.navigationBar.barTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)

        
        //Add "my current route" label to the UIView
        self.view.addSubview(currentRouteLabel)
        currentRouteLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 142, height: 25)
        currentRouteLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor ).isActive = true

        self.view.addSubview(createRouteLabel)
        createRouteLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 100, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 300, height: 100)
        createRouteLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor ).isActive = true
        
        
    }
    
    
    @objc func handleCheckin(_ sender: UIButton){
        Variables.checkedinIDs.append(sender.tag)
        
        let checkinController = checkintoViewController()
        for (i,stop) in (currentRoute?.RouteOrder)! {
            if i==sender.tag {
                checkinController.stopName = stop.Name
            }
        }
        navigationController?.pushViewController(checkinController, animated: true)
        Variables.checkinImage = #imageLiteral(resourceName: "add a photo")
        
    }
    
    @objc func handleStory(){
        print("handling view story")
        let containerView = StoryContainerView()
        containerView.previewImageView.image = Variables.checkinStory[0]
        

        UIApplication.shared.keyWindow?.addSubview(containerView)
        containerView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)

    }
    
    @objc func handleAddToStory(){
        print("handling adding image to story...")
        
        let alert = UIAlertController(title: "Not available in beta version", message: "When you add a story it will only be visible to you", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
            let cameraController = CameraController()
            self.present(cameraController, animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
        print("Yes, reloading!!")
        currentRoute = Variables.currentRoute
        onRoute = Variables.onRoute
        print("DateRoute", currentRoute as Any)
        print("onroute", onRoute)
        

        //Handle making the labels for each date
        
        let xPos = 52
        var yPos = 128
        
        if onRoute{
            
            removeDataFromPrevRoute()
            addStoryFunctionality()
            currentRouteLabel.isHidden = false
            createRouteLabel.isHidden = true
            
            if Variables.checkinStory.count > 0{
                storyImage.image = Variables.checkinStory[0]
                storyImage.layer.cornerRadius = 47
                storyImage.clipsToBounds = true
                storyImage.layer.borderColor = UIColor.black.cgColor
                storyImage.layer.borderWidth = 0.3
                storyButton.isEnabled = true
                
            } else {
                storyImage.image = #imageLiteral(resourceName: "temp story")
                storyImage.layer.masksToBounds = false
                storyImage.layer.cornerRadius = 50
                storyImage.translatesAutoresizingMaskIntoConstraints = false
                storyImage.clipsToBounds = true
                storyButton.isEnabled = false
            }
            
            for (i,stop) in (currentRoute?.RouteOrder)! {
                
                //var i: Int = 0
                //add the stop labels
                let stopLabel = UILabel()
                stopLabel.text = stop.Name
                print("label text", stopLabel.text ?? "", " at height: ", yPos)
                stopLabel.font = UIFont(name: "Avenir-Medium", size: 18)
                stopLabel.textColor = UIColor(red:155/255, green:155/255, blue:155/255, alpha:1.0)
                stopLabel.textAlignment = .left
                stopLabel.numberOfLines = 2
                stopLabel.translatesAutoresizingMaskIntoConstraints = false
                
                
                self.view.addSubview(stopLabel)
                stopLabel.anchor(top: currentRouteLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: CGFloat(yPos+(63*i)), paddingLeft: CGFloat(xPos), paddingBottom: 0, paddingRight: 100, width: 0, height: 0)
                listStops.append(stopLabel)
                
                //add the checkin buttons
                //Checkin button
                let checkinButton: UIButton = {
                    let button = UIButton()
                    button.setImage(#imageLiteral(resourceName: "checkin button"), for: .normal)
                    button.setImage(#imageLiteral(resourceName: "checkin button disabled"), for: .disabled)
                    button.translatesAutoresizingMaskIntoConstraints = false
                    button.addTarget(self, action: #selector(handleCheckin), for: .touchUpInside)
                    button.tag = i
                    return button
                }()
                
                self.view.addSubview(checkinButton)
                checkinButton.anchor(top: currentRouteLabel.bottomAnchor, left: nil, bottom: nil, right: view.rightAnchor, paddingTop: CGFloat(yPos+(63*i)), paddingLeft: 0, paddingBottom: 0, paddingRight: 25, width: 0, height: 0)
                listCheckinButtons.append(checkinButton)
                
                if Variables.checkedinIDs.contains(checkinButton.tag) {
                    checkinButton.isEnabled = false
                }
//                yPos += 63
                
//                i += 1
  
   
            }
            

   
        } else {
            print("not on route")
            removeDataFromPrevRoute()
            currentRouteLabel.isHidden = true
            createRouteLabel.isHidden = false
            
            //delete checkin story
            Variables.checkinStory = []
            removeStoryFunctionality()
            
        }
    }
    
    
    fileprivate func removeDataFromPrevRoute(){
        
        //remove stops and their labels
        for stop in listStops{
            var i = 0
            print("going into remove part")
            print(stop)
            stop.removeFromSuperview()
            listStops.remove(at: i)
            i += 1
        }
        
        //remove buttons
        for button in listCheckinButtons{
            var i = 0
            button.removeFromSuperview()
            listCheckinButtons.remove(at: i)
            i += 1
        }
        
    }
    
    fileprivate func addStoryFunctionality(){
        
        self.view.addSubview(storyButton)
        self.storyButton.anchor(top: currentRouteLabel.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 12, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 110, height: 110)
        self.storyButton.centerXAnchor.constraint(equalTo: view.centerXAnchor ).isActive = true

        
        self.view.addSubview(storyImage)
        self.storyImage.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 94, height: 94)
        self.storyImage.centerXAnchor.constraint(equalTo: storyButton.centerXAnchor ).isActive = true
        self.storyImage.centerYAnchor.constraint(equalTo: storyButton.centerYAnchor ).isActive = true
        
        
        self.view.addSubview(addToStoryButton)
        self.addToStoryButton.anchor(top: nil, left: storyButton.rightAnchor, bottom: storyButton.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: -35, paddingBottom: -8, paddingRight: 0, width: 36, height: 41)
        
    }
    
    fileprivate func removeStoryFunctionality(){
        storyButton.removeFromSuperview()
        addToStoryButton.removeFromSuperview()
        storyImage.removeFromSuperview()
    }
    
    
}


// Handle the user's selection.
extension checkinViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        self.searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        let checkinController = checkintoViewController()
        checkinController.stopName = place.name
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.navigationController?.pushViewController(checkinController, animated: true)
            Variables.checkinImage = #imageLiteral(resourceName: "add a photo")
        }
        
        
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
