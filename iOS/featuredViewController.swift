//
//  featuredViewController.swift
//  DateRoute
//
//  Copyright © 2018 DateRoute. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import GoogleMaps
import Alamofire
import SwiftyJSON


class featuredViewController: UICollectionViewController {

    //var myCollectionView: UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: FeaturedLayout)

    private let db = Firestore.firestore()
    
    var alamoFireManager = Alamofire.SessionManager()
    private var listener: ListenerRegistration?

    private var routes: [FeaturedRoute] = []
    private var transitioning: Bool = false
    var indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

    fileprivate func observeQuery() {
        // Show loading indicator while loading
        self.indicator.startAnimating()
        guard let query = query else { return }
        stopObserving()

        // Display data from Firestore, part one

        listener = query.addSnapshotListener { [unowned self] (snapShot, error) in
            guard let snapShot = snapShot else {
                print("Error fetching snapShot results: \(error!)")
                return
            }
            let models = snapShot.documents.map{ (document) -> FeaturedRoute in
                let routeName = document.data()["name"] as! String
                var routeImage = UIImage()
                if let currentImage = UIImage(named: routeName + ".jpg") {
                    routeImage = currentImage
                }
                
                let model = FeaturedRoute(data: document.data(), title: routeName, backgroundImage: routeImage)
                return model
            }
            
            self.routes = models
            self.collectionView?.reloadData()
            // Stop loading indicator once we've loaded the data
            self.indicator.stopAnimating()
        }
    }

    fileprivate var query: Query? {
        didSet {
            if let listener = listener {
                listener.remove()
                observeQuery()
            }
        }
    }

    fileprivate func baseQuery() -> Query {
        return db.collection("featuredDatesTest").limit(to: 15)
    }

    fileprivate func stopObserving() {
        listener?.remove()
    }

    override func viewDidLoad() {
        // Add loading indicator to view for when we load data from firebase
        view.addSubview(self.indicator)
        self.indicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        self.indicator.center = view.center
        let transform: CGAffineTransform = CGAffineTransform(scaleX: 5, y: 5)
        self.indicator.transform = transform

        super.viewDidLoad()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 6 // seconds
        configuration.timeoutIntervalForResource = 6
        alamoFireManager = SessionManager(configuration: configuration)
        
        collectionView?.register(featuredCell.self, forCellWithReuseIdentifier: "featuredCell")
        
        //Change to the correct color and font
        navigationController?.navigationBar.barTintColor = UIColor(red: 119/255, green: 162/255, blue: 187/255, alpha:1.0)
        navigationItem.title = "Featured"
        let label = UILabel(frame: CGRect(x: view.center.x, y: view.center.y+66, width: 97, height: 33))
        label.font = UIFont(name: "Avenir-Heavy", size: 24)
        //label.text = "Featured"
        label.textAlignment = .center
        label.textColor = UIColor(red:1, green:1, blue:1, alpha:1.0)
        navigationItem.titleView = label
        //UIApplication.shared.statusBarStyle = .default
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = .lightContent
        
        query = baseQuery()

        collectionView!.backgroundColor = UIColor.clear
        collectionView!.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        UIApplication.shared.statusBarStyle = .lightContent
        observeQuery()
    }
    
//    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
//        if transitioning == false {
//            transitioning = true
//            let homeController = HomeViewController()
//
//            let routeName = routes[indexPath.row].getRoute()["name"] as! String
//            let stops: [String] = routes[indexPath.row].getRoute()["stops"] as! [String]
//            var route = [RouteStop]()
//            var order = [Int:RouteStop]()
//
//            let path = GMSMutablePath()
//            let mapView = homeController.mapView
//            let color = (UIColor(red:119/255, green:162/255, blue:187/255, alpha:1.0),"blue")
//            for i in 0...(stops.count-1) {
//                let accessToken = "3Zdg7kl2psI3NWUEfmHuFl6Vqy-qz3aOHxpR54lHnnq9M0ELW2vad_Ht1wA5-IwB1VpnnEm3lU51JwdjVxLdgMaTFKekCNEOVsT2AT7gsw2RdSltPU8GxNIuQPn4WnYx"
//                let headers: HTTPHeaders = ["Authorization":"Bearer \(accessToken)"]
//                let id = stops[i]
//                let url = "https://api.yelp.com/v3/businesses/\(id)"
//
//                alamoFireManager.request(url, headers : headers).responseJSON { response in
//                    let json = JSON(response.data!)
//
//                    let name = json["name"].stringValue
//                    let id = json["alias"].stringValue
//                    let address = json["location"]["display_address"].stringValue
//                    let lat = json["coordinates"]["latitude"].floatValue
//                    let long = json["coordinates"]["longitude"].floatValue
//                    let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
//                    var categories = [String]()
//                    for (_, category) in json["categories"] {
//                        let type = category["alias"].stringValue
//                        categories.append(type)
//                    }
//                    let rating = json["rating"].stringValue
//                    let phone = json["phone"].stringValue
//                    let imageUrl = json["image_url"].stringValue
//                    let url = json["url"].stringValue
//                    let routeStop = RouteStop(Name: name, Id: id, YelpInfo: json, Address: address, location:location, Categories: categories, Rating: rating, Phone: phone, ImageURL: imageUrl, Url: url, image: nil, marker:nil)
//                    route.append(routeStop)
//                    order[i+1] = routeStop
//
//                    if route.count == stops.count {
//                        let selectedRoute = dateRoute(name: routeName, order: order, path: path, mapView: mapView, color: color, line: [GMSPolyline](), viewController: homeController)
//
//                        homeController.ROUTES = [selectedRoute]
//                        homeController.routeType = HomeViewController.routeTypes.Featured
//                        self.navigationController?.pushViewController(homeController, animated: true)
//                        self.transitioning = false
//
//                    }
//                    else {
//                        print("DIDNT FINISH FETCHING, missing:", id)
//                        if i == stops.count-1 {
//                            print("last item")
//                        }
//                    }
//
//                }
//
//            }
//        }
//
//    }


}


// MARK: - UICollectionViewDataSource

extension featuredViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return routes.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featuredCell", for: indexPath) as! featuredCell
        cell.route = routes[indexPath.item]
//        if cell.frame.height > FeaturedLayoutConstants.Cell.standardHeight {
//            cell.swipeLabel.fadeOut(duration: 3.0, delay: 1)
//        }
        
        return cell
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.setNeedsFocusUpdate()
    }
}

// MARK: - UICollectionViewDelegate

extension featuredViewController {

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let layout = collectionViewLayout as! FeaturedLayout
        let offset = layout.dragOffset * CGFloat(indexPath.item)
        if collectionView.contentOffset.y != offset {
            collectionView.setContentOffset(CGPoint(x: 0, y: offset), animated: true)
        }
        if transitioning == false {
            transitioning = true
            let homeController = HomeViewController()
            
            let routeName = routes[indexPath.row].getRoute()["name"] as! String
            let stops: [String] = routes[indexPath.row].getRoute()["stops"] as! [String]
            var route = [RouteStop]()
            var order = [Int:RouteStop]()
            
            let path = GMSMutablePath()
            let mapView = homeController.mapView
            let color = (UIColor(red:119/255, green:162/255, blue:187/255, alpha:1.0),"blue")
            for i in 0...(stops.count-1) {
                let accessToken = "3Zdg7kl2psI3NWUEfmHuFl6Vqy-qz3aOHxpR54lHnnq9M0ELW2vad_Ht1wA5-IwB1VpnnEm3lU51JwdjVxLdgMaTFKekCNEOVsT2AT7gsw2RdSltPU8GxNIuQPn4WnYx"
                let headers: HTTPHeaders = ["Authorization":"Bearer \(accessToken)"]
                let id = stops[i]
                let url = "https://api.yelp.com/v3/businesses/\(id)"
                
                alamoFireManager.request(url, headers : headers).responseJSON { response in
                    let json = JSON(response.data!)
                    
                    let name = json["name"].stringValue
                    let id = json["alias"].stringValue
                    let address = json["location"]["display_address"].stringValue
                    let lat = json["coordinates"]["latitude"].floatValue
                    let long = json["coordinates"]["longitude"].floatValue
                    let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                    var categories = [String]()
                    for (_, category) in json["categories"] {
                        let type = category["alias"].stringValue
                        categories.append(type)
                    }
                    let rating = json["rating"].stringValue
                    let phone = json["phone"].stringValue
                    let imageUrl = json["image_url"].stringValue
                    let url = json["url"].stringValue
                    let routeStop = RouteStop(Name: name, Id: id, YelpInfo: json, Address: address, location:location, Categories: categories, Rating: rating, Phone: phone, ImageURL: imageUrl, Url: url, image: nil, marker:nil)
                    route.append(routeStop)
                    order[i+1] = routeStop
                    
                    if route.count == stops.count {
                        let selectedRoute = dateRoute(name: routeName, order: order, path: path, mapView: mapView, color: color, line: [GMSPolyline](), viewController: homeController)
                        
                        homeController.ROUTES = [selectedRoute]
                        homeController.routeType = HomeViewController.routeTypes.Featured
                        self.navigationController?.pushViewController(homeController, animated: true)
                        self.transitioning = false
                        
                    }
                    else {
                        print("DIDNT FINISH FETCHING, missing:", id)
                        if i == stops.count-1 {
                            print("last item")
                        }
                    }
                    
                }
                
            }
        }

    }

}







class FeaturedRoute {
    private var route: Dictionary<String, Any>
    var title: String
    var stopNames : [String]
    //var tags: [String]
    var backgroundImage: UIImage

    var stopsTogether: String {
        get {
            var together = ""
            for i in 0...(stopNames.count-1) {
                if i == (stopNames.count-1) {
                    together += stopNames[i]
                } else {
                    together += stopNames[i] + "  •  "
                }
            }
            return together
        }
    }

    init(data: Dictionary<String, Any>, title: String, backgroundImage: UIImage) {
        self.route = data
        self.title = title
        self.stopNames = data["stopNames"] as! [String]
        //self.tags = self.getRoute()["tags"] as! [String]
        self.backgroundImage = backgroundImage
    }

    func getRoute() -> Dictionary<String, Any>{
        return route
    }

}



