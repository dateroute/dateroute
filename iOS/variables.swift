//
//  variables.swift
//  DateRoute
//
//  Created by Cameron Korb on 6/10/18.
//  Copyright © 2018 DateRoute. All rights reserved.
//

import Foundation
import UIKit

struct Variables {
    static var onRoute = false
    static var currentRoute: dateRoute?
    static var checkinImage: UIImage? = #imageLiteral(resourceName: "add a photo")
    static var checkinImageDict = [Int:UIImage]()
    static var currentCheckin = 0
    static var checkinStory: [UIImage] = []
    static var userProfile: UIImage?
    static var checkedinIDs: [Int] = []
        
}
