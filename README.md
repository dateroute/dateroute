#Dateroute
**Your mobile dating concierge.** 
Visit [www.dateroute.io](http://localhost/ "link title") for more!

![](https://static.wixstatic.com/media/5c8805_364454451bf8416080917c226f8ee01e~mv2.png/v1/fill/w_202,h_402,al_c,q_80,usm_0.66_1.00_0.01/iphonex.webp)

##Overview
Dateroute is an iOS app that uses the Yelp Fusion API to build custom dates by stringing together several locations. Swift makes an API call to a fully-managed Google Cloud instance running a search algorithm (Python). The algorithm then calls the Yelp API and loads the returned values in a graph--using the Floyd-Warshall algorithm to find the shortest path. User authenication is managed through Firebase using the Facebook API, while user information, metadata, and featured routes are stored in the realtime database. Swift directly calls the Google Maps/Places API to get the background map and load walking directions between businesses.
![](https://static.wixstatic.com/media/5c8805_36d9e5377ee3433388ebf039e952155d~mv2.png/v1/fill/w_1104,h_612,al_c,q_85,usm_0.66_1.00_0.01/Screen%20Shot%202018-11-08%20at%2010_48_55%20PM_pn.webp)

##Requirements

**Xcode** v10.1
**Swift** 4
**Python** 3
