# -*- coding: utf-8 -*-
import logging
import requests
import sys
import grequests

# This client code can run on Python 2.x or 3.x.  Your imports can be
# simpler if you only need one of those.
try:
    # For Python 3.0 and later
    from urllib.parse import quote
except ImportError:
    # Fall back to Python 2's urllib2 and urllib
    from urllib import quote

SEARCH_LIMIT = 50
"""
Yelp Fusion API code sample.

This program demonstrates the capability of the Yelp Fusion API
by using the Search API to query for businesses by a search term and location,
and the Business API to query additional information about the top result
from the search query.

Please refer to http://www.yelp.com/developers/v3/documentation for the API
documentation.

This program requires the Python requests library, which you can install via:
`pip install -r requirements.txt`.

"""

# Yelp Fusion no longer uses OAuth as of December 7, 2017.
# You no longer need to provide Client ID to fetch Data
# It now uses private keys to authenticate requests (API Key)
# You can find it on
# https://www.yelp.com/developers/v3/manage_app
API_KEY = '3Zdg7kl2psI3NWUEfmHuFl6Vqy-qz3aOHxpR54lHnnq9M0ELW2vad_Ht1wA5-IwB1VpnnEm3lU51JwdjVxLdgMaTFKekCNEOVsT2AT7gsw2RdSltPU8GxNIuQPn4WnYx'


# API constants, you shouldn't have to change these.
API_HOST = 'https://api.yelp.com'
SEARCH_PATH = '/v3/businesses/search'
AUTOCOMPLETE_PATH = '/v3/autocomplete'
BUSINESS_PATH = '/v3/businesses/'  # Business ID will come after slash.
EVENT_SEARCH_PATH = '/v3/events'


def request(host, path, api_key, url_params=None, use_grequests=False):
    """Given your API_KEY, send a GET request to the API.

    Args:
        host (str): The domain host of the API.
        path (str): The path of the API after the domain.
        API_KEY (str): Your API Key.
        url_params (dict): An optional set of query parameters in the request.

    Returns:
        dict: The JSON response from the request.

    Raises:
        HTTPError: An error occurs from the HTTP request.
    """
    url_params = url_params or {}
    url = '{0}{1}'.format(host, quote(path.encode('utf8')))
    headers = {
        'Authorization': 'Bearer %s' % api_key,
    }

    if use_grequests:
        return grequests.get(url, headers=headers, params=url_params)

    response = requests.request('GET', url, headers=headers, params=url_params)

    if response.status_code != 200:
        logging.error("request to url: %s with headers: %s and params: %s failed with status code: %d reason: %s", url, headers, url_params, response.status_code, response.reason)
        sys.exit(1)

    return response.json()


def search(term, latitude, longitude, categories, sort_by='best_match', radius=40000, api_key=API_KEY, limit=SEARCH_LIMIT):
    """Query the Search API by a search term and location.

    Args:
        term (str): The search term passed to the API.
        latitude (str): The Latitude of the search location
        longitude (str): The Longitude of the search location
    Returns:
        dict: The JSON response from the request.
    """

    url_params = {
        'term': term.replace(' ', '+'),
        'latitude': latitude,
        'longitude': longitude,
        'limit': limit,
        'sort_by': sort_by,
        'radius': radius,
    }
    if categories != "":
        url_params['categories'] = categories.rstrip(',')

    response = request(API_HOST, SEARCH_PATH, api_key, url_params=url_params)
    businesses = response.get('businesses')

    if not businesses:
        logging.error(u'No dateroutes for {0} in {1},{2} found.'.format(term, latitude, longitude))

    return response


def grequests_search(term, latitude, longitude, categories, sort_by='best_match', radius=20000, api_key=API_KEY, limit=SEARCH_LIMIT):
    """Query the Search API by a search term and location.

    Args:
        term (str): The search term passed to the API.
        latitude (str): The Latitude of the search location
        longitude (str): The Longitude of the search location
    Returns:
        dict: The JSON response from the request.
    """

    url_params = {
        'term': term.replace(' ', '+'),
        'latitude': latitude,
        'longitude': longitude,
        'limit': limit,
        'sort_by': sort_by,
        'radius': radius,
    }
    if categories != "":
        url_params['categories'] = categories.rstrip(',')
    return request(API_HOST, SEARCH_PATH, api_key, url_params=url_params, use_grequests=True)

def event_search(term, latitude, longitude, categories, start_date, end_date, sort_on='popularity', radius=2000, api_key=API_KEY, limit=SEARCH_LIMIT):
    """Query the Search API by a search term and location.

    Args:
        term (str): The search term passed to the API.
        latitude (str): The Latitude of the search location
        longitude (str): The Longitude of the search location

    Returns:
        dict: The JSON response from the request.
    """

    url_params = {
        'term': term.replace(' ', '+'),
        'latitude': latitude,
        'longitude': longitude,
        'limit': limit,
        'sort_on': sort_on,
        'start_date': start_date,
        'end_date': end_date,
        'radius': radius
    }

    if categories != "":
        url_params['categories'] = categories.rstrip(',')
    response = request(API_HOST, EVENT_SEARCH_PATH, api_key, url_params=url_params)
    events = response.get('events')
    if not events:
        logging.error(u'No dateroutes for {0} in {1},{2} found.'.format(term, latitude, longitude))

    return response


def autocomplete(text, api_key=API_KEY):
    """Query the automcomplete API by a search term and location.

    Args:
        term (str): The search term passed to the API.
        location (str): The search location passed to the API.

    Returns:
        dict: The JSON response from the request.
    """

    url_params = {
        'text': text.replace(' ', '+'),
    }
    return request(API_HOST, AUTOCOMPLETE_PATH, api_key, url_params=url_params)


def get_business(business_id, api_key=API_KEY):
    """Query the Business API by a business ID.

    Args:
        business_id (str): The ID of the business to query.

    Returns:
        dict: The JSON response from the request.
    """
    business_path = BUSINESS_PATH + business_id

    return request(API_HOST, business_path, api_key)


def query_api(term, location):
    """Queries the API by the input values from the user.

    Args:
        term (str): The search term to query.
        location (str): The location of the business to query.
    """
    response = search(API_KEY, term, location)

    businesses = response.get('businesses')

    if not businesses:
        logging.info(u'No businesses for {0} in {1} found.'.format(term, location))
        return

    business_id = businesses[0]['id']

    print(u'{0} businesses found, querying business info for the top result "{1}" ...'
          .format(len(businesses), business_id))
    response = get_business(API_KEY, business_id)

    print(u'Result for business "{0}" found:'.format(business_id))
    pprint.pprint(response, indent=2)


def get_reviews(api_key, business_id):
    reviews_path = BUSINESS_PATH + business_id + '/reviews'
    return request(API_HOST, reviews_path, api_key)


def get_tags(business):
    tags = []
    for i in business['categories']:
        tags.append(i['title'])
    return tags


def get_next_possible_stops(last_stop, last_stop_type, term, categories, search_radius):
    if last_stop_type.lower() == "event":
        latitude = last_stop['latitude']
        longitude = last_stop['longitude']
    else:
        latitude = last_stop['coordinates']['latitude']
        longitude = last_stop['coordinates']['longitude']

    if term.lower() == "event":
        start_date, end_date = get_start_end_date(2)
        next_stops = event_search(term, latitude, longitude, categories, start_date, end_date)
        if 'events' in next_stops:
            next_stops = next_stops['events']
        else:
            return
    else:
        next_stops = search(term, latitude, longitude, categories, radius=search_radius)['businesses']

    return next_stops
