
from __future__ import print_function

import json
import gmplot # pip install -r requirements.txt
import datetime
import time
import calendar
import argparse
import yelp_api_calls as api
import graph_search as gs
import logging


# Defaults for our simple example.
DEFAULT_LOCATION = 'New York, NY'
DEFAULT_GPS = [40.740004, -73.985453]
ROUTE_LENGTH = 3
NUM_ROUTES = 5
logging.basicConfig(level=logging.INFO)

BAR_CATEGORY_DICT = json.load(open("bar_categories.json"))
RESTAURANT_CATEGORY_DICT = json.load(open("restaurant_categories.json"))

def get_start_end_date(day_range):
    start_date = datetime.datetime.utcnow()
    end_date = start_date + datetime.timedelta(days=day_range)
    start_date = calendar.timegm(start_date.utctimetuple())
    end_date = calendar.timegm(end_date.utctimetuple())
    return start_date, end_date


def google_map_plot(dateroutes, event_list, start_lat, start_long):

    gmap = gmplot.GoogleMapPlotter(start_lat, start_long, 13)
    colors = ['cornflowerblue', 'red', 'yellow', 'plum', 'green']
    for i in range(len(dateroutes)):
        c = colors[i]
        
        for stop_i in range(len(dateroutes[i][0])):
            lat = dateroutes[i][0][stop_i]['coordinates']['latitude']
            longitude = dateroutes[i][0][stop_i]['coordinates']['longitude']
            gmap.marker(lat, longitude, c)
        
    gmap.draw('output.html')


def google_map_plot_new(dateroutes, start_lat, start_long, name_to_coords):
    gmap = gmplot.GoogleMapPlotter(start_lat, start_long, 13)
    colors = ['cornflowerblue', 'red', 'yellow', 'plum', 'green']
    for i in range(len(dateroutes)):
        c = colors[i]
        path = dateroutes[i][0]
        for stop_i in range(len(path)):
            lat = name_to_coords[path[stop_i]][0]
            longitude = name_to_coords[path[stop_i]][1]
            gmap.marker(lat, longitude, c)
        
    gmap.draw('output.html')


def get_categories(term):
    if term == "":
        return set()

    if term in BAR_CATEGORY_DICT:
        return set(BAR_CATEGORY_DICT[term])
    elif term in RESTAURANT_CATEGORY_DICT:
        return set(RESTAURANT_CATEGORY_DICT[term])

    # If we couldn't find the category we use yelp's autocomplete api to find the category set of strings
    A = api.autocomplete(term)
    categories = ''
    if 'categories' not in A:
        return ''
    for i in range(len(A['categories'])):
        categories = categories + A['categories'][i]['alias'] + ','

    if len(categories) > 0:
        # yelp autocomplete api returns a "," at the end of the string so we cut off the empty string from being part of the set
        return set(categories.split(",")[:-1])
    else:
        return set()


def get_route_lengths(dateroutes):
    lengths = []
    for route in dateroutes:
        lengths.append(route[2])
    return sum(lengths) / len(lengths)


def get_average_rating(dateroutes):
    averages = []
    for route in dateroutes:
        # index 1 is the rating
        averages.append(route[1])
    return sum(averages) / len(averages)


def get_distinct_stops(dateroutes):
    stops = set([])
    for route in dateroutes:
        for stop in route[0]:
            stops.add(stop["name"])
    return len(stops)


# returns (route_lengths, average_route_rating, number_distinct_stops)
def get_route_metrics(dateroutes):
    if len(dateroutes) > 0:
        logging.info("Average Route lengths: %f, Average rating: %f, Distinct Stops: %d",
                     get_route_lengths(dateroutes),
                     get_average_rating(dateroutes),
                     get_distinct_stops(dateroutes)
                     )
    else:
        logging.error("No routes returned to get metrics for")


def get_routes(search_info, plot=False):
    logging.info("Search request info: %s", search_info)
    requested_activites = search_info["Activities"]
    bar_categories = search_info["Bar_categories"]
    restaurant_categories = search_info["Restaurant_categories"]
    search_radius = int(search_info["Search_radius"])
    latitude = float(search_info["Latitude"])
    longitude = float(search_info["Longitude"])
    logging.info("Requested activity list: %s", requested_activites)

    # we use a set so that we do not pass in repeat categories in the bar_category_string to the yelp api
    bar_category_set = set()
    for category in bar_categories:
        bar_category_set.update(get_categories(category))
    bar_category_string = ",".join(bar_category_set)
    logging.info("Bar category string: %s", bar_category_string)

    # we use a set so that we do not pass in repeat categories in the restaurant_category_string to the yelp api
    restaurant_category_set = set()
    for category in restaurant_categories:
        restaurant_category_set.update(get_categories(category))
    restaurant_category_string = ",".join(restaurant_category_set)
    logging.info("Restaurant category string: %s", restaurant_category_string)

    dateroutes = gs.make_shortest_path_routes(NUM_ROUTES, requested_activites, bar_category_string, restaurant_category_string, latitude, longitude, search_radius)
    if plot:
        google_map_plot(dateroutes, event_list, latitude, longitude)
    return dateroutes


if __name__ == "__main__":
    location = DEFAULT_LOCATION
    parser = argparse.ArgumentParser(description="Run yelp search based on json event list and categories")
    parser.add_argument("json_file", type=str, help="json file specifying search")
    args = parser.parse_args()
    search_info = json.load(open(args.json_file))
    start = time.time()
    dateroutes = get_routes(search_info)
    logging.info("TIME: %d" % (time.time() - start))
    get_route_metrics(dateroutes)
