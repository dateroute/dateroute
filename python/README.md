# Killer date search algorithm

## Overview
The algorithm finds sweet dateroutes

# Running the algorithm
1. Make a json with the search criteria and run as follows
2. Run search
   'python search.py json_name.json'

# Running a local server that serves http post requests and returns search results
1. We run the search server in a docker container to avoid differences in python environments
2. Download docker on your Mac: https://www.docker.com/docker-mac
3. Open the docker app
4. Update WORKDIR specified in Dockerfile to be the absolute path of this repo's python directory on your host
5. Run (Assuming you want to run your server on port 19000):
   docker build -t search-server .
   docker run -p 19000:19000 -it --rm --name running-search-server search-server
6. You can now make requests to http://localhost:19000