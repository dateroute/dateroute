# http server to serve search requests
# to start server:
# python local_search_server.py --port 19000

import BaseHTTPServer
import argparse
import json
import httplib
import logging
import sys
import search

log = logging.getLogger("LocalSearchServer")


class SearchHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_POST(self):
        content_len = int(self.headers.getheader('content-length'))
        inbody = self.rfile.read(content_len)
        response = httplib.BAD_REQUEST
        try:
            search_info = json.loads(inbody)
        except:
            log.error("Failed parsing json -- inbody = %s", inbody)
            log.error("self.path: %s", self.path)
            log.error("self.headers: %s", self.headers)
        else:
            dateroutes = search.get_routes(search_info)
            response = httplib.OK
        self.send_response(response)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(dateroutes))


class SearchServer (BaseHTTPServer.HTTPServer):

    def __init__(self, host_name, port_number):
        BaseHTTPServer.HTTPServer.__init__(self, (host_name, port_number), SearchHandler)
        self.host_name = host_name
        self.port_number = port_number

    def serve_forever(self):
        log.info("SearchServer starting %s:%s", self.host_name, self.port_number)
        try:
            BaseHTTPServer.HTTPServer.serve_forever(self)
        except KeyboardInterrupt:
            log.info("KeyboardInterrupt")
        except:
            t, v, tb = sys.exc_info()
            log.warning("Suppressing (likely expected) %s: %s", t, v)
        finally:
            self.stop()

    def stop(self):
        self.server_close()
        log.info("SearchServer Stopped %s:%s", self.host_name, self.port_number)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Server for search requests')
    parser.add_argument('--port', required=True, type=long, help='Port to listen on')

    args = parser.parse_args()

    host_name = ''
    SearchServer(host_name, args.port).serve_forever()
