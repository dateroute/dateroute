import yelp_api_calls as api
import pandas as pd
import geopy.distance
import grequests
import heapq
import json
import logging
import time
from sklearn.cluster import KMeans

RESTAURANT = "Restaurant"
BAR = "Bar"
logger = logging.getLogger("graph_search")

class StopGraph:
    def __init__(self, activity_list, bar_categories, restaurant_categories):
        self.graph = []
        self.neighbors = {}
        self.loc_to_index = {}
        self.index_to_loc = {}
        self.name_to_stop = {}
        self.starter_list = []
        self.ender_list = []
        self.activity_list = activity_list
        self.bar_categories = bar_categories
        self.restaurant_categories = restaurant_categories

    def get_category(self, activity):
        if activity == RESTAURANT:
            return self.restaurant_categories
        elif activity == BAR:
            return self.bar_categories
        else:
            return ""

    # adds the vertex dest to the graph if it is not present and adds an edge from source to dest
    def add_to_graph(self, source, dest, distance):
        if dest not in self.loc_to_index:
            self.loc_to_index[dest] = len(self.loc_to_index)
            source_index = self.loc_to_index[source]
            dest_index = self.loc_to_index[dest]
            new_row = [float("inf") for i in range(len(self.loc_to_index))]
            new_row[source_index] = distance
            new_row[dest_index] = 0.0
            for i in range(len(self.graph)):
                if i == source_index:
                    self.graph[i].append(distance)
                else:
                    self.graph[i].append(float("inf"))
            self.graph.append(new_row)
        else:
            source_index = self.loc_to_index[source]
            dest_index = self.loc_to_index[dest]
            self.graph[source_index][dest_index] = distance
        source_index = self.loc_to_index[source]
        dest_index = self.loc_to_index[dest]
        if source_index not in self.neighbors:
            self.neighbors[source_index] = [dest_index]
        else:
            self.neighbors[source_index].append(dest_index)

            
    # adds another level of stops to the graph, searching for stops near the last sto
    def add_next_level_to_graph(self, last_stop, number_stop, search_radius):
        next_stops = self.get_next_level_stops(last_stop, number_stop, search_radius)
        self.add_next_stops_to_graph(last_stop['name'], next_stops)
        return next_stops

    def get_next_level_stops(self, last_stop, number_stop, search_radius):
        last_stop_type = self.activity_list[number_stop - 1]
        activity = self.activity_list[number_stop]
        categories = self.get_category(activity)
        return api.get_next_possible_stops(last_stop,
                                           last_stop_type,
                                           activity,
                                           categories,
                                           search_radius)

    def add_next_stops_to_graph(self, last_stop_name, next_stops):
        for next_stop in next_stops:
            self.add_to_graph(last_stop_name,
                              next_stop['name'],
                              next_stop['distance'])

    def cluster_search_stops(self, stops, num_stop, search_radius):
        latitudes = []
        longitudes = []
        for i in stops:
            stop_coords = self.name_to_stop[i]['coordinates']
            # In case the lat and long are None we will leave that location out of the clustering
            if stop_coords['latitude'] is not None and stop_coords['longitude'] is not None:
                latitudes.append(stop_coords['latitude'])
                longitudes.append(stop_coords['longitude'])
            else:
                logging.warn("Stop: %s does not have a Latitude and Longitude (%s)", self.name_to_stop[i]['name'], self.name_to_stop[i])

        # We don't have lats and longs for the stop so we don't know where to do the search from...
        # so we return an empty search result
        if len(latitudes) == 0:
            logging.error("Returning empty search because none of the stops had Latitude and Longitude")
            return []

        df = pd.DataFrame({
            'x': longitudes,
            'y': latitudes
        })
        # latitudes and longitudes are of the same length
        kmeans = KMeans(n_clusters=min(4, len(latitudes)))
        kmeans.fit(df)
        kmeans.predict(df)
        centroids = kmeans.cluster_centers_
        next_stops = []

        reqs = []
        for centroid in centroids:
            longitude, latitude = centroid
            activity = self.activity_list[num_stop]
            categories = self.get_category(activity)
            # make search radius smaller for bars and restaurants so that the results from yelp_api_calls
            # are more location specific
            if activity == BAR or activity == RESTAURANT:
                new_radius = int(search_radius * .75)
            else:
                new_radius = search_radius
            reqs.append(api.grequests_search(self.activity_list[num_stop], latitude, longitude, categories, radius=new_radius))

        responses = grequests.map(reqs)
        for response in responses:
            if response.status_code == 200:
                response_dict = json.loads(response.content)
                businesses = response_dict['businesses']
                next_stops.extend(businesses)
            else:
                logging.warn("Got Error Response code %s Response: %s", response.status_code, response.json())

        # add edges to graph
        for stop in stops:
            for next_stop in next_stops:
                coords_1 = (self.name_to_stop[stop]["coordinates"]["latitude"], self.name_to_stop[stop]["coordinates"]["longitude"])
                coords_2 = (next_stop["coordinates"]["latitude"], next_stop["coordinates"]["longitude"])
                distance = geopy.distance.vincenty(coords_1, coords_2).m
                self.add_to_graph(stop, next_stop["name"], distance)

        return next_stops

    def yelp_search_and_build(self, starters, search_radius=1600):
        for starter in starters:
            self.name_to_stop[starter['name']] = starter
        self.starter_list = [i['name'] for i in starters]

        expanding = self.starter_list
        for i in range(1, len(self.activity_list)):
            logging.info("Expanding stops: %s", expanding)
            new_name_to_stop = self.name_to_stop
            new_expanding = []
            for node_name in expanding:
                if node_name not in self.loc_to_index:
                    self.add_to_graph(node_name, node_name, 0.0)
            next_stops = self.cluster_search_stops(expanding, i, search_radius)

            for stop in next_stops:
                if stop['name'] not in new_name_to_stop:
                    new_name_to_stop[stop['name']] = stop
                    new_expanding.append(stop['name'])

            self.name_to_stop = new_name_to_stop
            # only update expanding if there are new stops to expand from
            if len(new_expanding) > 0:
                expanding = new_expanding
            if i == len(self.activity_list) - 1:
                self.ender_list = expanding

        for loc in self.loc_to_index:
            self.index_to_loc[self.loc_to_index[loc]] = loc

    def construct_nexts(self):
        nexts = []
        for i in range(len(self.graph)):
            row = []
            for j in range(len(self.graph)):
                if self.graph[i][j] < float("inf") and self.graph[i][j] != 0.0:
                    row.append(j)
                else:
                    row.append(None)
            nexts.append(row)
        return nexts

    # Updates the shortest paths between vertices in self.graph
    # Returns:
    #     2D array where each entry in the matrix (i, j) represents the vertex k which precedes vertex j in the shortest path from i to j
    # "self.graph" will have the shortest path lengths and "nexts" will be used to reconstruct the full path given a source and destination vertex
    def floyd_warshall(self):
        nexts = self.construct_nexts()
        
        for k in range(len(self.graph)):
            for i in range(len(self.graph)):
                for j in range(len(self.graph)):
                    if self.graph[i][j] > self.graph[i][k] + self.graph[k][j]:
                        self.graph[i][j] = self.graph[i][k] + self.graph[k][j]
                        nexts[i][j] = nexts[i][k]
        return nexts

    def dijkstra(self, source_indices):

        nexts = {}
        for source_index in source_indices:
            q = []
            dists = []
            prevs = []
            for i in range(len(self.graph)):
                dists.append(float("inf"))
                prevs.append(None)
            dists[source_index] = 0
            heapq.heappush(q, (0, source_index))
            while len(q) > 0:
                u_dist, u_index = heapq.heappop(q)
                if u_index not in self.neighbors:
                    continue
                for v in self.neighbors[u_index]:
                    alt = dists[u_index] + self.graph[u_index][v]
                    if alt < dists[v]:
                        dists[v] = alt
                        prevs[v] = u_index
                        heapq.heappush(q, (alt, v))

            for i in range(len(dists)):
                self.graph[source_index][i] = dists[i]
            nexts[source_index] = prevs

        return nexts


    # Arguments:
    #     num_shortest: number of shortest paths we want to see
    # Returns:
    #     shortest: list of 3 item tuples (start, end, length) where each tuple represents a path
    def get_top_shortest(self, num_shortest=50):
        shortest = []
        max_dist = 0.0
        max_index = 0
        for starter in self.starter_list:
            for ender in self.ender_list:
                source_index = self.loc_to_index[starter]
                dest_index = self.loc_to_index[ender]
                distance = self.graph[source_index][dest_index]
                if len(shortest) < num_shortest:
                    shortest.append((starter, ender, distance))
                    if distance > max_dist:
                        max_dist = distance
                        max_index = len(shortest) - 1
                elif distance < max_dist:
                    shortest[max_index] = (starter, ender, distance)
                    max_dist = 0.0
                    max_index = 0
                    for i in range(len(shortest)):
                        if shortest[i][2] > max_dist:
                            max_dist = shortest[i][2]
                            max_index = i
        return shortest


    # Arguments:
    #     source: name of starting stop in the path
    #     dest: name of last stop in the path
    #     nexts: 2D array where each entry in the matrix (i, j) represents the vertex k which precedes vertex j in the shortest path from i to j
    # Returns:
    #     list of the stops in the path
    def get_path(self, source, dest, nexts):
        source_index = self.loc_to_index[source]
        dest_index = self.loc_to_index[dest]
        if nexts[source_index][dest_index] is None:
            return [], 0.0

        path = [self.name_to_stop[source]]
        rating = self.name_to_stop[source]["rating"]
        length = 1
        while source_index != dest_index:
            source_index = nexts[source_index][dest_index]
            name = self.index_to_loc[source_index]
            path.append(self.name_to_stop[name])
            if "rating" in self.name_to_stop[name]:
                rating += self.name_to_stop[name]["rating"]
                length += 1
        return path, rating / length

    def get_dijkstra_path(self, source, dest, nexts):
        source_index = self.loc_to_index[source]
        dest_index = self.loc_to_index[dest]
        prevs = nexts[source_index]
        path = [self.name_to_stop[dest]]
        rating = self.name_to_stop[dest]["rating"]
        length = 1
        while prevs[dest_index] is not None:
            dest_index = prevs[dest_index]
            name = self.index_to_loc[dest_index]
            path.insert(0, self.name_to_stop[name])
            if "rating" in self.name_to_stop[name]:
                rating += self.name_to_stop[name]["rating"]
                length += 1
        return path, rating / length

    
    # Arguments:
    #     path_list: list of 3 item tuples (start, end, length) where each tuple represents a path
    #     nexts: 2D array where each entry in the matrix (i, j) represents the vertex k which precedes vertex j in the shortest path from i to j
    #     loc_to_index: dictionary maps stop names -> index in the 2D array
    #     index_to_loc: dictionary maps index in 2D array -> stop name
    # Returns:
    #     list of path lists
    def convert_to_full_paths(self, path_list, nexts):
        full_paths = []
        length_index = 2
        for path in path_list:
            path_length = path[length_index]
            path, avg_rating = self.get_path(path[0], path[1], nexts)
            full_paths.append((path, avg_rating, path_length))
        return full_paths

    def convert_dijkstras_to_full_paths(self, path_list, prevs):
        full_paths = []
        length_index = 2
        for path in path_list:
            path_length = path[length_index]
            path, avg_rating = self.get_dijkstra_path(path[0], path[1], prevs)
            full_paths.append((path, avg_rating, path_length))
        return full_paths

    def get_dijkstra_shortest_paths(self):
        starter_index_list = [self.loc_to_index[starter] for starter in self.starter_list]
        prevs = self.dijkstra(starter_index_list)

        shortest = self.get_top_shortest()
        return self.convert_dijkstras_to_full_paths(shortest, prevs)

    def get_floyd_shortest_paths(self):
        nexts = self.floyd_warshall()
        shortest = self.get_top_shortest()
        return self.convert_to_full_paths(shortest, nexts)

# Filters out the paths that are not within the top num_paths highest rated paths
def get_highest_rated_full_paths(full_paths, num_paths=5):
    best_paths = []
    min_average = float("inf")
    min_index = 0
    for i in range(len(full_paths)):
        path, average, length = full_paths[i]
        if len(best_paths) < num_paths:
            best_paths.append((path, average, length))
            if average < min_average:
                min_average = average
                min_index = len(best_paths) - 1
        elif average > min_average:
            best_paths[min_index] = (path, average, length)
            min_average = float("inf")
            min_index = 0
            for i in range(len(best_paths)):
                if best_paths[i][1] < min_average:
                    min_average = best_paths[i][1]
                    min_index = i
    return best_paths


# recursive dp implementation to get to find a certain number of routes (number_of_routes_left in the initial call) that have the highest
# number of distinct stops
def get_most_distinct(routes, index, contained, number_of_routes_left):
    if number_of_routes_left == 0 or index == len(routes):
        return len(contained), []

    # don't add route to routes
    not_added, not_added_routes = get_most_distinct(routes, index + 1, contained, number_of_routes_left)

    # add route to routes
    stop_names = []
    for stop in routes[index]:
        stop_names.append(stop["name"])
    new_contained = contained.union(set(stop_names))
    added, added_routes = get_most_distinct(routes, index + 1, new_contained, number_of_routes_left - 1)
    if added > not_added:
        added_routes.append(index)
        return added, added_routes
    else:
        return not_added, not_added_routes


def get_distinct_edge_routes(routes, start_index):
    start_route = routes[start_index]
    edges = set(get_edge_list(start_route))
    route_indices = [start_index]
    route_index = (start_index + 1) % len(routes)

    while route_index != start_index:
        route_edges = set(get_edge_list(routes[route_index]))
        if len(edges.intersection(route_edges)) == 0:
            route_indices.append(route_index)
            edges = edges.union(route_edges)
        route_index = (route_index + 1) % len(routes)
    return route_indices


def get_edge_list(route):
    edges = []
    for i in range(len(route) - 1):
        edge = (route[i]['name'], route[i + 1]['name'])
        edges.append(edge)
    return edges


def get_most_distinct_edge_routes(routes):
    best_route_indices = []
    for route_index in range(len(routes)):
        new_most_distinct = get_distinct_edge_routes(routes, route_index)
        if len(new_most_distinct) > len(best_route_indices):
            best_route_indices = new_most_distinct
    return best_route_indices

# removes routes that has stops outside of the search_radius window
def remove_out_of_window(start_lat, start_long, routes, search_radius):
    new_routes = []
    for route in routes:
        path, average, length = route
        can_add = True
        for stop in path:
            latitude = stop["coordinates"]["latitude"]
            longitude = stop["coordinates"]["longitude"]
            if geopy.distance.vincenty((start_lat, start_long), (latitude, longitude)).m > search_radius:
                can_add = False
        if can_add:
            new_routes.append(route)
    return new_routes


def make_shortest_path_routes(num_routes, activity_list, bar_categories, restaurant_categories, start_lat, start_long, search_radius, use_dijkstra=True):
    graph = StopGraph(activity_list, bar_categories, restaurant_categories)
    starter_categories = graph.get_category(activity_list[0])
    starters = api.search(activity_list[0], start_lat, start_long, starter_categories, radius=search_radius)['businesses']

    if len(activity_list) == 1:
        # Don't need an additional graph search if there's only one stop
        full_paths = []
        for starter in starters:
            # path in format (stop_list, avg_rating, path_length)
            # path length is always 0 because we just have one stop
            full_paths.append(([starter], starter["rating"], 0))
    else:
        graph_build = time.time()
        graph.yelp_search_and_build(starters, search_radius)
        logging.info("Yelp search and build time: %f", time.time() - graph_build)

        distance_search_start = time.time()
        if use_dijkstra:
            full_paths = graph.get_dijkstra_shortest_paths()
        else:
            full_paths = graph.get_floyd_shortest_paths()
        logging.info("Distance search time: %f", time.time() - distance_search_start)

    full_paths = remove_out_of_window(start_lat, start_long, full_paths, search_radius)
    just_paths = []
    for path in full_paths:
        just_paths.append(path[0])
    distinct_start = time.time()
    # gets maximal indpendent set of routes that do not share an edge in there routes
    path_indices = get_most_distinct_edge_routes(just_paths)
    logging.info("Distinct time: %f", time.time() - distinct_start)
    most_distinct = []
    for index in path_indices:
        most_distinct.append(full_paths[index])

    highest_rated = get_highest_rated_full_paths(most_distinct)
    for i in range(len(highest_rated)):
        path = highest_rated[i]
        logging.info("path %d:", int(i + 1))
        stop_path = [i['name'] for i in path[0]]
        logging.info(stop_path)
    return highest_rated
