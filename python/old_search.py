import yelp_api_calls as api

def add_to_route(route, route_names, last_stop_type, term, categories, search_radius):
    last_stop = route[-1]
    next_stops = api.get_next_possible_stops(last_stop, last_stop_type, term, categories, search_radius)

    for i in range(len(next_stops)):
        if next_stops[i]['name'] not in route_names:
            route.append(next_stops[i])
            route_names.append(next_stops[i]['name'])
            return


# returns list of routes in the format:
# (route, route_length, route_avg_stop_rating)
# route is a list of stops where a stop is a dictionary with keys: name, latitude, longitude
def make_routes(num_routes, term_list, starters, categories, search_radius=1600):
    dateroutes = []
    for r in range(min(num_routes, len(starters))):
        stop1 = starters[r]
        route = [stop1]
        names = [stop1['name']]
        for i in range(1, len(term_list)):
            add_to_route(route, names, term_list[i-1], term_list[i], categories[i], search_radius)

        print()
        for stop in route:
            #print(stop['name'], get_tags(stop))
            print(stop['name'])
        print('============================================')
        dateroutes.append(route)
    lengths = []
    routes_with_scores = []
    
    for dateroute in dateroutes:
        length = 0.0
        rating = 0.0
        route = []
        for i in range(len(dateroute)):
            stop = dateroute[i]
            route.append({"name": stop["name"], "latitude": stop["coordinates"]["latitude"], "longitude": stop["coordinates"]["longitude"]})
            if i != 0:
                length += stop["distance"]
                rating += stop["rating"]
        avg_rating = rating / len(dateroute)
        routes_with_scores.append((route, avg_rating, length))
        lengths.append(length)
    return routes_with_scores
