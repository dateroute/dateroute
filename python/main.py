from flask import Flask
from flask import request
import json
import search
# Imports the Google Cloud client library
import google.cloud.logging

app = Flask(__name__)

required_args = ["Activities", "Bar_categories", "Restaurant_categories", "Num_stops", "Latitude", "Longitude"]

@app.route('/dateRoute', methods=['POST'])
def searchRequestHandler():
    """Given search body, do route search."""
    search_info = request.get_json()
    for arg in required_args:
        if arg not in search_info:
            return "%s not provided" % (arg), 400

    dateroutes = search.get_routes(search_info)
    return json.dumps(dateroutes), 200

if __name__ == '__main__':
    # Instantiates a client
    client = google.cloud.logging.Client()

    # Connects the logger to the root logging handler; by default this captures
    # all logs at INFO level and higher
    client.setup_logging()
    app.run(host='127.0.0.1', port=19000)
