#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 00:09:05 2018

@author: davidsherwood
"""

import numpy as np
import itertools
import random


def create_activities(array_in):
    '''
    Arguments: 
        array_in - list of the buttons selected by the user, only include terms
        in the lookup
        
    Returns: an array to be passed to Marty's code as the 'activities' parameter
    '''

    # hard coded for now   
    if array_in==['Dinner']:
        choices = [['Restaurant','Bar'],
                  ['Restaurant','Dessert'],
                  ['Restaurant','Dessert','Bar']]
    
    elif array_in==['Drinks']:
        choices = [['Bar','Bar'],
                   ['Bar','Dessert'],
                   ['Bar','Bar','Bar'],
                   ['Bar','Bar','Bar','Bar']]
        
    elif array_in==['Coffee']:
        choices = [['Coffee','Park'],
                   ['Coffee','Dessert']]
    
    elif set(array_in)==set(['Dinner','Drinks']):
        choices = [['Restaurant','Bar','Bar'], 
                   ['Bar','Restaurant','Bar'],
                   ['Restaurant','Bar'],
                   ['Restaurant','Dessert','Bar'],
                   ['Restaurant','Bar','Dessert'],
                   ['Restaurant','Dessert','Bar','Bar']]
    
    elif set(array_in)==set(['Dinner','Coffee']):
        choices = [['Coffee','Dinner'],
                   ['Coffee','Dinner','Dessert'],
                   ['Coffee','Park','Dinner']]
                    
    elif set(array_in)==set(['Coffee','Drinks']):
        choices = [['Coffee','Bar'],
                   ['Coffee','Bar','Bar']]
    
    else:
        choices = [['Coffee','Restaurant','Bar']]
        
    # simply chooses a random array - ideally we want to display several routes 
    # with all different activities arrays
    activities = random.choice(choices) 


    return activities

